
# 2.0.18

 - nouveau build pour respecter les obligations de google

# 2.0.16
 - ajoute des clés css sur les boutons principaux pour permettre leur modification
   via la personnalisation CSS
 - lien avec le serveur pour race condition sur les vehicules associés aux IK

# 2.0.14
 - corrige le bug du mode de paiement "especes pro"

# 2.0.12
 - corrige une erreur de gestion des indemnités kilométriques (photo requise)
 
# 2.0.7
 - corrige un cas particulier (iPhone, login/pass mais pas de serveur)

# 2.0.5
 - choix du mode de paiement par défaut forcé sur "perso"
 - amélioration de l'affichage de la liste des véhicules (ik & carburant)

# 2.0.0

- intégration complète avec Dolibarr : les projets dolibarr sont remontés sous forme d'étiquettes
  ou tags que vous pouvez associer à des frais
- gestion des comptes inactifs
- ajout d'une option vous permettant de choisir la monnaie que vous voulez (l'euro n'est plus imposé)
- ajout d'une vérification mensuelle pour s'assurer que vous utilisez toujours le même véhicule
- choix d'un pdf ou d'une image issue de la galerie photo du smartphone comme justificatif de frais
- ajout d'une option en bas de la page d'ouverture de session pour que vous puissiez choisir votre
  serveur auto-hébergé si vous ne pouvez pas l'héberger sur doliscan.votredomaine.ext
- amélioration de la commande "fermer la session"
- amélioration de la gestion des tags / étiquettes
- mise à jour de toute la "pile" technique et correctif des bugs associés

# 1.9.15
- ajout de la distance (km) sur l'historique rapide des indemnités kilométriques
- affichage des itinéraires les plus fréquents des 6 derniers mois

# 1.9.14
- correctif : une erreur se produit sur certaines versions d'android lors de l'envoi de la photo
  (message "Error on resolveLocalFileSystemURL")
- ajout d'un formulaire de saisie de documents PDF en tests (fonctionnalité expérimentale)

# 1.9.10
- correctif : l'aperçu de la photo ne s'affichait plus (mais était bien envoyée au serveur)

# 1.9.8
- la modification des frais est de nouveau possible
- modification des IK et prise en compte du véhicule utilisé

# 1.9.6
- changement de la lib pinch-zoom pour utiliser celle-ci https://github.com/GoogleChromeLabs/pinch-zoom
- corrige un bug de gestion des traductions (affichage des détails pour la configuration des véhicules)
- la modification d'un itinéraire est de nouveau possible

# 1.9.4
- retour d'une fonctionnalité perdue de la version 1.9.2 : la possibilité de modifier la date des indemnités kilométriques
- optimisation technique (évite de tenter le chargement de la css custom à chaque changement de page si ça n'a pas marché après l'ouverture de session)

# 1.9.2
- nouveau module de gestion des IK pour simplifier la saisie des itinéraires
  répétitifs
- interdit la saisie de montants négatifs (frais, tva etc.)

# 1.8.8
- retour du dev sur la transformation des données de l'agenda en IK
- corrige un bug qui empêchait la mise à jour / modification d'un frais
  depuis l'historique

# 1.8.6
- ajoute une vérification des données saisies (TVA) pour éviter d'envoyer
  des montants "impossibles"
# 1.8.5
- ajoute un bouton pour supprimer un compte de la liste des comptes préférés
- affiche le choix du compte par défaut au lancement si on a plusieurs comptes
  actifs

# 1.8.4
- fin de l'implémentation de la solution de changement rapide d'utilisateur pour
  gérer plusieurs comptes depuis l'application

# 1.8.3
- grosse amélioration pour la re-synchronisation des frais dont le justificatif
  n'a pas été téléversé lors de la création de la fiche

# 1.8.2
- ajoute la possibilité de changer les taux de TVA (adaptation pour les DOM par ex.)
- interdit la saisie de date de frais dans le futur

# 1.7.2
- suppression de sqlite
- passage sur la version internationale multilingue

# 1.6.11
- corrige un bug de calcul automatique des distances pour les itinéraires avec étapes
- ameliore le système de récupération des tickets justificatifs manquants (lorsqu'un
  transfert a échoué).

# 1.6.10
- corrige un bug de sauvegarde de l'immatriculation du véhicule pro

# 1.6.9
- corrige un bug d'accès en écriture en // à IndexedDB sur certains smartphone

# 1.6.8
- corrige un bug d'accès en écriture en // à IndexedDB sur certains smartphone

# 1.6.6

 - mise à jour du compte utilisateur
 - supprime les données custom pour éviter un effet "étrange" lorsqu'on "change de compte"
   (anticipation sur la version multicomptes)

# 1.6.4

- récupération automatique des données de personnalisation lors de l'ouverture de session
- transfert des véhicules automatique en cas de changement de smartphone
- modification des véhicules possible
- ik: affiche le choix des véhicules même s'il n'y en a qu'un + icone pour en ajouter/modifier

# 1.6.2

- possibilité d'adaptation de l'interface pour les revendeurs partenaires
- migration de SQLite vers IndexedDB pour simplifier l'adoption dans f-droid et limiter les
  dépendances externes -> phase transitoire, sqlite est encore présent le temps de faire
  la migration des données

# 1.6.1

- corrige l'authentification automatique avec le qrCode
- corrige le retour sur l'historique lorsqu'on est sur un détail de note de frais

# 1.6.0

 - modification profonde de l'outil de gestion des indemnités kilométriques pour gérer les
   déplacements de type "tournée" avec des étapes

# 1.5.0

 - grosse modification de présentation générale
 - menu principal réorganisé par thème
 - menu latéral réorganisé par priorité des outils
 - ajout d'une icone "nuage" sur les zones qui peuvent être activées pour aider à la saisie
   (par exemple calcul automatique de TVA, gestion des distances pour les IK)
 - mise à jour des outils de développement (invisible pour l'utilisateur)
 - grosse modification du modèle de calcul des distances
 - modification technique pour la saisie des montants à virgules (ios en particulier)
 - ajouts de messages d'informations pour aider à trouver les erreurs en cas de problème
 - pré-remplissage de certains champs (repas / hotel)
 - début d'intégration du module QRCode (mise à jour serveur à venir)

# 1.4.0

 - module indemnités kilométriques : prise en charge des homonymes (villes) et propositions de
   choisir la bonne ville dans une liste fournie par le serveur
 - module indemnités kilométriques : possibilité de saisir le code postal de la ville de départ
   ou d'arrivée, le serveur fera tout seul la conversion
 - début de r&d sur la capacité d'aller "piocher" dans l'agenda local de l'utilisateur ses rendez
   vous pour les transformer en IK, ça se passe dans la zone de tests (menu à droite puis agenda-ik)
 - de ce fait l'application nécessite une nouvelle autorisation pour consulter votre agenda

# 1.2.3

 - passage en prod de cette nouvelle "branche" de l'application

# 1.2.0

 - grosse modification du coeur de l'application pour se conformer aux exigences d'Apple vis à vis des
   applications développées avec Cordova (voir "ITMS-90809: Deprecated API Usage - Apple will stop
   accepting submissions of apps that use UIWebView APIs")

# 1.0.22

 - ajoute un test pour vérifier si un dolibarr auto hébergé existe vraiment et bascule sur doliscan.fr sinon
 - mémorise le dernièer moyen de paiement utilisé pour le cocher par défaut lors de la prochaine opération

# 1.0.21

 - corrige le bug #236 : au lancement de la version 1.0.19 ou 1.0.20 l'application boucle sur l'ajout de véhicule

# 1.0.20

- version f-droid

# 1.0.19

- pre remplis le champ email quand on fait une demande de mot de passe oublié
- ajoute un liseret rouge autour des champs à compléter, retour du bouton valider en bas de chaque formulaire de saisie

# 1.0.18

- numero de version ++ pour ios

# 1.0.17

- corrige un gros bug sur la base sqlite embarquee : on risquait d'avoir des doublons
- amelioration de la logique de lancemnt, supprime l'index sur le nom de fichier (IK = pas de fichier donc doublons de NULL possible)
- tentative d'upgrade cordova pour ios
- presentation dans le cas d'un choix de vehicules
- deplace l'oeil pour voir le mot de passe, sur ios il était quasi impossible à activer
- amelioration pour la popup de mise à jour de l'appli
- ressere un peu l'interface d'accueil + duplication de code pour la gestion d'une mise à jour du client
- on deplace le message d'information de nouvelle version sur la page d'accueil
- build prod sans debug pour release
- sauvegarde du fichier si upload KO + correctifs iPhone

