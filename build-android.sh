#!/bin/bash
APPNAME="doliscan"
#pseudo script used to build package
#export ANDROID_SDK_ROOT=/opt/android/
#export ANDROID_HOME=/opt/android/sdk/
export JAVA_HOME=/usr
export ANDROID_SDK_ROOT=/opt/android/sdk/
unset ANDROID_HOME

export PATH=${PATH}:$ANDROID_SDK_ROOT:$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/platform-tools:$ANDROID_SDK_ROOT/tools/bin/
SIGNID="ryxeo"
SIGFILE=~/bin/android_release.keystore
STOREPASS=`cat ~/.android-key_pass`
fullInstall=""

#par defaut on n'essaye pas de déployer via ADB
adbInstall="N"

function usage {
    echo "Usage: $(basename $0) [-n] [-f]" 2>&1
    echo "  -n : build leger"
    echo "  -d : build debug"
    echo "  -f : build complet avec suppression et réinstallation des plugins & base"
    echo "  -i : déploiement sur le téléphone via ADB à la fin du build"
    exit 1
}

optstring=":ndfhi"
while getopts ${optstring} arg; do
    case "${arg}" in
        h)
            usage
        ;;
        n)
            fullInstall="N"
        ;;
        d)
            debug="Y"
        ;;
        f)
            fullInstall="o"
        ;;
        i)
            echo "- on fait une installation via ADB à la fin ..."
            adbInstall="o"
        ;;
        *)
            usage
        ;;
    esac
done

if [[ ${#} -eq 0 ]]; then
    echo "Voulez vous faire un build complet avec suppression et réinstallation des plugins & base ?"
    echo -n "[o/N]"
    read fullInstall
fi

#dans tous les cas on fait place nette sinon par exemple le versionCode n'est pas mis à jour
rm -rf platforms/android/app/build/*

cordova clean

if [ "${fullInstall}" == "o" ]; then
    cordova platform remove android
    cordova platform add android@12.0.1
    
    # liste des plugins cordova pour la version android
    PLUGINS="cordova-plugin-image-cropper cordova-plugin-whitelist cordova-plugin-androidx cordova-plugin-androidx-adapter cordova-plugin-thumbnail"
    for p in $PLUGINS
    do
        cordova plugin remove ${p}
    done
    PLUGINS="cordova-plugin-aes256-encryption"
    for p in $PLUGINS
    do
        cordova plugin add ${p}
    done
    
    #image cropper n'est pa publié chez npm
    #nov 2023 mort cordova plugin add https://github.com/rycks/cordova-plugin-image-cropper.git
    cordova plugin add https://github.com/rycks/cordova-plugin-crop.git

    #barcode scanner a jour
    cordova plugin add @red-mobile/cordova-plugin-barcodescanner

    #image thumbnail
    cordova plugin add https://github.com/almas/cordova-plugin-thumbnail.git

    #l'agenda
    cordova plugin add cordova-plugin-calendar-cxm --variable CALENDAR_USAGE_DESCRIPTION="Calendar access is required to read appointments and build expenses..."
fi

#on force le UserAgent :)
APPVERSION=`grep fr.caprel.doliscan config.xml | grep ${APPNAME} | sed s/".*version=\""/""/ | cut -d '"' -f1`
sed -i -e s%".*preference name=\"OverrideUserAgent\" value=\".*"%"    <preference name=\"OverrideUserAgent\" value=\"Mozilla/5.0 OpenNDF/${APPVERSION} Android\" />"% config.xml

#et on hardcode le globalAppVersion au passage
sed -i -e s%"\(var globalAppVersion =\)\( .*; \)\(.*\)"%"\1 \"${APPVERSION}\"; \3"% ./www/js/src/config.js

#puis on minify/compile les js si c'est le mode prod
if [ "${debug}" == "Y" ]; then
  echo "build js mode dev"
  if [ -x /usr/bin/uglifyjs ]; then
    /usr/bin/uglifyjs www/js/src/*.js -b --webkit -o www/js/index.minTMP.js --verbose
  else
    cat www/js/src/*.js > www/js/index.minTMP.js
  fi
else
  echo "build js mode prod"
  if [ -x /usr/bin/uglifyjs ]; then
    /usr/bin/uglifyjs www/js/src/*.js -c --webkit -o www/js/index.minTMP.js --verbose
  else
    cat www/js/src/*.js > www/js/index.minTMP.js
  fi
fi

if [ -f www/js/index.minTMP.js ]; then
    cat www/js/src/copyright.js www/js/index.minTMP.js > www/js/index.min.js
    rm -f www/js/index.minTMP.js
fi

if [ "${debug}" == "Y" ]; then
    cordova build android --debug
else
    cordova build android --release
fi
if [ ${?} -eq "0" ]; then
    echo "build ok ... la suite"
    if [ -z "${STOREPASS}" ]; then
        echo "Il manque la passphrase pour utiliser la clé de signature de l'app..."
        exit -2
    fi
    
    if [ "${debug}" == "Y" ]; then
        echo "no sign, debug mode"
        if [ "${adbInstall}" == "o" ]; then
            echo -n "Installation en cours via ADB ..."
            adb install -r platforms/android/app/build/outputs/apk/debug/app-debug.apk
        else
            echo "adbInstall = ${adbInstall} ... si vous voulez installer manuellement le paquet :"
            echo "adb install -r platforms/android/app/build/outputs/apk/debug/app-debug.apk"
        fi
    else
    
        # OUTFILE="./platforms/android/app/build/outputs/bundle/release/app-release.aab"
        OUTFILE="./platforms/android/app/build/outputs/bundle/release/app-release.aab"
        OUTAPK="./platforms/android/app/build/outputs/apk/release/app-release.apks"
        # jarsigner -verbose -sigalg SHA256withRSA -digestalg SHA-256 -keystore ${SIGFILE} -storepass ${STOREPASS} ${OUTFILE}  ${SIGNID}
        jarsigner -verbose -sigalg SHA256withRSA -digestalg SHA-256 -keystore ${SIGFILE} -storepass ${STOREPASS} ${OUTFILE}  ${SIGNID}
        #rm -f ${OUTAPK}
        #/opt/android/build-tools/26.0.2/zipalign -v 4 ${OUTFILE} ${OUTAPK}

        if [ ${?} -eq "0" ]; then
            echo "sign ok ... la suite"
            java -jar /opt/android/bundletool-all-1.8.0.jar build-apks --bundle=${OUTFILE} \
              --output=${OUTAPK} --ks=${SIGFILE} --ks-key-alias=${SIGNID} --ks-pass=pass:${STOREPASS}

            #copy file to /tmp
            # TMPFILENAME="/tmp/${APPNAME}-${APPVERSION}.aab"
            # echo "Copie du paquet vers ${TMPFILENAME}"
            # cp ${OUTFILE} ${TMPFILENAME}
            TMPFILENAME="/tmp/${APPNAME}-${APPVERSION}.aab"
            echo "Copie du paquet vers ${TMPFILENAME}"
            cp ${OUTFILE} ${TMPFILENAME}

            if [ "${adbInstall}" == "o" ]; then
                echo -n "Installation en cours via bundletool ..."
                #jarsigner -verbose -sigalg SHA256withRSA -digestalg SHA-256 -keystore ${SIGFILE} -storepass ${STOREPASS} ${OUTAPK}  ${SIGNID}
                #adb install ${OUTAPK}
                java -jar /opt/android/bundletool-all-1.8.0.jar install-apks --apks=${OUTAPK}
            else
                echo "Si vous voulez installer manuellement le paquet :"
                echo "java -jar /opt/android/bundletool-all-1.8.0.jar install-apks --apks=${OUTAPK}"
            fi
        else
            echo "erreur de signature"
            exit -2
        fi
    fi
else
    echo "erreur"
    exit -1
fi

