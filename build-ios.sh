#pseudo script used to build package

#on force le UserAgent :)
APPVERSION=`grep version config.xml | grep fr.caprel.doliscan | sed s/".*version=\""/""/ | cut -d '"' -f1`
sed -i -e s%".*preference name=\"OverrideUserAgent\" value=\".*"%"    <preference name=\"OverrideUserAgent\" value=\"Mozilla/5.0 OpenNDF/${APPVERSION} iOS\" />"% config.xml

cordova platform remove android

#et on vire les plugins android qui - je ne sais pas pourquoi - causent des soucis lorsqu'on builde la version ios
PLUGINS="cordova-plugin-image-cropper cordova-plugin-androidx cordova-plugin-androidx-adapter cordova-plugin-wkwebview-file-xhr"
for p in ${PLUGINS}
do
  cordova plugin remove ${p} --save
done

#on a deux plugins differents pour decouper les images selon qu'on est avec android ou ios

cordova platform remove ios
cordova platform add ios@6.2.0

# le xhr ios est un grand mystere... il ne faut surtout pas laisser trainer celui-ci qui entre en conflit avec
# cordova-plugin-wkwebview-file-xhr
cordova plugin remove @globules-io/cordova-plugin-ios-xhr

#les plugins a ajouter, on les supprime avant de les remettre pour etre sur qu'ils sont propres
# note: cordova-plugin-aes256-encryption amene swift et +200Mo d'exe final ... on fait ou pas ?
PLUGINS="cordova-plugin-wkwebview-file-xhr cordova-plugin-aes256-encryption"
for p in ${PLUGINS}
do
  cordova plugin remove ${p}
  cordova plugin add ${p} --save
done

cordova plugin add https://github.com/AraHovakimyan/cordova-plugin-wkwebviewxhrfix

cordova plugin remove cordova-plugin-calendar-cxm
cordova plugin add cordova-plugin-calendar-cxm --variable CALENDAR_USAGE_DESCRIPTION="Calendar access is required to read appointments and build expenses..."

cordova prepare ios
cordova build ios

#corrige le "bug" du plugin crop qui limite la decoupe a un carre par defaut
#cf https://github.com/jeduan/cordova-plugin-crop/pull/65
sed -i -e s/"cropController.keepingCropAspectRatio = YES"/"cropController.keepingCropAspectRatio = NO"/ plugins/cordova-plugin-crop/src/ios/CTCrop.m
if [ -f ./platforms/ios/doliscan/Plugins/cordova-plugin-crop/CTCrop.m ]; then
    cp ./plugins/cordova-plugin-crop/src/ios/CTCrop.m ./platforms/ios/doliscan/Plugins/cordova-plugin-crop/CTCrop.m
fi

#et xcode recompilera proprement les modifications

echo "lancez xcode pour signer l'appli et l'expédier sur apple store ..."

