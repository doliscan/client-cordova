#pseudo script used to build package

#on force le UserAgent :)
APPVERSION=`grep version config.xml | grep doliscan | sed s/".*version=\""/""/ | cut -d '"' -f1`
sed -i -e s%".*preference name=\"OverrideUserAgent\" value=\".*"%"    <preference name=\"OverrideUserAgent\" value=\"Mozilla/5.0 OpenNDF/${APPVERSION} iOS\" />"% config.xml

#on a deux plugins differents pour decouper les images selon qu'on est avec android ou ios
#cordova platform remove android
#cordova plugin remove cordova-plugin-image-cropper
#cordova platform add ios@6.1.1
#cordova plugin add cordova-plugin-crop
# pour le passé cordova plugin add cordova-plugin-wkwebview-engine

#corrige le "bug" du plugin crop qui limite la decoupe a un carre par defaut
#cf https://github.com/jeduan/cordova-plugin-crop/pull/65
sed -i -e s/"cropController.keepingCropAspectRatio = YES"/"cropController.keepingCropAspectRatio = NO"/ plugins/cordova-plugin-crop/src/ios/CTCrop.m
if [ -f ./platforms/ios/doliscan/Plugins/cordova-plugin-crop/CTCrop.m ]; then
    cp ./plugins/cordova-plugin-crop/src/ios/CTCrop.m ./platforms/ios/doliscan/Plugins/cordova-plugin-crop/CTCrop.m
fi

cordova prepare ios
cordova build ios
echo "lancez xcode pour signer l'appli et l'expédier sur apple store ..."
