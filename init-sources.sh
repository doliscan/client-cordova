#!/bin/bash
textReset=$(tput sgr0)
textGreen=$(tput setaf 2)
message_info () {
  echo "${textGreen}[my-app]${textReset} $1"
}

message_info "Creating necessary directories..."
mkdir plugins
mkdir platforms

message_info "Adding platforms..."
# If using cordova, change to: cordova platform add android
cordova build android
cordova build ios

message_info "Adding plugins..."
# If using cordova, change to: cordova plugin add
#cordova local plugin add https://git-wip-us.apache.org/repos/asf/cordova-plugin-device.git
