#!/bin/bash

# npm install
# cordova telemetry off
#cordova plugin rm cordova-plugin-advanced-http
#cordova plugin add cordova-plugin-advanced-http
#first cordova prepare fail with
#ENOENT: no such file or directory, open '.../plugins/cordova-plugin-advanced-http/www/dependency-validator.js'
# cordova prepare android || cordova prepare android

#on force le UserAgent :)
APPVERSION=`grep version config.xml | grep doliscan | sed s/".*version=\""/""/ | cut -d '"' -f1`
sed -i -e s%".*preference name=\"OverrideUserAgent\" value=\".*"%"    <preference name=\"OverrideUserAgent\" value=\"Mozilla/5.0 OpenNDF/${APPVERSION} Android\" />"% config.xml

#et on hardcode le globalAppVersion au passage
sed -i -e s%"\(var globalAppVersion =\)\( .*; \)\(.*\)"%"\1 \"${APPVERSION}\"; \3"% ./www/js/src/config.js

#puis on minify/compile les js
if [ -x /usr/bin/uglifyjs ]; then
    /usr/bin/uglifyjs www/js/src/*.js -o www/js/index.minTMP.js --verbose
else
    #no minify
    cat www/js/src/*.js > www/js/index.minTMP.js
fi
if [ -f www/js/index.minTMP.js ]; then
    cat www/js/src/copyright.js www/js/index.minTMP.js > www/js/index.min.js
    rm -f www/js/index.minTMP.js
fi

#cleanup for fdroid
#find . -name *.jar -exec rm {} \;
