#!/bin/bash

#pour etre sur d'avoir les plugins ope

for plugin in cordova-plugin-advanced-http cordova-plugin-app-version cordova-plugin-camera cordova-plugin-console cordova-plugin-crop cordova-plugin-device cordova-plugin-file cordova-plugin-file-opener2 cordova-plugin-fullscreen cordova-plugin-inappbrowser cordova-plugin-insomnia cordova-plugin-network-information cordova-plugin-whitelist cordova-plugin-wkwebview-file-xhr
do
    cordova plugin rm $plugin
    cordova plugin add $plugin
done

#plugin special non dispo dans les depots npm
cordova plugin rm cordova-plugin-image-cropper
cordova plugin add https://github.com/Riyaz0001/cordova-plugin-image-cropper.git --save

