#!/bin/bash
VERSION=`grep version= config.xml | grep widg | sed s/".*version=\"\(.*\)\" xmlns=.*"/"\\1"/`
rsync -avP ./platforms/android/app/build/outputs/apk/release/app-release.apks doliscan.fr:/home/webs/doliscan.fr/download/doliscan-android-${VERSION}.apk
rsync -avP ./platforms/android/app/build/outputs/bundle/release/app-release.aab doliscan.fr:/home/webs/doliscan.fr/download/doliscan-android-${VERSION}.aab
