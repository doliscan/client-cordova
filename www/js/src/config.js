/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019-2021 - GNU AGPLv3
*/
// **********************************************************************
//Ne pas toucher, mis à jour automatiquement à partir du config.xml
var globalAppVersion = "2.0.18";         // mis à jour automatiquement à partir du fichier config.xml lors du build android
// **********************************************************************


var globalDevMode = false;              // on est en mode dev ou prod ? par defaut oui pour la page d'index
                                        // note en mode dev la photo de la facturette n'est pas obligatoire
var globalMyCamera = undefined;         // l'appareil photo
var globalpictureSource = undefined;    // picture source
var globaldestinationType = undefined;  // sets the format of returned value
var globalNetworkResponse = Date.now(); // Date a laquelle on a reçu une réponse du serveur
var globalCurrentPage = "";             // Page courante pour gérer le retour précédent
var globalCurrentTitle = "";            // Titre de la page courante
var globalTenMinuts = 60 * 10 * 1000;   // 10 minutes pour l'implementation du keepalive (avant de basculer en full API je pense)
var globalCompatibleWithAPIVersion = 4; // On incrémentera la version de l'API compatible quand on modifie des choses importantes sur le serveur
var globalDoliScanDomain = "";          // Pour basculer sur un serveur auto-hebergé
var globalLastInsertId = "";            // Avant d'envoyer les données au serveur on les stocke en cache dans la base, ceci est l'id à supprimer si l'upload est ok
var globalSyncInProgress = false;       // Pour savoir si on est en phase de synchronisation ou pas (gestion différente du "formulaire" d'upload)
var globalPictureFileName = "";         // Le nom du fichier sans résolution (depuis ios wkbidule app:// ... on garde dans cette variable le path basique pour resumable & sync)
var globalLocalFileContent = undefined; // Le contenu du fichier PDF ou Image a envoyer
var globalPicturesAvailable = [];       // Liste des images disponibles (locales)
var globalSyncObject = [];              // Objet qui stocke tout ce qui reste à synchroniser
var globalIkToReuse = undefined;        // L'itineraire à réutiliser pour les IK
var globalCustomCSS = true;             // Par défaut on essaye de charger le CSS custom
var globalLastVehiculeCheck = 0;        // Dernière vérification du véhicule ? voir #25
var globalOneMonth = 60 * 60 * 24 * 31; // 31 jours pour vérifier si l'utilisateur a toujours le même véhicule
var nbEtape = 1;
var dataLDF = "";
var dataIK = "";
var globalMyNavigator = undefined;
var globalResumable = undefined;
var globalProgressBar = undefined;
var globalTypeFrais = "";
var globalTagFrais = "";                //pour stocker quel est le tag actuellement sélectionné
var globalVehiculeEd = undefined;       //vehicule a modifier
var loginAPIKeyErrorInProgress = false; //passe a true dans le cas particulier d'une tentative de login avec une clé d'api qui ne marche pas
//liste des donnees que le serveur peut eventuellement échanger avec nous (factorisation de code)
var globalSrvKeys = ['email', 'api_token', 'firstname', 'name', 'adresse', 'cp', 'ville', 'pays', 'send_copy_to', 'compta_email',
    'compta_ik', 'compta_peage', 'compta_hotel', 'compta_train', 'compta_carburant', 'compta_taxi', 'compta_restauration', 'compta_divers',
    'compta_compteperso', 'compta_tvadeductible', 'official_app_version', 'official_app_message', 'mode_simple',
    'compta_tva_tx1', 'compta_tva_tx2', 'compta_tva_tx3', 'compta_tva_tx4', 'currency'];
