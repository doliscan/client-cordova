/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2021 - GNU AGPLv3
 *
 * all of customizing parts
*/

/**
 * Gestion de l'erreur sur la generation du fichier custom
 * @param {string} title
 * @param {string} message
 */
function errorHandlerCustomFile(title, message) {
    myDebug('errorHandlerCustomFile : ' + title + ' -- message follow:');
    myDebug(message);
    // myDebug('errorHandlerCustomFile end message');
    // ons.notification.alert("Erreur : " + title, { title: "Attention" });
    //Pour ne plus essayer de charger de css custom jusqu'au prochain login
    globalCustomCSS = false;
}


function loadCustomCSS() {
    if ((typeof cordova == 'undefined') || (cordova === null) || (cordova == "")) {
        return;
    }
    if ((typeof cordova.file == 'undefined') || (cordova.file === null) || (cordova.file == "")) {
        return;
    }
    if ((typeof cordova.file.cacheDirectory == 'undefined') || (cordova.file.cacheDirectory === null) || (cordova.file.cacheDirectory == "")) {
        return;
    }

    let cssFileName = cordova.file.cacheDirectory + 'custom.css';
    // myDebug("loadCustomCSS : " + cssFileName);

    window.resolveLocalFileSystemURL(cssFileName, function (fileEntry) {
        // myDebug("loadCustomCSS (2): " + JSON.stringify(fileEntry));
        fileEntry.file(function (file) {
            var reader = new FileReader();
            var fileSize = file.size;
            // myDebug("loadCustomCSS (3) Size = " + file.size);

            reader.onloadend = function (e) {
                var fileContent = reader.result;
                // myDebug("loadCustomCSS : append to header " + fileContent);
                $('head').append(fileContent);
            };

            reader.readAsText(file);
        }, errorHandlerCustomFile.bind(null, "Error on FileEntry " + cssFileName));
    }, errorHandlerCustomFile.bind(null, "Error on resolveLocalFileSystemURL " + cssFileName));
    // $('head').append('<style type="text/css">body{font:normal 14pt Arial, Helvetica, sans-serif;background:#000;color:#fff}a,a:visited{color:#ccc;text-decoration:none;border-bottom:1px solid #00ff00}a:hover{color:#00ff00;border-color:#ccc}</style>');
}

function downloadCustomCSS() {
    $.ajaxSetup({
        timeout: 10000,
        headers: {
            'X-CSRF-TOKEN': localGetData("api_token")
        }
    });

    if (ajaxDownloadCSS)
        ajaxDownloadCSS.abort();

    var ajaxDownloadCSS = $.ajax({
        type: "POST",
        timeout: 5000,
        url: localGetData("api_server") + "/api/CustomizingApp/css",
        data: {
            email: localGetData("email"),
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
        },
        xhrFields: {
            responseType: 'arraybuffer',
            withCredentials: true
        },
        success: function (data) {
            globalNetworkResponse = Date.now();
            // myDebug('updateServerUserFromLocal : succès ...' + JSON.stringify(result));
            hideWait();
            // myDebug('downloadCustomCSS : done');
            let blob = new Blob([data], { type: "text/css" });
            saveCSSFile(data);
        },
        error: function (result) {
            hideWait();
            //pas trop grave pour l'image :)
            // alert('error');
            // myDebug('downloadCustomCSS error.');
        }
    });
}

/**
 * Save custom CSS as local file
 * @param {string} css
 */
function saveCSSFile(css) {
    let cssFileName = '/custom.css';
    // myDebug(blob.size);
    // myDebug(data);

    // cordova.file.etc is where you will save the file. In this case, inside the app folder, in the main storage in Android
    window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function (directoryEntry) {
        myDebug('saveCSSFile Write to ' + cordova.file.cacheDirectory);
        // the 'temp.pdf' is the name you want for your file.
        directoryEntry.getFile(cssFileName, { create: true }, function (fileEntry) {
            // myDebug('downloadCustomCSS Write to ' + cssFileName);
            fileEntry.createWriter(function (fileWriter) {
                fileWriter.onwriteend = function (e) {
                    // great success!
                    myDebug('saveCSSFile Write of file completed.');
                    // myDebug(fileEntry.fullPath);
                    // myDebug(fileEntry);
                    loadCustomCSS();
                };

                fileWriter.onerror = function (e) {
                    // Meh - fail
                    // myDebug('Write failed');
                };

                fileWriter.write(css);
            });
        });
    });

}



function downloadCustomLogo() {
    $.ajaxSetup({
        timeout: 10000,
        headers: {
            'X-CSRF-TOKEN': localGetData("api_token")
        }
    });

    if (ajaxDownloadLogo)
        ajaxDownloadLogo.abort();

    var ajaxDownloadLogo = $.ajax({
        type: "POST",
        timeout: 5000,
        url: localGetData("api_server") + "/api/CustomizingApp/logo",
        data: {
            email: localGetData("email"),
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
        },
        xhrFields: {
            responseType: 'arraybuffer',
            withCredentials: true
        },
        success: function (data) {
            globalNetworkResponse = Date.now();
            // myDebug('updateServerUserFromLocal : succès ...' + JSON.stringify(result));
            hideWait();
            // myDebug('downloadCustomLogo : done');
            let blob = new Blob([data], { type: "image/png" });
            saveLogoFile(data, "png");
        },
        error: function (result) {
            hideWait();
            //pas trop grave pour l'image :)
            // myDebug('downloadCustomLogo error.');
        }
    });
}

function onErrorResolveLocalFileSystemURL(error) {
    myDebug('onErrorResolveLocalFileSystemURL error : ' + error.code);

}
/**
 * Save logo to local file
 * @param {*} data
 * @param {string} format default png file
 */
function saveLogoFile(data, format = "png") {
    myDebug("saveLogoFile");

    let logoFileName = '/custom.' + format;
    // myDebug(blob.size);
    // myDebug(data);

    // cordova.file.etc is where you will save the file. In this case, inside the app folder, in the main storage in Android
    window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function (directoryEntry) {
        myDebug('  saveLogoFile Write to ' + cordova.file.cacheDirectory);
        // the 'temp.pdf' is the name you want for your file.
        directoryEntry.getFile(logoFileName, { create: true }, function (fileEntry) {
            myDebug('  saveLogoFile Write to ' + logoFileName);
            fileEntry.createWriter(function (fileWriter) {
                fileWriter.onwriteend = function (e) {
                    // great success!
                    myDebug('  saveLogoFile Write of file completed.');
                    // myDebug(fileEntry.fullPath);
                    // myDebug(fileEntry);
                    loadCustomLogo();
                };

                fileWriter.onerror = function (e) {
                    // Meh - fail
                    myDebug('  saveLogoFile Write failed');
                };

                fileWriter.write(data);
            });
        });
    }, onErrorResolveLocalFileSystemURL);
    myDebug("  saveLogoFile end");
}


/**
 * Chargement du logo personnalisé
 */
function loadCustomLogo() {
    //Si on fait un appel trop tot ... pas la peine d'aller plus loin
    if ((typeof cordova == 'undefined') || (cordova === null) || (cordova == "")) {
        return;
    }
    if ((typeof cordova.file == 'undefined') || (cordova.file === null) || (cordova.file == "")) {
        return;
    }
    if ((typeof cordova.file.cacheDirectory == 'undefined') || (cordova.file.cacheDirectory === null) || (cordova.file.cacheDirectory == "")) {
        return;
    }
    let logoFileName = cordova.file.cacheDirectory + 'custom.png';
    // myDebug("loadCustomLogo : " + logoFileName);

    window.resolveLocalFileSystemURL(logoFileName, function (fileEntry) {
        // myDebug("loadCustomLogo (2): " + JSON.stringify(fileEntry));
        fileEntry.file(function (file) {
            var reader = new FileReader();
            var fileSize = file.size;
            // myDebug("loadCustomLogo (3) Size = " + file.size);

            reader.onloadend = function () {
                // myDebug("loadCustomLogo Successful file read: " + this.result);
                var blob = new Blob([new Uint8Array(this.result)], { type: "image/png" });
                $("#homeLogo").attr("src", window.URL.createObjectURL(blob));
            };

            reader.readAsArrayBuffer(file);
        }, errorHandlerCustomFile.bind(null, logoFileName));
    }, errorHandlerCustomFile.bind(null, logoFileName));
}

/**
 * remove all custom files and data from local cache
 */
function removeCustomFiles() {
    deleteFile(cordova.file.cacheDirectory, 'custom.png');
    deleteFile(cordova.file.cacheDirectory, 'custom.css');
    localStoreData("customAbout", "");
    //Pour ne plus essayer de charger de css custom jusqu'au prochain login
    globalCustomCSS = false;
}

