/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

var globalDB = null;                    // Le connecteur vers la base de données SQLite
var databaseEngine = 'IndexedDB';
var databaseName = 'doliscanDB';
var databaseVersion = 8;
var globalIndexedDB = null;               // Le connecteur vers la base de données IndexedDB

