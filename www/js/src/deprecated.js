//Fonction dépréciées ou en cours de l'être ... on garde ici au cas ou


/* *************************************
* Lecture du fichier temporaire (photo) (en cache) pour le joindre au formulaire
* au passage on essaye d'en faire une sauvegarde dans le repertoire de stockage
* permanent avec un nom explicite YYYYMMJJ-subject-filename.jpeg
*************************************** */
// function readFromFile(fileName, formdata, subject) {
//     // myDebug('readFromFile (1) : ' + fileName);
//     var pathToFile = cordova.file.documentsDirectory + fileName;
//     window.resolveLocalFileSystemURL(fileName, function (fileEntry) {
//       // myDebug("readFromFile (2): " + JSON.stringify(fileEntry));
//       fileEntry.file(function (file) {
//         var reader = new FileReader();
//         var fileSize = file.size;
//         // myDebug("readFromFile (3) Size = " + file.size);

//         reader.onloadend = function (e) {
//           var fileContent = new Uint8Array(reader.result);
//           // var fileContent = reader.result;

//           formdata.append("afile", fileContent);
//           formdata.append("afilesize", fileSize);

//           //formdata.append("bfile", blob, file.name);
//           // myDebug('Taille du fichier image : ' + fileSize);
//           // myDebug('Fichier image : ' + fileName);

//           // -------------------------------------------------
//           //On sauvegarde le Fichier sous un nom qui sera plus facile a gerer par la suite
//           var today = new Date();
//           var destFilename = today.getFullYear().toString() + ('0' + (today.getMonth() + 1)).slice(-2) + ('0' + today.getDate()).slice(-2) + "_"
//             + today.getHours().toString() + today.getMinutes().toString() + "-" + subject + "-" + basename(fileName);
//           var newFileUri = cordova.file.documentsDirectory;
//           // myDebug('Sauvegarde de la photo dans (1): ' + newFileUri);
//           // myDebug('Sauvegarde de la photo dans (2): ' + destFilename);
//           moveFile(fileName, destFilename);
//           // -------------------------------------------------

//           //myDebug('Contenu du fichier : ' + fileContent);

//           //Appel a la suite de l'upload ici pour ne pas être coincé par la lecture async. de l'image
//           endSendForm(formdata, destFilename);
//         };

//         reader.readAsArrayBuffer(file);
//       }, errorHandler.bind(null, fileName));
//     }, errorHandler.bind(null, fileName));
//   }




/**
 * Autre idée pour une "popup" pour préciser la ville de depart lorsqu'on a des homonymes
 *
 * @param   {[type]}  result  [result description]
 *
 * @return  {[type]}          [return description]
 */
// function popupChoixDepart(result) {
//   myDebug('popupChoixDepart');

//   return new Promise((resolve, reject) => {
//     if (result.departsNb !== undefined) {
//       if (result.departsNb == 1) {
//         functionChooseDepart2(encodeHTMLEntities(result.depart) + " (" + result.departCP + ")", result.departSlug, result.id, true);
//       }
//       else {
//         let btn = [];
//         let btnNb = 0;
//         for (btnNb = 0; btnNb < result.length; btnNb++) {
//           btn.push({
//             label: encodeHTMLEntities(result[btnNb].depart) + " (" + result[btnNb].departCP + ")",
//             slug: result[btnNb].slug,
//             uid: result[btnNb].id
//           });
//         }
//         // btn.push({ label: 'Annuler', icon: 'md-close' });

//         if (globalMyNavigator !== undefined) {
//           resolve(
//             globalMyNavigator.pushPage("choix-liste.html", {
//               data: {
//                 title: "Précisez la ville de départ",
//                 callFunction: 'functionChooseDepart2',
//                 btn: btn,
//                 btnNb: btnNb,
//               },
//               animation: 'fade',
//             })
//           );
//         }
//         else {
//           myDebug('ERROR myNavigator does not exists !');
//           reject("ERROR myNavigator does not exists (2)");
//         }
//       }
//     }
//     else {
//       //Si pas de choix de ville de départ il a peut-être un choix sur les arrivés ...
//       myDebug('Pas de solution !');
//       // resolve(popupChoixArrivee(result));
//       reject("Pas de solution (2)");
//     }
//   });
// }

//Autre idée pour une popup pour préciser la ville d'arrivée lorsqu'on a des homonymes
// function popupChoixArrivee(result) {
//   return new Promise((resolve, reject) => {

//     if (result.arriveesNb !== undefined) {
//       if (result.arriveesNb == 1) {
//         functionChooseArrivee2(encodeHTMLEntities(result.arrivee) + " (" + result.arriveeCP + ")", result.arriveeSlug, true);
//       }
//       else {
//         let btn = [];
//         let btnNb = 0;
//         for (btnNb = 0; btnNb < result.arriveesNb; btnNb++) {
//           btn.push({
//             label: encodeHTMLEntities(result.arrivee[btnNb]) + " (" + result.arriveeCP[btnNb] + ")",
//             slug: result.arriveeSlug[btnNb]
//           });
//         }
//         // btn.push({ label: 'Annuler', icon: 'md-close' });

//         if (globalMyNavigator !== undefined) {
//           resolve(
//             globalMyNavigator.pushPage("choix-liste.html", {
//               data: {
//                 title: "Précisez la ville d'arrivée",
//                 callFunction: 'functionChooseArrivee2',
//                 btn: btn,
//                 btnNb: btnNb,
//               },
//               animation: 'fade',
//             })
//           );
//         }
//         else {
//           myDebug('ERROR myNavigator does not exists !');
//           reject("ERROR myNavigator does not exists (3)");
//         }
//       }
//     }
//   });
// }


/**
 * La popup pour choisir l'étape, considéré par le serveur comme la ville d'arrivée
 *
 * @param   {[type]}  result  [result description]
 *
 * @return  {[type]}          [return description]
 */
// function popupChoixEtape(result, etape, nb) {
//   myDebug('popupChoixEtape');

//   return new Promise((resolve, reject) => {
//     if (result.arriveesNb !== undefined) {
//       if (result.arriveesNb == 1) {
//         functionChooseArrivee2(encodeHTMLEntities(result.arrivee) + " (" + result.arriveeCP + ")", result.arriveeSlug, true);
//       }
//       else {
//         let btn = [];
//         let btnNb = 0;
//         for (btnNb = 0; btnNb < result.arriveesNb; btnNb++) {
//           btn.push({
//             label: encodeHTMLEntities(result.arrivee[btnNb]) + " (" + result.arriveeCP[btnNb] + ")",
//             slug: result.arriveeSlug[btnNb],
//             ville: etape,
//             villeSlug: etape + "Slug"
//           });
//         }
//         // btn.push({ label: 'Annuler', icon: 'md-close' });

//         if (globalMyNavigator === undefined) globalMyNavigator = document.getElementById('myNavigator');
//         if (globalMyNavigator !== undefined) {
//           resolve(
//             globalMyNavigator.pushPage("choix-liste.html", {
//               data: {
//                 title: "Précisez l'étape",
//                 callFunction: 'functionChooseEtape',
//                 btn: btn,
//                 btnNb: btnNb
//               },
//             })
//           );
//         }
//         else {
//           myDebug('ERROR myNavigator does not exists !');
//           reject("ERROR myNavigator does not exists (4)");
//         }
//       }
//     }
//     else {
//       //Si pas de choix de ville de départ il a peut-être un choix sur les arrivés ...
//       myDebug('Pas de solution !');
//       reject("pas de solution (4)");
//       // resolve(popupChoixArrivee(result));
//     }
//   });
// }

//Actualisation du champ "distance" du formulaire
// function functionChooseEtape(label, slug, fieldname, fieldnameSlug, single = false) {
//   myDebug("functionChooseEtape: " + label + " : " + slug + " : " + fieldname + ":" + fieldnameSlug + ":" + single + " start");
//   if (slug !== undefined) {
//     //Il faudrait donc aller chercher la val correspondante
//     $('#' + fieldname).val(decodeURIComponent(label));
//     $('#' + fieldnameSlug).val(slug);
//     // $('#btnCalculDistance').attr('style', "border-radius: 5px; border:#00FF00 2px solid;");
//     // $('#btnCalculDistanceArrivee').attr('style', "");
//     if (globalMyNavigator !== undefined) {
//       //Si on a une seule réponse et qu'on a fait un appel direct on ne pop pas la page
//       if (!single) {
//         let opt = { animation: 'fade' };
//         globalMyNavigator.popPage(opt);
//       }
//     }
//     // blinkElement("#btnCalculDistance", 3, 800);
//   }
//   else {
//     myDebug("functionChooseArrivee2: slug is undef !")
//   }
// }


//Actualisation du champ "distance" du formulaire
// function functionChooseArrivee2(label, slug, single = false) {
//   myDebug("functionChooseArrivee2: " + label + " : " + slug + " : " + single + " start");
//   if (slug !== undefined) {
//     //Il faudrait donc aller chercher la val correspondante
//     $('#arrivee').val(decodeURIComponent(label));
//     $('#arriveeSlug').val(slug);
//     $('#btnCalculDistance').attr('style', "border-radius: 5px; border:#00FF00 2px solid;");
//     $('#btnCalculDistanceArrivee').attr('style', "");
//     if (globalMyNavigator !== undefined) {
//       //Si on a une seule réponse et qu'on a fait un appel direct on ne pop pas la page
//       if (!single) {
//         let opt = { animation: 'fade' };
//         globalMyNavigator.popPage(opt);
//       }
//     }
//     blinkElement("#btnCalculDistance", 3, 800);
//   }
//   else {
//     myDebug("functionChooseArrivee2: slug is undef !")
//   }
// }

//Actualisation du champ "distance" du formulaire
// function functionChooseDepart2(label, slug, uid, single = false) {
//   if (slug !== undefined) {
//     //Il faudrait donc aller chercher la val correspondante
//     $('#depart').val(decodeURIComponent(label));
//     $('#departSlug').val(slug);
//     $('#departUID').val(uid);
//     $('#btnCalculDistanceArrivee').attr('style', "border-radius: 5px; border:#00FF00 2px solid;");
//     if (globalMyNavigator !== undefined) {
//       //Si on a une seule réponse et qu'on a fait un appel direct on ne pop pas la page
//       if (!single) {
//         let opt = { animation: 'fade' };
//         globalMyNavigator.popPage(opt);
//       }
//     }
//     blinkElement("#btnCalculDistanceArrivee", 3, 800);
//     // return true;
//   }
//   else {
//     myDebug("functionChooseDepart2: slug is undef !")
//     // return false;
//   }
//   //et maintenant on affiche la liste des arrivées possibles
//   // popupChoixArrivee(res);
// }


//Actualisation du champ "distance" du formulaire
// function functionChooseArrivee(nb, res) {
//   if (res.arrivee[nb] !== undefined) {
//     //Il faudrait donc aller chercher la val correspondante
//     $('#arrivee').val(res.arrivee[nb] + " (" + res.arriveeCP[nb] + ")");
//     $('#arriveeSlug').val(res.arriveeSlug[nb]);
//     $('#btnCalculDistance').attr('style', "border-radius: 5px; border:#00FF00 2px solid;");
//     blinkElement("#btnCalculDistance", 3, 800);
//   }
// }


//Actualisation du champ "distance" du formulaire
// function functionChooseDepart(nb, res) {
//   if (res.depart[nb] !== undefined) {
//     //Il faudrait donc aller chercher la val correspondante
//     $('#depart').val(res.depart[nb] + " (" + res.departCP[nb] + ")");
//     $('#departSlug').val(res.departSlug[nb]);
//     $('#btnCalculDistance').attr('style', "border-radius: 5px; border:#00FF00 2px solid;");
//     blinkElement("#btnCalculDistance", 3, 800);
//   }
//   //et maintenant on affiche la liste des arrivées possibles
//   popupChoixArrivee(res);
// }
