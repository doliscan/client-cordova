/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/


// ============================================================================================================= ville simple

/**
 * Recherche si une ville existe ... propose un choix le cas échéant
 *
 * @param   {string}  leform      formulaire html
 * @param   {string}  depart      point de depart
 * @param   {string}  departSlug  slug du point de depart
 *
 * @return  {string}              [return description]
 */
function searchVille(leform, ville, slug, uid, callbackfunction = null, callbackargs = null) {
  myDebug("searchVille : " + ville + " slug=" + slug + " uid=" + uid);
  let objForm = document.getElementById(leform);

  let d = $('#' + ville).val();

  showWait();
  $.ajaxSetup({
    timeout: 10000,
    headers: {
      'X-CSRF-TOKEN': localGetData("api_token")
    }
  });
  //Pour eviter les pb de persistance de donnée
  result = undefined;
  //au cas ou
  if (ajaxDistance)
    ajaxDistance.abort();

  let ajaxURL = localGetData("api_server") + "/api/geo/" + fixedEncodeURIComponent(d);
  var ajaxDistance = $.ajax({
    type: "GET",
    timeout: 15000,
    url: ajaxURL,
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
    },
    xhrFields: {
      withCredentials: true
    },
    success: function (result) {
      // myDebug('searchVille ajax ok1 ::: ' + JSON.stringify(result));
      if (result != undefined && Array.isArray(result)) {
        hideWait();
        if (result.length <= 0) {
          ons.notification.alert("Saisie manuelle nécessaire, cette ville n'est pas connue de nos serveurs", { title: "Erreur #1" });
        }
        else if (result.length == 1) {
          // myDebug('searchVille une seule réponse, on applique le contenu');
          functionChooseVille(encodeHTMLEntities(result[0].name) + " (" + result[0].zip_code + ")", result[0].slug, result[0].id, ville, slug, uid, true, callbackfunction, callbackargs);
          // $('#' + ville).val(result.depart + ' (' + result.departCP + ')');
          // $('#' + slug).val(result.departSlug);
        }
        else {
          popupChoixVille(result, ville, slug, uid, callbackfunction, callbackargs);
        }
      }
      else {
        hideWait();
        ons.notification.alert("Saisie manuelle nécessaire, cette ville n'est pas connue de nos serveurs", { title: "Erreur #2" });
      }
    },
    error: function (result) {
      myDebug('searchVille ajax erreur' + result);
      hideWait();
      ons.notification.alert("Saisie manuelle nécessaire, cette ville n'est pas connue de nos serveurs", { title: "Erreur #3" });
    }
  });

}


//Quand on est dans le choix d'une ville ce ne sont que des "départs"
function popupChoixVille(result, fieldVille, fieldVilleSlug, fieldVilleUID, callbackfunction = null, callbackargs = null) {
  myDebug('popupChoixVille pour ' + fieldVille + " -- " + fieldVilleSlug);

  return new Promise((resolve, reject) => {
    if (result.length > 0) {
      let btn = [];
      let btnNb = 0;
      for (btnNb = 0; btnNb < result.length; btnNb++) {
        btn.push({
          label: encodeHTMLEntities(result[btnNb].name) + " (" + result[btnNb].zip_code + ")",
          slug: result[btnNb].slug,
          uid: result[btnNb].id,
          ville: fieldVille,
          villeSlug: fieldVilleSlug,
          villeUID: fieldVilleUID,
          callbackfunction: callbackfunction,
          callbackargs: callbackargs
        });
      }
      // btn.push({ label: 'Annuler', icon: 'md-close' });

      if (globalMyNavigator !== undefined) {
        resolve(
          globalMyNavigator.pushPage("choix-liste.html", {
            data: {
              title: "Précisez la Ville",
              callFunction: 'functionChooseVille',
              btn: btn,
              btnNb: btnNb
            },
            animation: 'fade',
          })
        );
      }
      else {
        myDebug('ERROR myNavigator does not exists !');
        reject("ERROR myNavigator does not exists (1)");
      }
    }
    else {
      //Si pas de choix de ville de départ il a peut-être un choix sur les arrivés ...
      myDebug('Pas de solution !');
      reject("Pas de solution (1)");
    }
  });

}

//Actualisation du champ "distance" du formulaire
function functionChooseVille(label, slug, uid, fieldVille, fieldVilleSlug, fieldVilleUID, single = false, callbackfunction = null, callbackargs = null) {
  myDebug("functionChooseVille : " + label + " | " + slug + " | " + uid + " [" + fieldVille + " | " + fieldVilleSlug + " | " + fieldVilleUID);
  if (slug !== undefined) {
    //Il faudrait donc aller chercher la val correspondante
    $('#' + fieldVille).val(decodeURIComponent(label));
    $('#' + fieldVilleSlug).val(slug);
    $('#' + fieldVilleUID).val(uid);
    // $('#btnCalculDistanceArrivee').attr('style', "border-radius: 5px; border:#00FF00 2px solid;");
    if (globalMyNavigator !== undefined) {
      //Si on a une seule réponse et qu'on a fait un appel direct on ne pop pas la page
      if (!single) {
        let opt = { animation: 'fade' };
        globalMyNavigator.popPage(opt);
      }
    }
    // blinkElement("#btnCalculDistanceArrivee", 3, 800);
    // return true;
    if (callbackfunction != null) {
      // myDebug("Appel callback de " + callbackfunction);
      let fn = window[callbackfunction];
      if (fn) {
        myDebug("Appel callback via window global function ...");
        fn(callbackargs[0], callbackargs[1], callbackargs[2]);
      }
      else {
        myDebug("Appel callback direct  :" + callbackargs + " soit " + callbackargs[0] + " :: " + callbackargs[1] + " et " + callbackargs[2]);
        setTimeout(function () {
          callbackfunction(callbackargs[0], callbackargs[1], callbackargs[2]);
        }, 500);
      }
    }

  }
  else {
    myDebug("functionChooseVille: slug is undef !")
    // return false;
  }
}

// ============================================================================================================= étape


/**
 * Calcul de distance entre deux éléments, regroupe deux cas: le choix de la ville en cas d'hésitation et la recherche de la distance
 *
 * @param   {[type]}  leform  Le formulaire
 * @param   {[type]}  etape   L'etape en cours
 * @param   {[type]}  nb      Le numéro de l'étape de 1 à x
 *
 * @return  {[type]}          [return description]
 */
function distanceAutomatiqueEtape(leform, etape, nb) {
  let objForm = document.getElementById(leform);

  if (nb === undefined || nb == null || nb == '') {
    myDebug("distanceAutomatiqueEtape : nb undefined or null, early break");
    return;
  }
  myDebug("distanceAutomatiqueEtape : " + nb);

  let a = "null";
  let d = "null";

  //Cas particulier de la 1ere étape
  if (nb == 1) {
    //On privilégie les UID
    if (objForm.elements['departUID'].value != "") {
      d = objForm.elements['departUID'].value;
    }
    else {
      myDebug("distanceAutomatiqueEtape : depart brouillon");
      searchVille('leformIK', 'depart', 'departSlug', 'departUID', 'distanceAutomatiqueEtape', [leform, etape, nb]);
      return;
      // d = objForm.elements['depart'].value;
    }
  }
  else {
    //Les autres étapes ... le départ est le point étape précédent
    if (objForm.elements['etape' + (nb - 1) + 'UID'].value != "") {
      d = objForm.elements['etape' + (nb - 1) + 'UID'].value;
    }
    else {
      myDebug("distanceAutomatiqueEtape : etape " + (nb - 1) + " brouillon");
      searchVille('leformIK', 'etape' + (nb - 1), 'etape' + (nb - 1) + 'Slug', 'etape' + (nb - 1) + 'UID', 'distanceAutomatiqueEtape', [leform, etape, nb]);
      return;
      // d = objForm.elements['etape' + (nb - 1)].value;
    }
  }
  //l'arrivée c'est l'objet etape en cours
  if (objForm.elements['etape' + nb + 'UID'].value != "") {
    a = objForm.elements['etape' + nb + 'UID'].value;
  }
  else {
    myDebug("distanceAutomatiqueEtape : etape " + nb + " brouillon");
    searchVille('leformIK', 'etape' + nb, 'etape' + nb + 'Slug', 'etape' + nb + 'UID', 'distanceAutomatiqueEtape', [leform, etape, nb]);
    return;
    //    a = objForm.elements['etape' + nb].value;
  }

  showWait();
  $.ajaxSetup({
    timeout: 10000,
    headers: {
      'X-CSRF-TOKEN': localGetData("api_token")
    }
  });
  //Pour eviter les pb de persistance de donnée
  result = undefined;
  //au cas ou
  if (ajaxDistance)
    ajaxDistance.abort();

  let ajaxURL = localGetData("api_server") + "/api/geo/" + fixedEncodeURIComponent(d) + "/" + fixedEncodeURIComponent(a);

  var ajaxDistance = $.ajax({
    type: "GET",
    timeout: 5000,
    url: ajaxURL,
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
    },
    xhrFields: {
      withCredentials: true
    },
    success: function (result) {
      myDebug('distanceAutomatiqueEtape ajax ok1 / ' + nb + " ::: " + JSON.stringify(result));
      if (result !== undefined) {
        hideWait();
        if (result <= 0) {
          ons.notification.alert("Saisie manuelle nécessaire, cette ville n'est pas connue de nos serveurs", { title: "Erreur #1a" });
        }
        else {
          updateDistanceEtapeFromServer(result, etape, nb);
        }
      }
      else {
        hideWait();
        ons.notification.alert("Saisie manuelle nécessaire, cet itinéraire n'est pas connue sur nos serveurs", { title: "Erreur #1" });
      }
    },
    error: function (result) {
      myDebug('distanceAutomatiqueEtape ajax erreur' + JSON.stringify(result));
      hideWait();
      ons.notification.alert("Saisie manuelle nécessaire, cet itinéraire n'est pas connue sur nos serveurs", { title: "Erreur #2" });
    }
  });
}

//Actualisation du champ "distance" du formulaire
function updateDistanceFromServer(result) {
  $('#distance').val(result);
  $('#btnCalculDistance').attr('style', "");
}

function updateDistanceEtapeFromServer(result, etape, nb) {
  $('#etape' + nb + 'Distance').val(result);
  $('#distance').val();
  $('#btnCalculDistance').attr('style', "");
}

//Ajoute un itinéraire
function addFrequentRoute(storageKey, depart, arrivee, distance, label, etapes) {
  return new Promise((resolve, reject) => {
    //On cherche une place de libre (max 10 on a dit)
    let rNew = { "depart": depart, "arrivee": arrivee, "distance": distance, "label": label, "etapes": etapes };
    let rs = JSON.parse(localGetData(storageKey));
    if (rs === null) {
      rs = [];
    }
    rs.push(rNew);
    resolve(localStoreData(storageKey, JSON.stringify(rs)));
    //window.location.replace('config.html');
  });
}
