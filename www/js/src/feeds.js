/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

//Affiche la liste des vehicules à partir du contenu de la storageKey
// s'il y en a plusieurs: on affiche un choix
// et pas de choix (champ hidden) s'il n'y en a qu'un
//storageKey = vehicules ou vehiculesPro
function feedVehicules(storageKey) {
  // myDebug("feedVehicules key = " + storageKey);
  //          <label for=\"vehicule\">Véhicule</label>";
  let htmlCode = "";
  let html = "";
  let htmlSingleVehicule = "";
  let hasVehicule = false;
  let hasMultiVehicules = false;
  let v = localGetData(storageKey);
  if (v == null) {
    // myDebug("feedVehicules v is null");
    return;
  }
  vehicules = JSON.parse(v);

  for (let i = 0; i < vehicules.length && i < 4; i++) {
    // myDebug("feedVehicules vehicule #" + i);
    let vA = vehicules[i]['nom'];
    let vE = vehicules[i]['energie'];
    let vP = vehicules[i]['puissance'];
    let vT = vehicules[i]['type'];
    let vKM = vehicules[i]['kmbefore'];
    let vIMMAT = vehicules[i]['immat'];
    let vUUID = vehicules[i]['uuid'];
    let option = vA + ";" + vE + ";" + vP + ";" + vT + ";" + vKM + ";" + vIMMAT + ";" + vUUID;

    if (vA !== null) {
      //Le 1er est checked (sauf si dataLDF voir plus loin)
      let checked = " checked";
      //Si jamais cette boucle a déjà fait un tour on ne coche pas les suivants
      if (hasVehicule == true) {
        checked = "";
      }
      //Si jamais on est en update on connait le uuid du vehicule -> c'est donc lui qui sera coché
      if (dataLDF) {
        if (dataLDF['vehicule_uuid'] == vUUID) {
          checked = " checked";
        }
        else {
          checked = "";
        }
      }
      // html += ("<option value=\"" + option + "\">" + vA + "</option>");
      html += "<div class=\"segment__item\">";
      html += "  <input type=\"radio\" name=\"vehicule\" id=\"vehicule-" + i + "\" value=\"" + option + "\"" + checked + " class=\"segment__input\" />";
      html += "  <div class=\"segment__button\">" + vA + "</div>";
      html += "</div>";
      htmlSingleVehicule = option;
      //Si il y en a déjà au moins un alors multiple :=)
      if (hasVehicule == true) {
        hasMultiVehicules = true;
      }
      hasVehicule = true;
    }
  }

  if (hasVehicule == false) {
    htmlCode = "<p id=\"vehicule\">Vous n'avez pas encore de véhicule, veuillez <a href=\"#\" onclick=\"gotoPage('config.html', false);\">configurer vos véhicules</a></p>";
  } else {
    // if (hasMultiVehicules == true) {
    htmlCode += "<fieldset data-role=\"controlgroup\" data-type=\"horizontal\" data-mini=\"true\">";
    htmlCode += "<legend>Choix du véhicule :</legend>";
    htmlCode += "  <div class=\"segment\" style=\"width:100%; margin: 0 auto;\">";

    htmlCode += html;

    htmlCode += "<div class=\"segment__item\">";
    htmlCode += "  <input type=\"radio\" name=\"vehicule\" id=\"vehicule-ajout\" value=\"\" class=\"segment__input\" onclick=\"gotoPage('config.html', false);\" />";
    htmlCode += "  <div class=\"segment__button\"> <i class=\"fa fa-pencil\"></i> </div>";
    htmlCode += "</div>";


    htmlCode += "</div>";
    htmlCode += "</fieldset>";
    // }
    // else {
    //   //Un seul véhicule on ne l'affiche pas
    //   htmlCode = "<input type=\"hidden\" name=\"vehicule\" id=\"vehicule\" value=\"" + htmlSingleVehicule + "\">";
    // }
  }
  // myDebug("feedVehicules, retour " + htmlCode);

  return htmlCode;
}

//On insere les donnnes dans l'objet formField a partir de ce qui est contenu dans storageKey
function feedVehiculesConfig(formField, storageKey) {
  $(formField).empty();
  let v = localGetData(storageKey);
  if (v !== null) {
    let vehicules = JSON.parse(v);
    let html = "<ul>";
    for (var i = 0; i < vehicules.length && i < 4; i++) {
      let vA = vehicules[i]['nom'];
      let vE = vehicules[i]['energie'];
      let vP = vehicules[i]['puissance'];
      let vT = vehicules[i]['type'];
      let vKM = vehicules[i]['kmbefore'];
      let vIMMAT = vehicules[i]['immat'];
      if (vIMMAT === undefined)
        vIMMAT = "";

      if (vA !== null) {
        html += "<li>" + vA + " [" + vIMMAT + "] " + " (" + vT + " " + vE + " " + vP + ") <br /> <span style=\"text-align: right; display: block;\"><a href=\"#\" onclick=\"rmVehicule('" + storageKey + "'," + i + ");feedVehiculesConfig('" + formField + "','" + storageKey + "');\">Supprimer</a> - <a href=\"#\" onclick=\"edVehicule('" + storageKey + "'," + i + ");feedVehiculesConfig('" + formField + "','" + storageKey + "');\">Modifier</a></span></li>\n";
      }
    }
    html += "</ul>";
    $(formField).append(html);
  }
}

/**
 * This function will draw the given path.
 */
function listPath(myPath) {
  if (myPath == null)
    return;

  globalPicturesAvailable.splice(0, globalPicturesAvailable.length)

  myDebug("listPath pour " + myPath);
  window.resolveLocalFileSystemURL(myPath, function (dirEntry) {
    myDebug("  listPath resolve ok " + myPath);
    var directoryReader = dirEntry.createReader();
    directoryReader.readEntries(onSuccessListCallback, onFailListCallback);
  });

  function onSuccessListCallback(entries) {
    myDebug("  listPath callback ok");
    let html = '';
    for (i = 0; i < entries.length; i++) {
      var row = entries[i];
      if (row.isDirectory) {
        // We will draw the content of the clicked folder
        // html = '<li onclick="listPath(' + "'" + row.nativeURL + "'" + ');">' + row.name + '</li>';
        myDebug("  listPath new dir " + row.nativeURL);
      } else {
        // alert the path of file
        // html = '<li onclick="getFilepath(' + "'" + row.nativeURL + "'" + ');">' + row.name + '</li>';
        globalPicturesAvailable.push(row.nativeURL);
        myDebug("  listPath new file " + row.nativeURL);
      }
      // $("#listPath").append(html);
    }
  }

  function onFailListCallback(e) {
    myDebug("  listPath callback error");
    console.error(e);
    // In case of error
  }
}

function getFilepath(thefilepath) {
  alert(thefilepath);
}


//Liste des facturettes en attente de synchronisation ...
//note: fichier sauvegardé
// -iPhone file:///var/mobile/Containers/Data/Application/E8013039-4FC0-4460-BE86-1372E90EFFCC/tmp/cdv_photo_001.jpg -> 20210405_1918--cdv_photo_001.jpg
function feedToSync(formField) {
  let htmlFTS = "";
  // myDebug("feedToSync pour " + formField);
  $(formField).empty();
  if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
    // myDebug("ldfsToSync : select * from ldfsToSync");

    let objectStore = globalIndexedDB.transaction("ldfsToSync").objectStore("ldfsToSync", "readwrite");
    objectStore.openCursor().onsuccess = function (event) {

      var cursor = event.target.result;
      if (cursor) {
        let nbRes = 0;
        let iFTS = 0;

        myDebug("feedToSync " + cursor.key + " is " + JSON.stringify(cursor.value));
        let id = cursor.key;
        let v = cursor.value;
        //Mise a jour uniquement si non vide
        if (v !== null && v != "") {
          let url = v.url;
          let jsonData = v.jsonData;
          let localFileName = v.localFileName;
          let data = JSON.parse(jsonData);
          myDebug('  feedToSync ' + id + ' -> ' + v);

          myDebug("  ldfsToSync res " + iFTS + " : " + id + " / " + jsonData);

          htmlFTS += "<div style=\"clear:both\">" + id + ": " + data.typeFrais + " du " + data.ladate + " (" + data.label + ", " + nbFR(data.ttc) + " ) <br /><div style=\"float:right\"><ons-button modifier=\"material\" onclick=\"rSyncOne(" + id + ");\">Synchroniser</ons-button> <ons-button modifier=\"material\" onclick=\"rSyncDeleteConfirm(" + id + ");\">Supprimer</ons-button></div></div><hr />\n";
        }
        else {
          myDebug('  feedToSync ' + id + ' -> null ');
        }
        iFTS++;
        cursor.continue();
        // myDebug("  ldfsToSync res " + iFTS + " : " + id + " : " + data.ladate);
      }
      else {
        myDebug("  feedToSync no more entries!");
        // myDebug("  ldfsToSync fin de la boucle");
        $(formField).append(htmlFTS);
      }

    };
    objectStore.openCursor().onerror = function (event) {
      myDebug("  feedToSync Error " + error.message);
    }
    myDebug("  feedToSync : read OK, append HTML done");
  }

  // globalDB.readTransaction(function (t) {
  //   t.executeSql('SELECT * FROM ldfsToSync', [],
  //     function (tx, rs) {

  //       // myDebug("  ldfsToSync -> SQL SELECT * OK: res " + rs.rows.length);
  //       let nbRes = rs.rows.length;
  //       let iFTS = 0;
  //       for (iFTS = 0; iFTS < nbRes; iFTS++) {
  //         let id = rs.rows.item(iFTS).id;
  //         let url = rs.rows.item(iFTS).url;
  //         let jsonData = rs.rows.item(iFTS).jsonData;
  //         let localFileName = rs.rows.item(iFTS).localFileName;

  //         let data = JSON.parse(jsonData);
  //         // myDebug("  ldfsToSync res " + iFTS + " : " + id + " / " + jsonData);

  //         htmlFTS += "<div style=\"clear:both\">" + id + ": " + data.typeFrais + " du " + data.ladate + " (" + data.label + ", " + nbFR(data.ttc) + " ) <br /><div style=\"float:right\"><ons-button modifier=\"material\" onclick=\"rSyncOne(" + id + ");\">Synchroniser</ons-button> <ons-button modifier=\"material\" onclick=\"rSyncDeleteConfirm(" + id + ");\">Supprimer</ons-button></div></div><hr />\n";
  //         // myDebug("  ldfsToSync res " + iFTS + " : " + id + " : " + data.ladate);
  //       }
  //       // myDebug("  ldfsToSync fin de la boucle");
  //       $(formField).append(htmlFTS);
  //     },
  //     function () {
  //       // myDebug("  ldfsToSync -> SQL Erreur de SELECT * FROM ldfsToSync");
  //     });
  // });
  // }
  myDebug("end feedToSync ..." + htmlFTS);
  return htmlFTS;
}

//Ajout du message custom envoyé par le serveur sur la boite à propos
function feedAproposCustom() {
  myDebug("feedAproposCustom");
  $("#feedAproposCustom").empty();
  myDebug("feedAproposCustom empty ok -> feed with html now");

  $("#feedAproposCustom").append(localGetData('customAbout'));
  return;
}

/**
 *
 * @param {boolean} ispro true if is pro
 * @returns
 */
function feedKMBefore(ispro) {
  myDebug("feedKMBefore");
  $("#feedKMBefore").empty();

  let html = "  <ons-row>";
  html += "    <ons-col>";
  //uniquement pour les vehicules perso
  if (ispro) {
    html += "      <input type=\"hidden\" name=\"kmbefore\" id=\"kmbefore\" value=\"0\">";
  }
  else {
    html += "      <label for=\"kmbefore\">Kilométrage réalisé cette année dans le cadre de votre travail avec ce véhicule avant l'utilisation de doliscan (c'est pour pouvoir établir la base de calcul des indemnités qui doit prendre en compte le total de l'année civile en cours)</label>";
    html += "      <ons-input name=\"kmbefore\" id=\"kmbefore\" type=\"number\" placeholder=\"km\"></ons-input>";
  }
  html += "    </ons-col>";
  html += "  </ons-row>";
  $("#feedKMBefore").append(html);
  return;
}

//Ajoute quelques informations
function feedDoliscanInfos() {
  let htmlInfos = "";

  myDebug("feedDoliscanInfos");
  $("#feedDoliscanInfos").empty();
  myDebug("feedDoliscanInfos empty ok -> feed with html now");

  htmlInfos += "<p>";
  htmlInfos += "Compte " + localGetData("email") + "<br />";
  htmlInfos += "Serveur " + localGetData("name_server") + "<br />";
  htmlInfos += "Version " + globalAppVersion + "<br />";
  htmlInfos += "</p>";

  $("#feedDoliscanInfos").append(htmlInfos);
  myDebug("feedDoliscanInfos append: " + htmlInfos);
  myDebug("feedDoliscanInfos append, hide and show ok");
  return htmlInfos;
}

//Ajoute l'adresse mail du compte
function feedDoliscanAccount() {
  let htmlInfos = "";
  $("#feedDoliscanAccount").empty();
  htmlInfos += translateStrings('account') + " : " + localGetData("email");

  $("#feedDoliscanAccount").append(htmlInfos);
  myDebug("feedDoliscanAccount append: " + htmlInfos);
  return htmlInfos;
}

//Liste des rdv de l'agenda qu'on peut transformer automatiquement en IK
function feedToAgendaIK(formField, dateStart, dateEnd) {
  let htmlFTS = "";
  myDebug("feedToAgendaIK pour " + formField);
  $(formField).empty();

  showWait();
  // ons.notification.alert("Récupération des rdv, patientez quelques secondes ...", { title: "Analyse en cours" });
  var title = null;
  var loc = null;
  var notes = null;
  var startDate = new Date(dateStart); //"December 10, 2018 00:00:00"
  var endDate = new Date(dateEnd);
  // alert('Calendar search: ' + dateStart + " (" + startDate + ") - " + dateEnd + " (" + endDate + ")");
  window.plugins.calendar.findEvent(title, loc, notes, startDate, endDate, onSuccessAgendaIK, onErrorAgendaIK);
  myDebug("end feedToAgendaIK ..." + htmlFTS);
}

function onSuccessAgendaIK(events) {
  myDebug("onSuccessAgendaIK ...");
  hideWait();

  var formField = '#feedToAgendaIK';
  var html = "";
  $(formField).empty();

  if (events.length == 0) {
    // TODO
    myDebug("onSuccessAgendaIK aucun rdv sur cette periode");
    html += "<h2>Aucun rdv sur cette période</h2>";
    $(formField).append(html);
    return;
  }

  let depart = $("#departAgendaIK").val();
  if (typeof depart == 'undefined' || depart == "") {
    depart = "Ales";
    ons.notification.alert("N'oubliez pas de spécifier une ville de départ !", { title: "Hum !" });
  }

  // Find events
  for (var i = 0; i < events.length; i++) {
    myDebug("onSuccessAgendaIK event finded !");
    //Si on a le hashtag #ik ... ou qu'on a un emplacement géographique de déterminé
    if (events[i].title.includes("#ik") || (typeof events[i].location != 'undefined')) {
      let titre = events[i].title.replace('#ik', '');
      //Split pour garder uniquement le jour, plus tard on aura peut-être besoin de l'heure pour
      //la gestion des tournées (exemple: 3 rdv à la suite)
      let startDate = events[i].startDate.split(' ')[0];
      let msg = "";
      let loc = "";

      html += "<h2>" + titre + "</h2>";
      html += "<ul><li>Date : " + startDate + "</li>";
      if (events[i].message) {
        msg = events[i].message;
        html += "<li>Détails: " + msg + "</li>";
      }
      if (events[i].location) {
        loc = events[i].location;
        html += "<li>Lieu : " + loc + "</li>";
      }
      html += "</ul>";

      myDebug("onSuccessAgendaIK add entry : titre=" + titre + " msg=" + msg + " loc=" + loc + " depart=" + depart);

      //html += "<div id='ajouterFrais-" + i + "' onclick='createIK(" + events[i] + ")'>Créer une fiche d'IK</div>";
      html += "<ons-button modifier='material large' onclick='createIK(\""
        + fixedEncodeURIComponent(titre.trim()) + "\",\""
        + fixedEncodeURIComponent(startDate) + "\",\""
        + fixedEncodeURIComponent(msg.trim()) + "\",\""
        + fixedEncodeURIComponent(loc.replace(/France/i, '').trim()) + "\",\""
        + fixedEncodeURIComponent(depart.trim()) + "\""
        + ");' class='menubutton'><i class='zmdi zmdi-car'></i> Créer la fiche d'IK</ons-button>";
    }
  }
  $(formField).append(html);

  myDebug('Calendar success: ' + JSON.stringify(events));
  // ons.notification.alert("Récupération de l'agenda OK", { title: "Agenda" });
}

function onErrorAgendaIK(msg) {
  myDebug("onErrorAgendaIK ...");
  hideWait();
  // alert('Calendar error: ' + JSON.stringify(msg));
  ons.notification.alert("Erreur d'accès à l'agenda" + JSON.stringify(msg), { title: "Erreur" });
}

//On complète la fiche d'IK avec les informations dont on dispose
function createIK(titre, ladate, message, arrivee, depart) {
  myDebug("createIK : " + titre);
  dataIK = {
    label: titre + " " + message,
    ladate: ladate,
    arrivee: arrivee,
    depart: depart,
  };
  myDebug("createIK : avant le Goto : " + JSON.stringify(dataIK));
  gotoPage("frais-ik.html", true);
}

function makeOneAccount(fieldName, id, title, mail) {
  let html = "";
  html = "<ons-row>\n";
  html += "  <ons-col>\n";
  html += "     <span class=\"deleteiconAccount\">\n";
  html += "        <span class=\"deleteiconAccount2\" onclick=\"deletePrefAccount('" + id + "','" + fieldName + "');\"><i style=\"font-size: x-large;\" class=\"zmdi zmdi-close-circle-o\"></i></span>\n";
  html += "      </span>\n";
  html += "      <ons-card>\n";
  html += "          <ons-row>\n";
  html += "              <ons-col width=\"20%\">\n";
  html += "                     <ons-icon icon=\"fa-user-tie\" size=\"50px\"></ons-icon>\n";
  html += "              </ons-col>\n";
  html += "              <ons-col style=\"text-align: center;\">\n";
  html += "                  <div class=\"title\">\n";
  html += "                      " + title + "\n";
  html += "                  </div>\n";
  html += "              </ons-col>\n";
  html += "          </ons-row>\n";
  html += "          <ons-row>\n";
  html += "              <ons-col style=\"text-align: center;\">\n";
  html += "                  <ons-button modifier=\"material large\" onclick=\"loginAPIKeyErrorInProgress=false;loginWithAPIToken(" + id + ");\" class=\"menubutton\">\n";
  html += "                      <ons-icon icon=\"md-sign-in\"></ons-icon>\n";
  html += "                      <span class=\"menulabel translate\">AUTH</span>\n";
  html += "                  </ons-button>\n";
  html += "              </ons-col>\n";
  html += "          </ons-row>\n";
  html += "      </ons-card>\n";
  html += "  </ons-col>\n";
  html += "</ons-row>\n";
  return html;
}

function deletePrefAccount(id, fieldName) {
  let c = JSON.parse(localGetData("accounts"));
  c.splice(id, 1);
  if (c.length > 0) {
    localStoreData("accounts", JSON.stringify(c));
  }
  else {
    localStoreData("accounts", null);
  }
  feedMyAccounts(fieldName);
}

function feedMyAccounts(fieldName) {
  myDebug("appel de feedMyAccounts :: " + fieldName);
  let formField = '#' + fieldName;
  let html = "";
  $(formField).empty();
  let a = localGetData("accounts");
  myDebug("on a feedMyAccounts :: " + a);
  if (a !== null && a !== "") {
    myDebug("on a feedMyAccounts non null ... ");
    let accounts = JSON.parse(a);
    for (let f = 0; f < accounts.length; f++) {
      lemail = accounts[f]['email'];
      html += makeOneAccount(fieldName, f, lemail, lemail);
    }
  }

  $(formField).append(html);
}


function feedAgendaList() {
  window.plugins.calendar.listCalendars(listAgendaSuccess, listAgendaError);
}

function listAgendaError(message) {
  myDebug("listCalendarsError: " + message);
}

function listAgendaSuccess(agendas) {
  myDebug("appel de listAgendaSuccess");
  let formField = "#feedAgendaList";
  $(formField).empty();
  let html = "<ons-select id=\"chooseAgenda\" onchange=\"editSelectsListAgenda(event)\">\n";
  if (agendas !== null && agendas !== "") {
    myDebug("on a listAgendaSuccess non null ... ");
    for (let f = 0; f < agendas.length; f++) {
      let lid = agendas[f]['id'];
      let lname = agendas[f]['displayname'];

      //Sur ios on utilise le nom de l'agenda et pas son id
      if (device.platform == "iOS") {
        lid = lname;
      }

      html += "<option value=\"" + lid + "\">" + lname + "</option>\n";
    }
  }
  html += "</ons-select>";
  $(formField).append(html);
}


/**
 * complète la liste automatiquement
 */
function feedList(fieldName, sourceListName, search = "") {
  myDebug("feedList : " + fieldName);

  let formField = '#' + fieldName;
  let html = "";
  let strRegEx = new RegExp(search, 'i');
  $(formField).empty();

  let thelist = JSON.parse(localGetData(sourceListName));
  if (thelist !== null) {
    thelist.sort(function (a, b) {
      return a.label.localeCompare(b.label);
    });
    //On re-sauvegarde pour avoir la liste triée et donc les indices correspondants (pour del/update)
    localStoreData(sourceListName, JSON.stringify(thelist));

    myDebug("Liste non vide : " + thelist.length);
    for (let f = 0; f < thelist.length; f++) {
      let ltitle = thelist[f].label;
      // let llabel = thelist.arrayData[f][''];
      let llabel = "";
      if (search == "") {
        myDebug("Search vide, ajout d'une entree : " + ltitle + " / " + llabel);
        html += makeOneListEntry(f, thelist[f], sourceListName);
      }
      else {
        myDebug("Vérifie si l'entrée matche avec la recherche ... " + ltitle + " search " + strRegEx);
        if (ltitle.match(strRegEx)) {
          myDebug("Ca matche ! Ajout d'une entree : " + ltitle + " / " + llabel);
          html += makeOneListEntry(f, thelist[f], sourceListName);
        }
      }
    }
  }
  // myDebug("Ajout du code HTML : " + html);

  $(formField).append(html);
}


/**
 * Ajoute une entrée à la liste
 */
function makeOneListEntry(id, obj, sourceListName) {
  let h = "";
  let val = btoa(JSON.stringify(obj));
  // if (typeof l_subtitle === undefined || l_subtitle == "undefined" || l_subtitle == "") {
  //ok            h += "<ons-list-item tappable onclick='listChooseVal(\"" + id + "\", \"" + l_value + "\")'>" + l_title + "</ons-list-item>\n";

  let nextFunction = '';

  let dist = "";
  //cas particulier des ik
  if (obj.distance !== undefined && obj.distance > 0) {
    dist = " (" + obj.distance + " km)";
    nextFunction = "applyIKSaved();";
  }

  let code = null;
  if(obj.code) {
    code = obj.code;
  }

  //tentative avec checkbox
  h += "<ons-list-item tappable onclick=\"listChooseVal('" + id + "','" + sourceListName + "','" + code + "');" + nextFunction + "\">\n";
  h += "      <label class=\"left\"><ons-radio name=\"routeSelected\" input-id=\"radio-" + id + "\" value=\"" + val + "\"></ons-radio></label>\n";
  h += "      <label for=\"radio-" + id + "\" class=\"center\"><span id=\"txtvalue-" + id + "\">" + obj.label + dist + "</span></label>\n";
  h += "</ons-list-item>\n";

  // h += "<ons-list-item tappable onclick='listChooseVal(\"" + id + "\", \"" + l_value + "\")'>" + l_title + "</ons-list-item>\n";
  // }
  return h;
}

/**
 * Clic sur un element de la liste de choix 
 */
function listChooseVal(id, sourceListName, code) {
  let val = $('#txtvalue-' + id).text();
  let checkedBoxes = document.querySelectorAll('ons-list-item.expanded');
  //Si c'est une liste repliable, lancer le replis
  if (checkedBoxes.length > 0) {
    checkedBoxes[0].hideExpansion();
  }
  //S'il y a du texte a afficher dans l'entrée de base
  if ($('#labelChooseTag')) {
    $('#labelChooseTag').text(val);
  }

  //S'il y a un code il faut le stocker qqpart pour le joindre au submit du form
  if(sourceListName == 'tags') {
      globalTagFrais = code;
  }
}

