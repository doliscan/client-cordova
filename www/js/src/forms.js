/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/


/* *************************************
* Pousse le formulaire vers le stockage serveur
* leformName = nom du formulaire dans lequel il faut aller chercher les infos
*************************************** */
function sendForm(leformName) {
  myDebug("sendForm " + leformName);
  showWait();

  $.ajaxSetup({
    timeout: 10000,
    headers: {
      'X-CSRF-TOKEN': localGetData("api_token")
    }
  });

  //Sauvegarde la date pour la proposer par défaut au prochain scan
  if (document.getElementById('ladate')) {
    myDebug("on a ladate : " + document.getElementById('ladate').value);
    localStoreData("ladate", document.getElementById('ladate').value);
  }

  //Sauvegarde le moyen de paiement pour le proposer par défaut au prochain scan
  moyenPaiement = $("input:radio[name='moyenPaiement']").filter('input:checked').val();
  myDebug("on a moyenPaiement : " + moyenPaiement);
  localStoreData("moyenPaiement", moyenPaiement);
  var leform = document.getElementById(leformName);
  myDebug("formdata tout neuf");
  // myDebug(leform);
  var formdata = new FormData(leform);
  myDebug(formdata);
  formdata.append('_token', localGetData("api_token"));

  //tag frais à ajouter (injecter a partir de la valeur globalTagFrais, voir func listChooseVal)
  if (null !== globalTagFrais) {
    formdata.append('tag_frais_code', globalTagFrais);
  }

  //Si on est en UPDATE, pas besoin de l'image
  if (document.getElementById('updateID').value == 0) {
    let photoFacturetteValue = $('#photoFacturette').val();
    myDebug("on est pas en update, photoFacturette = " + photoFacturetteValue + " et globalPictureFileName=" + globalPictureFileName);

    //En mode dev on ne demande pas forcément la photo
    //et si c'est un pdf photoFacturette = pdf
    if (globalPictureFileName && !globalDevMode) {
      //cas particulier des ik
      if(globalPictureFileName == 'ik') {
        globalPictureFileName = '';
      }
      myDebug("sendForm fileImage : " + globalPictureFileName);
      //On part dans la fonction dont la lecture est async

      let typeFraisObj = document.getElementById(leformName).typeFrais;
      myDebug("sendForm  typeFraisObj: " + typeFraisObj.value);
      globalTypeFrais = "autre";
      if (typeFraisObj !== null) {
        globalTypeFrais = typeFraisObj.value;
      }
      //On bypass pour resumable.min.js
      //readFromFile(file, formdata, typeFrais);
      myDebug("appel de endSendForm ... (ligne 65) fic = " + globalPictureFileName);
      endSendForm(formdata, globalPictureFileName);
    }
    else {
      hideWait();
      // myDebug(formdata);
      ons.notification.alert("Photo du justificatif introuvable !", { title: "Attention" });
      myDebug("appel de endSendForm ... pas d'image (fic vide)");
      endSendForm(formdata, "");
    }
  }
  else {
    //en mode update
    myDebug("On n'essaye pas de joindre le fichier on est en UPDATE")
    // myDebug(formdata);
    endSendFormForUpdate(formdata, leformName);
  }
}

/**
 * On ne fait qu'un update de la facturette ...
 *
 * @param   {[type]}  formdata    le formdata qu'on envoi
 * @param   {[type]}  leformName  nom du formulaire
 *
 * @return  {[type]}              [return description]
 */
function endSendFormForUpdate(formdata, leformName) {

  //Transformer les données du formulaire en JSON
  var objectTempToConvertFD = {};
  formdata.forEach((value, key) => {
    if (key == "afile") {
      //nothing, on vire le fichier joint du blob pour le json :)
    }
    else if (key == "tvaTx1" || key == "tvaTx2" || key == "tvaTx3" || key == "tvaTx4") {
      //On retransforme les taux de tva de leur indice vers la bonne valeur pour ne pas avoir
      //a toucher au code du serveur ...
      let keyTaux = "compta_tva_tx" + value;
      let valValue = localGetData(keyTaux);
      if (!Array.isArray(objectTempToConvertFD[key])) {
        objectTempToConvertFD[key] = [objectTempToConvertFD[key]];
      }
      objectTempToConvertFD[key] = valValue;
      formdata.set(key, valValue);
      myDebug("on est sur une clé TVA: " + key + " val " + value + " set " + valValue);
    }
    else {
      // Reflect.has in favor of: object.hasOwnProperty(key)
      if (!Reflect.has(objectTempToConvertFD, key)) {
        objectTempToConvertFD[key] = value;
        return;
      }
      if (!Array.isArray(objectTempToConvertFD[key])) {
        objectTempToConvertFD[key] = [objectTempToConvertFD[key]];
      }
      objectTempToConvertFD[key].push(value);
    }
  });
  //Le moyen de paiement est un peu particulier...
  objectTempToConvertFD['moyenPaiement'] = $("input:radio[name='moyenPaiement']").filter('input:checked').val();
  formdata.set('moyenPaiement', $("input:radio[name='moyenPaiement']").filter('input:checked').val());

  var formDataInJSON = JSON.stringify(objectTempToConvertFD);
  myDebug("endSendFormForUpdate JSON : " + leformName + " :: " + formDataInJSON);

  //remettre à zéro la structure de données provoquant l'accès au mode "mise à jour"
  dataLDF = "";
  if (ajaxRequestUpdate)
    ajaxRequestUpdate.abort();
  var ajaxRequestUpdate = $.ajax({
    url: localGetData("api_server") + "/api/LdeFrais/" + document.getElementById('updateID').value,
    timeout: 10000,
    type: "PUT",
    data: formDataInJSON,
    processData: true,
    cache: false,
    contentType: 'application/json',
    tryCount: 0,
    retryLimit: 3,
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
    },
    xhrFields: {
      withCredentials: true
    },
    success: function (result, textStatus, request) {
      globalNetworkResponse = Date.now();

      hideWait();
      myDebug("Request PUT finished (success signal) : " + textStatus + '(' + result.length + ')');
      myDebug("Request PUT status (success signal) : " + request.status);
      //Cote serveur on a un code 201 "special" dans ce cas
      if (request.status == "200") {
        gotoPage('messageDone.html', false);
      }
      else {
        ons.notification.alert("Le serveur n'est pas accessible. Votre document est enregistré sur votre téléphone, pensez à le synchroniser avec le serveur dès que vous retrouverez une bonne connexion (utilisez le menu synchronisation à ce moment là).", { title: "Réseau lent (1)" }).then(function () {
          gotoPage("menu.html", true);
        });
      }
    },
    error: function (request, textStatus, errorThrown) {
      hideWait();
      myDebug("Request finished (error signal) : " + textStatus);
    },
    // complete: function (request, textStatus, errorThrown) {
    //   myDebug("Request PUT finished (complete signal) : " + textStatus);
    //   myDebug('Taille des donees envoyees sur le serveur : ' + request.getResponseHeader('Content-Length') + ' octets');
    //   if (request.status == "200") {
    //     gotoPage('messageDone.html', false);
    //   }
    //   else {
    //     ons.notification.alert("Le serveur n'est pas accessible. Votre document est enregistré sur votre téléphone, pensez à le synchroniser avec le serveur dès que vous retrouverez une bonne connexion (utilisez le menu synchronisation à ce moment là).", { title: "Réseau lent (2)" });
    //     gotoPage('menu.html', false);
    //   }
    // }
  });
}

/**
 * 
 * @param {*} formdata : les données du formulaire
 * @param {*} filename : le nom du fichier
 * @param {*} newOrSync : un mot clé pour savoir si on est en mode "sync" (re-upload d'une pièce justificative) ou "new" (nouveau frais)
 * @param {*} syncLdfID : dans le cas d'une sync, l'id du frais à synchroniser
 */
function endSendForm(formdata, filename, newOrSync = "new", syncLdfID = null) {
  let subject = "";
  myDebug('endSendForm ... avec fic = ' + filename + ' mais sur ios on utilisera plutot globalPictureFileName = ' + globalPictureFileName);
  //TODO A verifier
  if (device !== undefined) {
    if (device.platform == "iOS") {
      if (globalPictureFileName != "")
        filename = globalPictureFileName;
    }
  }
  else {
    myDebug("device is undef !");
  }
  if (filename == "" || filename.startsWith("data:image")) {
    filename = globalPictureFileName;
  }
  //Transformation du formdata en json pour stockage en sql
  var objectTempToConvertFD = {};
  formdata.forEach((value, key) => {
    if (key == "afile") {
      //nothing, on vire le fichier joint du blob pour le json :)
    }
    else if (key == "tvaTx1" || key == "tvaTx2" || key == "tvaTx3" || key == "tvaTx4") {
      myDebug("on est sur une clé TVA: " + key + " val " + value);

      //On retransforme les taux de tva de leur indice vers la bonne valeur pour ne pas avoir
      //a toucher au code du serveur ...
      let keyTaux = "compta_tva_tx" + value;
      let valValue = localGetData(keyTaux);
      if (!Array.isArray(objectTempToConvertFD[key])) {
        objectTempToConvertFD[key] = [objectTempToConvertFD[key]];
      }
      objectTempToConvertFD[key].push(valValue);
      formdata.set(key, valValue);
    }
    else {
      // Reflect.has in favor of: object.hasOwnProperty(key)
      if (!Reflect.has(objectTempToConvertFD, key)) {
        objectTempToConvertFD[key] = value;
        return;
      }
      if (!Array.isArray(objectTempToConvertFD[key])) {
        objectTempToConvertFD[key] = [objectTempToConvertFD[key]];
      }
      objectTempToConvertFD[key].push(value);
    }
  });

  //Le moyen de paiement est un peu particulier...
  formdata.set('moyenPaiement', $("input:radio[name='moyenPaiement']").filter('input:checked').val());
  objectTempToConvertFD['moyenPaiement'] = $("input:radio[name='moyenPaiement']").filter('input:checked').val();

  var formDataInJSON = JSON.stringify(objectTempToConvertFD);

  //debug du formdata dans la console
  // for (var pair of formdata.entries()) {
  //   myDebug(pair[0] + ': ' + pair[1]);
  //   if (pair[0] == "label")
  //     subject = pair[1];
  // }

  //On sauvegarde dans la base pour pouvoir faire une synchro plus tard si jamais ca a foiré
  //Bien entendu on ne fait pas ça si on est en cours de synchronisation :)

  if (globalIndexedDB !== null && databaseEngine == 'IndexedDB' && globalSyncInProgress == false) {
    let url = localGetData("api_server") + "/api/LdeFrais";
    localStoreLDFtoSync(url, formDataInJSON, filename).then((value) => {
      globalLastInsertId = value;
      myDebug("  endSendForm retour de localStoreLDFtoSync : globalLastInsertId=" + globalLastInsertId);
    });
  }
  else {
    if (globalSyncInProgress == true) {
      myDebug("  endSendForm on evite de sauvegarder les donnees en BDD avant upload car on est en synchronisation...");
    }
  }

  if (ajaxRequestSendForm) {
    ajaxRequestSendForm.abort();
  }

  if (filename != "") {
    gotoPage('messageUploadProgress.html', false);
  }

  let apiTarget = "/api/LdeFrais";
  if (newOrSync == "sync") {
    apiTarget = "/api/ldfImagesMissed/" + syncLdfID;
  }

  myDebug("  endSendForm on se connecte sur " + localGetData("api_server") + apiTarget);
  console.debug("  endSendForm on se connecte sur " + localGetData("api_server") + apiTarget);

  var ajaxRequestSendForm = $.ajax({
    url: localGetData("api_server") + apiTarget,
    timeout: 10000,
    type: "POST",
    data: formdata,
    processData: false,
    cache: false,
    contentType: false,
    tryCount: 3,
    retryLimit: 4,
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
    },
    xhrFields: {
      withCredentials: true
    },
    success: function (result, textStatus, request) {
      hideWait();
      globalNetworkResponse = Date.now();

      myDebug("Request finished (success signal) : " + textStatus + (result + '').length);
      myDebug("Request status (success signal) : " + request.status);
      myDebug("+++++++++++++++++++++++++++++++++++++++++++++++");
      myDebug(result.fileName);
      myDebug("+++++++++++++++++++++++++++++++++++++++++++++++");
      //Cote serveur on a un code 201 "special" dans ce cas
      if (request.status == "201") {

        //****************************************************** */
        //Expedition du fichier ?
        let ajaxURL = localGetData("api_server") + "/api/upload";

        //On stoppe tout autre transfert qui serait encore en arrière plan ...
        myDebug("Request finished, creation d'un objet resumable pour envoyer le fichier");
        let r = newResumable(ajaxURL, result, filename);
      }
      else {
        ons.notification.alert("Erreur de transfert avec le serveur. Votre document est enregistré sur votre téléphone, pensez à le synchroniser avec le serveur dès que vous retrouverez une bonne connexion (utilisez le menu synchronisation à ce moment là).", { title: "Réseau lent (3)" }).then(function () {
          gotoPage("menu.html", true);
        });
      }
    },
    error: function (request, textStatus, errorThrown) {
      hideWait();
      uploadErrorSaveFile(filename);

      myDebug("Request finished (error signal) : " + textStatus);
      if (textStatus == 'timeout') {
        myDebug('timeout, try again ...');
        ons.notification.alert("Erreur de transfert avec le serveur. Votre document est enregistré sur votre téléphone, pensez à le synchroniser avec le serveur dès que vous retrouverez une bonne connexion (utilisez le menu synchronisation à ce moment là).", { title: "Réseau lent (4)" }).then(function () {
          gotoPage("menu.html", true);
        });
        // this.tryCount++;
        // if (this.tryCount <= this.retryLimit) {
        //   //try again
        //   $.ajax(this);
        //   return;
        // }
        return;
      }
      if (request.status == 500) {
        ons.notification.alert("Une erreur serveur est survenue. Votre document est enregistré sur votre téléphone, pensez à le synchroniser avec le serveur un peu plus tard via le menu synchronisation. Si le problème persiste contactez-nous par mail à sav@doliscan.fr.", { title: "Réseau lent (5)" }).then(function () {
          gotoPage("menu.html", true);
        });
      } else {
        ons.notification.alert("Le serveur n'est pas accessible. Votre document est enregistré sur votre téléphone, pensez à le synchroniser avec le serveur dès que vous retrouverez une bonne connexion (utilisez le menu synchronisation à ce moment là).", { title: "Réseau lent (6)" }).then(function () {
          gotoPage("menu.html", true);
        });
      }

    }
    // complete: function (request, textStatus, errorThrown) {
    //   hideWait();
    //   myDebug("Request finished (complete signal) : " + textStatus);
    //   myDebug('Taille des donees envoyees sur le serveur : ' + request.getResponseHeader('Content-Length') + ' octets');
    //   if (request.status == "201") {

    //     if (globalDB !== null) {
    //       myDebug("globalLastInsertId : " + globalLastInsertId);
    //       if (globalLastInsertId > 0) {
    //         myDebug("  ldfsToSync SQL DELETE FROM ldfsToSync WHERE ID=" + globalLastInsertId);
    //         globalDB.executeSql('DELETE FROM ldfsToSync WHERE ID=?;', [globalLastInsertId]);
    //         myDebug("  ldfsToSync SQL DELETE FROM ... : ok");
    //         globalLastInsertId = "";
    //       }
    //     }
    // gotoPage('messageDone.html', false);
    //   }
    //       else {
    //     gotoPage('messageError.html', false);
    //   }
    // }
  });
}


// -----------------------------
// Analyseur de formulaire générique ... tant qu'on a pas trouvé mieux
//retourne vrai si le check est ok
function genericCheckForm(leform = null) {
  myDebug("genericCheckForm " + leform);
  if (leform == null) {
    myDebug("genericCheckForm leform null");
    return false;
  }

  let probleme = "";
  // myDebug("genericCheckForm");
  //Les deux tableaux doivent etre synchro, en haut le code de l'objet en bas le message à afficher
  fieldsToValidate = ['nom', 'arrivee', 'depart', 'distance', 'energievalue', 'label', 'ladate', 'email', 'password', 'puissancevalue', 'immat', 'ttc', 'typeFrais', 'typevalue', 'photoFacturette'];
  fieldsTexte = ["Nom", "Arrivée", "Départ", "Distance", "Type de carburant", "Objet", "La date", "Courrier électronique", "Mot de passe", "Puissance du véhicule", "Plaque d'immatriculation", "Montant TTC", "Type de frais", "Type", "Photo de la facturette"];

  //En mode developpeur la photo n'est pas indispensable
  if (globalDevMode) {
    fieldsToValidate.splice(-1, 1);
    fieldsTexte.splice(-1, 1);
  }

  let objForm = document.getElementById(leform);
  if (objForm == null || objForm == undefined) {
    myDebug("genericCheckForm objForm null");
    return false;
  }

  //Si le compte utilisateur est configuré sur le serveur pour etre en mode simple on exige alors que la photo ...
  myDebug("mode d'utilisation pour cette personne : " + localGetData('mode_simple'));
  if (localGetData('mode_simple') == 1) {
    fieldsToValidate = ['photoFacturette'];
    fieldsTexte = ["Photo de la facturette"];
  }
  for (let i = 0; i < fieldsToValidate.length; i++) {
    let field = fieldsToValidate[i];
    //Si il existe mais qu'il est vide alors c'est un pb
    myDebug("on cherche a valider le champ : " + field);

    if (objForm.elements[field] !== undefined) {
      myDebug("Cet element existe, " + field + "(" + i + "), de type " + objForm.elements[field].type + ", quelle est sa valeur ..." + objForm.elements[field].value);

      if (!objForm.elements[field].value) {
        $('#' + field).attr('style', "border-radius: 5px; border:#FF0000 2px solid;");
        probleme += "<ul>" + fieldsTexte[i] + "</ul>";
      }
      // fix #437 : evite de saisir des montants négatifs
      else if (objForm.elements[field].type == "number" && objForm.elements[field].value < 0) {
        $('#' + field).attr('style', "border-radius: 5px; border:#FF0000 2px solid;");
        probleme += "<ul>Montant négatif interdit</ul>";
      }
      else {
        $('#' + field).attr('style', "");
      }
    }
    else {
      myDebug("le champ suivant n'existe pas : " + field);
    }
  }
  if (probleme != "") {
    ons.notification.alert("Certaines informations sont manquantes ou incorrectes :<ul> " + probleme + "</ul>", { title: "Attention" }).then(function () {
      return false;
    });
    // myDebug("genericCheckForm return false " + probleme);
  }
  else {
    myDebug("genericCheckForm return true");
    return true;
  }
}


//initialise le formulaire avec des valeurs par défaut ...
function initialiseForm() {
  myDebug('initialiseForm ... ');
  if (document.getElementById('feedPaiements')) {
    myDebug('feedPaiements ...');
    $('#feedPaiements').load('parts/paiement.html', function () {
      myDebug('feedPaiements load ...');
      $('#feedPaiements').trigger("create");

      //Cas particulier pour la traduction
      $('#feedPaiements').find('.translate').each(function (i, obj) {
        // console.debug("PAY traduction de " + $(obj).html());
        // alert("on traduit [" + i + "] et " + $(obj).html());
        if ($(obj).html()) {
          let t = trans($(obj).html());
          $(obj).html(t.trim());
        }
      });


      //Si on est en mode modification de frais dataLDF n'est pas vide...
      if (dataLDF != "") {
        if (dataLDF['moyenPaiement']) {
          myDebug("Choix du moyen de paiement : ");
          $('#moyenPaiement-' + dataLDF['moyenPaiement']).click();
          myDebug("On a cliqué sur " + dataLDF['moyenPaiement']);
        }
      }
      else {
        //on essaye de lui attribuer par défaut le moyen de paiement sauvegardé
        //localStoreData("moyenPaiement", document.getElementById('moyenPaiement').value);
        if (localGetData('moyenPaiement')) {
          $('#moyenPaiement-' + localGetData('moyenPaiement')).click();
          myDebug("On a prévalidé le moyenPaiement-" + localGetData('moyenPaiement'));
        }
        else {
          myDebug('feedPaiements aucun moyen de paiement prévalidé ... clic on perso');
          $('#moyenPaiement-perso').click();
        }
      }
    });
    myDebug('feedPaiements end ');
  }

  //Si on a un objet "ladate" on essaye de lui attribuer par défaut la date du jour
  if (document.getElementById('ladate')) {
    let today = new Date();
    $('#ladate').val(today.toDateInputValue());
    // myDebug('date : ');
    // myDebug(today.toDateInputValue());
    // myDebug("Résultat de l'affectation : " + document.getElementById('ladate').value);
  }

  if (document.getElementById('feedAgendaList')) {
    if ($('#feedAgendaList').is(':empty')) {
      $("#feedAgendaList").append(feedAgendaList());
    }
  }


  if (document.getElementById('feedVehiculesPerso')) {
    // myDebug('Depuis initialiseForm, appel feedVehicules ...');
    if ($('#feedVehiculesPerso').is(':empty')) {
      $("#feedVehiculesPerso").append(feedVehicules("vehicules"));
    }
  }

  if (document.getElementById('feedVehiculesPersoCheck')) {
    // myDebug('Depuis initialiseForm, appel feedVehicules ...');
    if ($('#feedVehiculesPersoCheck').is(':empty')) {
      $("#feedVehiculesPersoCheck").append(feedVehicules("vehicules"));
    }
  }

  if (document.getElementById('feedVehiculesPro')) {
    myDebug('Depuis initialiseForm, appel feedVehiculesPro ...');
    if ($('#feedVehiculesPro').is(':empty')) {
      $("#feedVehiculesPro").append(feedVehicules("vehiculesPro"));
    } else {
      myDebug('Depuis initialiseForm, appel feedVehiculesPro a priori le bloc n est pas vide...');
    }
  }
  if (document.getElementById('feedVehiculesProCheck')) {
    myDebug('Depuis initialiseForm, appel feedVehiculesProCheck ...');
    if ($('#feedVehiculesProCheck').is(':empty')) {
      $("#feedVehiculesProCheck").append(feedVehicules("vehiculesPro"));
    } else {
      myDebug('Depuis initialiseForm, appel feedVehiculesPro a priori le bloc n est pas vide...');
    }
  }

  if (document.getElementById('feedTVA1')) {
    // myDebug('Depuis initialiseForm, appel feedTVA1 ...');
    if ($('#feedTVA1').is(':empty')) {
      let v = localGetData("compta_tva_tx1");
      $("#feedTVA1").append(v);
    }
  }
  if (document.getElementById('feedTVA2')) {
    // myDebug('Depuis initialiseForm, appel feedTVA1 ...');
    if ($('#feedTVA2').is(':empty')) {
      let v = localGetData("compta_tva_tx2");
      $("#feedTVA2").append(v);
    }
  }
  if (document.getElementById('feedTVA3')) {
    // myDebug('Depuis initialiseForm, appel feedTVA3 ...');
    if ($('#feedTVA3').is(':empty')) {
      let v = localGetData("compta_tva_tx3");
      $("#feedTVA3").append(v);
    }
  }
  if (document.getElementById('feedTVA4')) {
    // myDebug('Depuis initialiseForm, appel feedTVA4 ...');
    if ($('#feedTVA4').is(':empty')) {
      let v = localGetData("compta_tva_tx4");
      $("#feedTVA4").append(v);
    }
  }
  if (document.getElementById('feedCurrency')) {
    // myDebug('Depuis initialiseForm, appel feedTVA4 ...');
    if ($('#feedCurrency').is(':empty')) {
      let v = localGetData("currency");
      $("#feedCurrency").append(v);
    }
  }


  //Si on a un objet "ladateStart" on essaye de lui attribuer par défaut la date du 1er jour du mois en cours
  if (document.getElementById('ladateStart')) {
    var date = new Date();
    let jour = new Date(date.getFullYear(), date.getMonth() - 1, 1);
    $('#ladateStart').val(jour.toDateInputValue());
    // alert("on initialise la date start a " + jour);
  }

  //Si on a un objet "ladateEnd" on essaye de lui attribuer par défaut la date du 1er jour du mois en cours
  if (document.getElementById('ladateEnd')) {
    var date = new Date();
    let jour = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    $('#ladateEnd').val(jour.toDateInputValue());
  }

  //la liste des tags: creation du bloc
  if (document.getElementById('feedTags')) {
    myDebug('feedTags ...');
    if (null !== localGetData('tags')) {
      $('#feedTags').load('parts/tags.html', function () {
        myDebug('feedTags load ...');
        $('#feedTags').trigger("create");

        //Cas particulier pour la traduction
        $('#feedTags').find('.translate').each(function (i, obj) {
          // console.debug("PAY traduction de " + $(obj).html());
          // alert("on traduit [" + i + "] et " + $(obj).html());
          if ($(obj).html()) {
            let t = trans($(obj).html());
            $(obj).html(t.trim());
          }
        });

        //puis le contenu de la liste dans ce conteneur
        feedList('listeTags', 'tags', '');

        //par défaut aucun tag associé au frais en cours
        globalTagFrais = null;
        myDebug('feedTags load end...');
      });
    } else {
      myDebug('feedTags : pas de tags');
    }
    myDebug('feedTags end ');
  }

  //Sur iPhone on transforme les champs de type input=number pour avoir le clavier special
  //issu du plugin cordova-plugin-decimal-keyboard
  if (device !== undefined) {
    if (device.platform == "iOS") {
      $('input[type="number"]').attr('pattern', '[0-9]*');
      $('input[type="number"]').attr('decimal', 'true');
      $('input[type="number"]').prop('type', 'text');
    }
  }
  else {
    myDebug("device is undef !");
  }
  document.querySelector(':root').style.setProperty('--currency', "'" + localGetData("currency") + "'");
  myDebug('initialiseForm end');
}

function newResumable(ajaxURL, result, filename) {
  myDebug("creation de l'objet globalResumable pour " + filename + " a envoyer vers " + ajaxURL + " global = " + globalPictureFileName);

  if (filename.startsWith('content:')) {
    myDebug("  fichier commence par content:// ...");
  }
  myDebug("  fichier after ..." + globalPictureFileName);

  //Si on avait déjà un resume.js qui bosse (planté ?) on stoppe tout
  if (typeof globalProgressBar != 'undefined') {
    globalResumable.cancel();
  }

  //Si on a pas de nom de fichier c'est qu'on est peut-être sur des IK donc on short-return
  if (typeof filename === undefined || filename == "" || globalPictureFileName == 'ik') {
    if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
      myDebug("globalLastInsertId : " + globalLastInsertId);
      if (globalLastInsertId > 0) {
        localDeleteLDFtoSync(globalLastInsertId);
        // myDebug("  ldfsToSync SQL DELETE FROM ldfsToSync WHERE ID=" + globalLastInsertId);
        // globalDB.executeSql('DELETE FROM ldfsToSync WHERE ID=?;', [globalLastInsertId]);
        // myDebug("  ldfsToSync SQL DELETE FROM ... : ok");
        globalLastInsertId = "";
      }
    }
    gotoPage('messageDone.html', false);
    return;
  }

  //L'option testChunks est super mais pour l'instant provoque un bug sur iPhone donc on l'active que pour Android
  //Avril 2021 on re tente le tChunks pour tout le monde ...
  let para = 1;
  let tChunks = false;
  // if (window.cordova.platformId == "android") {
  //avec android ca passe :-)
  // tChunks = true;
  // para = 1;
  // }


  //Note/WARNING : ne pas passer en dessous de 40.000 ms de xhrTimeout sinon ça déconne complet
  //tests 4G eric@garage ...
  globalResumable = new Resumable({
    // Use chunk size that is smaller than your maximum limit due a resumable issue
    // https://github.com/23/resumable.js/issues/51
    chunkSize: 1 * 1024 * 1024, // 1Mo ?
    simultaneousUploads: para,
    forceChunkSize: 1 * 1024 * 1024, // 1Mo ?
    testChunks: tChunks,
    throttleProgressCallbacks: 1,
    parallelChunkUploads: true,
    chunkRetryInterval: 1000, // 1 sec pour éviter le flood from ip coté serveur
    maxChunkRetries: 4,
    maxFiles: 1,
    xhrTimeout: 40000, // 40 sec pour envoyer une photo
    // Get the url from data-url tag
    target: ajaxURL,
    headers: {
      ldfFileName: result.fileName,
      Authorization: 'Bearer ' + localGetData("api_token"),
    }
  });

  if (typeof globalProgressBar === 'undefined') {
    myDebug("resumable : creation de globalProgressBar ");
    globalProgressBar = new ProgressBar($('#upload-progress'));
  }

  globalResumable.on('fileAdded', function (file, event) {
    myDebug("resumable : fileAdded, file=" + file);

    globalProgressBar.fileAdded();
    myDebug("Apres progressBar.fileAdded, on essaye de lancer l'upload");
    globalResumable.upload();
  });

  globalResumable.on('fileSuccess', function (file, message) {
    let size = file.length;
    myDebug("resumable : fileSuccess, size=" + size);
    globalProgressBar.finish();

    if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
      myDebug("globalLastInsertId : " + globalLastInsertId);
      if (globalLastInsertId > 0) {
        localDeleteLDFtoSync(globalLastInsertId);
        globalLastInsertId = "";
      }
    }
    gotoPage('messageDone.html', false);
  });

  globalResumable.on('fileError', function (file, message) {
    myDebug("resumable : fileError on essaye de sauvegarder le fichier en espace permanent ...");
    uploadErrorSaveFile(filename);
    ons.notification.alert("Erreur de transfert du fichier, réessayez plus tard depuis le menu Synchronisation. Assurez-vous d'avoir une bonne couverture 4G/Data ou d'utiliser une connexion internet rapide via WiFi.", { title: "Attention" }).then(function () {
      gotoPage("menu.html", true);
    });
  });

  globalResumable.on('progress', function () {
    myDebug("resumable : progress " + globalResumable.progress());
    globalProgressBar.uploading(globalResumable.progress() * 100);
    // $('#pause-upload-btn').find('.glyphicon').removeClass('glyphicon-play').addClass('glyphicon-pause');
  });

  globalResumable.on('pause', function () {
    myDebug("resumable : pause ");
    // $('#pause-upload-btn').find('.glyphicon').removeClass('glyphicon-pause').addClass('glyphicon-play');
  });

  myDebug("resumable : ajout du fichier " + filename);

  /* *************************************
  * on expédie le fichier sur le serveur en utilisant le découpeur de fichier
  * resumable.js histoire de proposer une barre de progression et de rendre
  * tout ça un peu plus pro
  *************************************** */
  myDebug('pushFileResumeable (1) : ' + filename);

  //fichier PDF / Image locale sur android
  let photoFacturetteValue = $('#photoFacturette').val();
  myDebug("  code facturette =" + photoFacturetteValue);
  if (photoFacturetteValue == 'pdf') {
    var fileSize = globalLocalFileContent.length;
    myDebug("  'photo' code pdf ... size=" + fileSize);
    var myBlob = new Blob([globalLocalFileContent], { type: 'application/pdf' });
    myDebug("pushFileResumeable : addFile sur globalResumable (fichier local)");
    myBlob.name = result.fileName; //important pour que la magie côté serveur marche
    globalResumable.addFile(myBlob);
  } else {
    myDebug("  code facturette photo normale");
    //Fichier normal
    window.resolveLocalFileSystemURL(filename, function (fileEntry) {
      // myDebug("readFromFile (2): " + JSON.stringify(fileEntry));
      fileEntry.file(function (file) {
        var reader = new FileReader();
        var fileSize = file.size;
        // myDebug("readFromFile (3) Size = " + file.size);

        reader.onloadend = function (e) {
          var fileContent = new Uint8Array(reader.result);
          var myBlob = new Blob([fileContent], { type: 'image/jpeg' });
          myBlob.name = result.fileName; //important pour que la magie côté serveur marche

          myDebug("pushFileResumeable : addFile sur globalResumable");
          globalResumable.addFile(myBlob);
        };

        reader.readAsArrayBuffer(file);
      }, errorHandlerFileResumeable.bind(null, "Error on FileEntry " + filename));
    }, errorHandlerFileResumeable.bind(null, "Error on resolveLocalFileSystemURL " + filename));
  }
}


function uploadErrorSaveFile(filename) {
  myDebug('uploadErrorSaveFile, fic = ' + filename);
  // -------------------------------------------------
  //Erreur d'upload il est donc important de stocker le fichier en espace permanent
  // mais si on est en mode sync alors il est déjà dans un espace permanent ...
  if (globalSyncInProgress == false) {
    //On sauvegarde le Fichier sous un nom qui sera plus facile a gerer par la suite et stocké en espace persistant
    var today = new Date();
    var destFilename = today.getFullYear().toString() + ('0' + (today.getMonth() + 1)).slice(-2) + ('0' + today.getDate()).slice(-2) + "_"
      + today.getHours().toString() + today.getMinutes().toString() + "-" + globalTypeFrais + "-" + basename(filename);
    moveFile(filename, destFilename);
    myDebug('uploadErrorSaveFile, fic permanent sur = ' + destFilename);
  }
  // -------------------------------------------------
}


/**
* Purpose: blink a page element
* Preconditions: the element you want to apply the blink to,
    the number of times to blink the element (or -1 for infinite times),
    the speed of the blink
**/
function blinkElement(elem, times, speed) {
  if (times > 0 || times < 0) {
    if ($(elem).hasClass("blink"))
      $(elem).removeClass("blink");
    else
      $(elem).addClass("blink");
  }

  clearTimeout(function () { blinkElement(elem, times, speed); });

  if (times > 0 || times < 0) {
    setTimeout(function () { blinkElement(elem, times, speed); }, speed);
    times -= .5;
  }
}
