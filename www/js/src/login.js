function CreateAccount() {
    window.open("https://doliscan.fr/webRegister", "_system");
    return false;
}

//si accountID est non nul alors on essaye d'utiliser ce compte sauvegardé
function loginWithAPIToken(accountID = null) {
    myDebug("loginWithAPIToken : " + accountID);
    let lemail = "";
    if (accountID !== null) {
        let accounts = JSON.parse(localGetData("accounts"));
        if (accounts !== null) {
            lemail = accounts[accountID]['email'];
            api_token = accounts[accountID]['apikey'];
            let api_server = accounts[accountID]['server'];
            localStoreData("email", lemail);
            localStoreData("api_token", api_token);
            localStoreData("api_server", api_server);
        }
        else {
            myDebug("auto user switch NULL");
        }
    }

    if (localGetData("api_token") === null) {
        myDebug("loginWithAPIToken : aucun token, return");
        myDebug("carouselLogin call 0");
        loginAPIKeyErrorInProgress = true;
        carouselLogin();
        return;
    }
    lemail = localGetData("email");
    api_token = localGetData("api_token");
    myDebug("loginWithAPIToken : token existant on essaye de se connecter sur " + localGetData("api_server"));
    myDebug("loginWithAPIToken : avec " + api_token);
    myDebug("loginWithAPIToken : avec " + lemail);


    /*
      processData: false,
      cache: false,
      mimeType: "json",
      dataType: "json",
    */

    showWait();
    if (ajaxRequestJSON)
        ajaxRequestJSON.abort();
    var ajaxRequestJSON = $.ajax({
        url: localGetData("api_server") + "/api/ping",
        timeout: 5000,
        type: "POST",
        dataType: "json",
        data: {
            email: lemail,
        },
        headers: {
            Authorization: 'Bearer ' + api_token
        },
        //ok
        success: function (result, textStatus, request) {
            globalNetworkResponse = Date.now();
            hideWait();
            myDebug("loginWithAPIToken : OK (" + textStatus + "/" + request.status + ")");
            if (typeof result != 'undefined') {
                myDebug("loginWithAPIToken : result ok");
                myDebug(result.data);
                if (typeof result.data != 'undefined' && typeof result.data.api_token != 'undefined') {
                    myDebug("loginWithAPIToken : result.data ok, on compare " + globalCompatibleWithAPIVersion + " et " + result.data.api_version);
                    //On a la même version d'API entre client & serveur
                    if (result.data.api_version == globalCompatibleWithAPIVersion) {
                        //Dans tous les cas on peut continuer
                        updateLocalUserFromServer(result, updateLocalUserFromServerSuccess);
                        localStoreData("api_token", api_token);
                        gotoPage('menu.html', 'false');
                    }
                    else {
                        //Si on a un message specifique du serveur (genre api trop vieille) on l'affiche
                        if (typeof result.data.api_version_message != 'undefined') {
                            myDebug("result.data.api_token undefined : " + result);
                            $("#messagePopupMessage").replaceWith(result.data.api_version_message);
                        }
                        //Affichage forcé et aucun moyen de le contourner
                        $("#messagePopup").show();
                        setTimeout(function () { $("#erreurLogin").hide(); }, 5000);
                    }
                }
                else {
                    myDebug("loginWithAPIToken : ERR -> result.data undefined");
                    localStoreData("api_token", null);
                    myDebug("carouselLogin call 3");
                    carouselLogin();
                }
            }
            else {
                myDebug("loginWithAPIToken : ERR -> result undefined");
                localStoreData("api_token", null);
                myDebug("carouselLogin call 4");
                carouselLogin();
            }
        },
        //logout
        error: function (result) {
            hideWait();
            let msg = "";
            if (result.status === 0) {
                msg = 'Not connect. Verify Network.';
            } else if (result.status == 401) {
                msg = 'API Key error, not authenticated, maybe too old. [401]';
                myDebug("On passe loginAPIKeyErrorInProgress a true");
                loginAPIKeyErrorInProgress = true;
                nullKeyAccount("accounts", lemail);
            } else if (result.status == 403) {
                $("#erreurAccountDisabled").show();
                msg = 'Account disabled. [403]';
            } else if (result.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (result.status == 500) {
                msg = 'Internal Server Error [500].';
            } else {
                msg = 'Uncaught Error.\n' + result.responseText;
            }
            myDebug("loginWithAPIToken : ERR -> end session : " + msg);
            myDebug("loginWithAPIToken : ERR -> end session : " + result.status);
            localStoreData("api_token", null);
            myDebug("carouselLogin call 2");
            carouselLogin();
        }
    });
}

function passwordJson() {
    //On supprime tout message précédent
    $("#messagePopupMessage").replaceWith("");
    let lemail = document.getElementById('emailForget').value;
    showWait();
    // myDebug('loginJson : tentative de login via ajax request vers le serveur ');
    if (ajaxRequestPasswordJSON)
        ajaxRequestPasswordJSON.abort();

    var ajaxRequestPasswordJSON = $.ajax({
        type: "POST",
        url: localGetData("api_server","https://doliscan.fr") + "/api/password/forgot",
        timeout: 15000,
        data: {
            email: lemail,
        },
        success: function (result) {
            hideWait();
            // myDebug(result);

            globalNetworkResponse = Date.now();
            // myDebug("Success on a un message du serveur on l'affiche ...");
            ons.notification.alert(result.message, { title: "Information" }).then(function () {
                myDebug("carouselLogin call 9");
                carouselLogin();
            });
        },
        error: function (result) {
            hideWait();
            // myDebug(result.responseJSON);
            if (typeof result.responseJSON.message != 'undefined') {
                // myDebug("Error ... on a un message du serveur on l'affiche ...");
                ons.notification.alert(result.responseJSON.message, { title: "Erreur" });
            }
            else {
                // myDebug("Error ... pas de message serveur");
                // myDebug(result.message);
                ons.notification.alert("Erreur ... contactez le service technique pour plus d'informations.", { title: "Erreur" }).then(function () {
                    myDebug("carouselPassword call 1");
                    carouselPassword();
                });
                // setTimeout(function(){  $("#erreurLogin").hide(); }, 5000);
                // ons.notification.alert(result.message);
            }
        }
    });
}

//On essaye de trouver le serveur pour ce compte: si le sous domaine doliscan.domaine.ext existe alors c'est lui
//sinon on utilise doliscan.fr tout simplement
function loginButtonPressed(accountID = null) {
    let remember = 0;
    let lemail = document.getElementById('email').value;
    if (document.getElementById('rememberMe').checked) {
        remember = 1;
    }
    //dans tous les cas on le stocke pour pas que l'utilisateur ait à le re-saisir s'il s'est planté de passwd
    localStoreData("email", lemail);
    localStoreData("rememberMe", remember);
    myDebug("loginButtonPressed pour " + lemail);
    showWait();

    //un serveur special ?
    let customServer = document.getElementById('loginServer').value;
    if (customServer.length > 4) {
        globalDoliScanDomain = customServer;
        var testURL = "https://" + globalDoliScanDomain + "/api/hello";
        myDebug('Un serveur custom est spécifié : ' + globalDoliScanDomain);
    } else {
        //on essaye de passer en variable globale car les fonctions de retour en ont besoin ...
        globalDoliScanDomain = lemail.replace(/.*@/, "doliscan.");
        myDebug('On cherche ' + globalDoliScanDomain);

        var testURL = "https://" + globalDoliScanDomain + "/api/hello";
        myDebug("-> tentative de découverte d'un serveur doliscan sur " + testURL);
    }
    // let lemail = localGetData("email");
    showWait();

    if (ajaxRequestDiscover)
        ajaxRequestDiscover.abort();
    var ajaxRequestDiscover = $.ajax({
        url: testURL,
        debug: true,
        timeout: 15000,
        type: "POST",
        data: {
            email: lemail,
        },
        dataType: "json",
        // statusCode: {
        //     200: function () {
        //         myDebug('success, serveur doliscan trouvé !');
        //         localStoreData("email", lemail);
        //         localStoreData("api_server", "https://" + globalDoliScanDomain);
        //         localStoreData("name_server", globalDoliScanDomain);
        //         myDebug("Request finished 200 (api/hello)");
        //         loginJson();
        //     }
        // },
        success: function (data, textStatus, request) {
            hideWait();
            myDebug('success ! -> ' + data);
            myDebug(request);
            localStoreData("email", lemail);
            localStoreData("api_server", "https://" + globalDoliScanDomain);
            localStoreData("name_server", globalDoliScanDomain);
            myDebug("Request finished (api/hello)");
            //On essaye de se logguer sur ce serveur alors
            loginJson();
        },
        error: function (request, textStatus, error) {
            hideWait();
            localStoreData("api_server", "https://doliscan.fr");
            localStoreData("name_server", "doliscan.fr");
            globalDoliScanDomain = "doliscan.fr";
            let tryJson = true;
            let msg = "";
            if (request.status === 0) {
                msg = 'Not connect. Verify Network.';
            } else if (request.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (request.status == 500) {
                msg = 'Internal Server Error [500].';
                tryJson = false;
            } else {
                msg = 'Uncaught Error.\n' + textStatus;
            }
            myDebug("loginCustomServer error : ERR -> end session : " + msg);
            if (request.status == "503") {
                ons.notification.alert("Serveur indisponible, veuillez re-essayer plus tard." + localGetData("api_server"));
                tryJson = false;
            } else {
                $("#erreurCustomServerLogin").show();
            }
            if (textStatus === "timeout") {
                myDebug("Request finished (error signal) : " + textStatus);
            }
            myDebug(JSON.stringify(request));
            myDebug('error, fallback to doliscan.fr !');
            if(tryJson) {
                //Et on essaye de s'authentifier sur le serveur officiel
                loginJson();
            }
        }
    });
}

//Authentification sur le serveur
function loginJson() {
    myDebug("Tentative de connexion JSON ...");
    let lemail = document.getElementById('email').value;
    let lpassword = document.getElementById('password').value;
    let carousel = document.getElementById('carousel');
    showWait();
    var loginURI = localGetData("api_server") + "/api/login";
    myDebug('  loginJson : tentative de login via ajax request vers le serveur ' + loginURI + ' pour ' + lemail + ' **' + lpassword + "**");
    let remember = 0;
    if (document.getElementById('rememberMe').checked) {
        remember = 1;
    }
    localStoreData("rememberMe", remember);

    //au cas ou
    if (ajaxRequestLoginJSON)
        ajaxRequestLoginJSON.abort();

    var ajaxRequestLoginJSON = $.ajax({
        url: loginURI,
        timeout: 5000,
        type: "POST",
        data: {
            email: lemail,
            password: lpassword,
        },
        success: function (result, textStatus, request) {
            hideWait();
            myDebug("loginJson success ");
            globalNetworkResponse = Date.now();

            //alert(JSON.stringify(result));
            if (typeof result != 'undefined') {
                if (typeof result.data != 'undefined' && typeof result.data.api_token != 'undefined') {
                    if (result.data.api_version == globalCompatibleWithAPIVersion) {
                        updateLocalUserFromServer(result, updateLocalUserFromServerSuccess);
                    }
                    else {
                        // myDebug("result.data.api_token undefined : " + result);
                        if (typeof result.data.api_version_message != 'undefined') {
                            $("#messagePopupMessage").replaceWith(result.data.api_version_message);
                            $("#messagePopup").show();
                        }
                        $("#erreurLogin").show();
                        myDebug("carouselLogin call 5");
                        carouselLogin();
                    }
                    // setTimeout(function(){  $("#erreurLogin").hide(); }, 5000);
                }
                else {
                    $("#erreurLogin").show();
                    myDebug("carouselLogin call 6");
                    carouselLogin();
                }
            }
            else {
                $("#erreurLogin").show();
                myDebug("carouselLogin call 7");
                carouselLogin();
            }

            if (localGetData("api_server") == null) {
                // myDebug('login.html : erreur 1 ...');
                $("#erreurLogin").show();
                // setTimeout(function(){  $("#erreurLogin").hide(); }, 5000);
                myDebug("carouselLogin call 8");
                carouselLogin();
            }
        },
        error: function (result) {
            hideWait();
            let msg = "";
            if (result.status === 0) {
                msg = 'Not connect. Verify Network.';
            } else if (result.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (result.status == 500) {
                msg = 'Internal Server Error [500].';
            } else {
                msg = 'Uncaught Error.\n' + result.responseText;
            }
            if (result.status == "503") {
                ons.notification.alert("Serveur indisponible, veuillez re-essayer plus tard." + localGetData("api_server"));
            } else if (result.status == "403") {
                $("#erreurAccountDisabled").show();
            } else {
                $("#erreurLogin").show();
            }
            myDebug("loginJson error : ERR -> end session : " + msg);
            myDebug("loginJson error : ERR -> end session : " + result.status);
            localStoreData("api_token", null);
            myDebug("carouselLogin call 10");
            carouselLogin();
        }
    });
}

function switchServer(formId, nom, adresse) {
    $(formId).empty();
    $(formId).append('Serveur: <b>' + nom + "</b>");
    localStoreData("api_server", adresse);
    localStoreData("name_server", nom);
    // document.querySelector('ons-list-item').hideExpansion();
}

//Passe sur la page de Login
function carouselLogin(forceIndex = -1) {
    myDebug("carouselLogin... loginAPIKeyErrorInProgress=" + loginAPIKeyErrorInProgress);

    if (globalMyNavigator.topPage.data.name != "login.html") {
        gotoPage('login.html');
    }

    let defaultCarouselIndex = 0;
    if (localGetData("accounts") !== null) {
        defaultCarouselIndex = 2;
    }
    if (loginAPIKeyErrorInProgress == true) {
        defaultCarouselIndex = 0;
    }
    if (forceIndex >= 0) {
        defaultCarouselIndex = forceIndex;
    }

    if (localGetData("email") !== null) {
        // $("#autoLoginText").append("Compte " + localGetData("email") + " sur " + localGetData("name_server"));
        $('input[name="email"]').val(localGetData("email"));
    }

    //let carousel = document.getElementById('carousel');
    try {
        carousel = document.querySelector('ons-carousel');
        if (typeof carousel == 'object') {
            myDebug("carouselLogin... on est sur l'index " + carousel.getActiveIndex());
            if (carousel.getActiveIndex() != defaultCarouselIndex) {
                myDebug("carouselLogin... set Active Index : " + defaultCarouselIndex);
                carousel.setActiveIndex(defaultCarouselIndex);
                carousel.refresh();
            }
            myDebug("carouselLogin... on est maintenant sur l'index " + carousel.getActiveIndex());
        }
        else {
            myDebug("carouselLogin not ready...");
        }
    } catch (e) {
        myDebug("carouselLogin not ready catched ... ", e)
    }
}

function carouselPassword() {
    let carousel = document.getElementById('carousel');
    if (typeof carousel == 'object') {
        if (carousel.getActiveIndex() != 1) {
            myDebug("carouselLogin... set Active Index : 1");
            carousel.setActiveIndex(1);
            document.getElementById('emailForget').value = document.getElementById('email').value;
        }
    }
}

function logoutOnDoliSCAN() {
    myDebug("Fermeture de session ...");
    showWait();
    var logoutURI = localGetData("api_server") + "/api/logout";

    //au cas ou
    if (ajaxRequestLogout)
        ajaxRequestLogout.abort();

    var ajaxRequestLogout = $.ajax({
        url: localGetData("api_server") + "/api/logout",
        timeout: 5000,
        type: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
        },
        xhrFields: {
            withCredentials: true
        },
        success: function (result) {
            globalNetworkResponse = Date.now();
            hideWait();
        },
        error: function (result) {
            hideWait();
            //TODO message a faire
            // window.location.replace('index.html');
            closePage();
        }
    });
}