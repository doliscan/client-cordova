/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

function updateLocalUserFromServerSuccessMessage(code) {
  ons.notification.alert('Données sauvegardées, vous pouvez revenir au menu principal...', { title: "Succès #1" });
}

function updateLocalUserFromServerSuccess(resultBool) {
  if (globalMyNavigator !== undefined) {
    gotoPage('menu.html');
  }
}

//Factorisation de code: on analyse un result serveur pour sauvegarder en local les donnees
function updateLocalUserFromServer(result, callback) {
  myDebug("=== updateLocalUserFromServer / " + callback + "===");
  //myDebug(result);
  //On supprime les fichiers de customization : peut-être que la nouvelle entité n'en a pas
  //si l'utilisateur change de compte par exemple
  let ret = false;
  // myDebug(JSON.stringify(result));
  if ((typeof result != 'undefined') && (result !== null) && (result != "")) {

    for (let key of globalSrvKeys) {
      myDebug("updateLocalUserFromServer: on cherche " + key);
      if ((typeof result.data != 'undefined') && (result.data !== null) && (result.data != "")) {
        // myDebug('updateLocalUserFromServer typeOf : ' + (typeof result.data));
        // myDebug("on essaye : " + key + " -> " + result.data[key]);
        if ((typeof result.data[key] != 'undefined') && (result.data[key] !== null) && (result.data[key] != "")) {
          myDebug("  valeur non nulle on la stocke : " + key + " = " + result.data[key]);
          localStoreData(key, result.data[key]);
          ret = true;
        }
      }
      else {
        //Dans les retours de PUT on a droit aux donnees directement
        if ((typeof result[key] != 'undefined') && (result[key] !== null) && (result[key] != "")) {
          // myDebug("  PUT / valeur non nulle on la stocke : " + key + " = " + result.data[key]);
          localStoreData(key, result[key]);
          ret = true;
        }
      }
    }

    //Les données des véhicules
    myDebug("  on cherche si on a des vehicules ...");
    myDebug(result.vehicules);
    //On vide la liste : si on change de compte sur le smartphone il ne faut pas garder les "autres véhicules"
    localStoreData("vehicules", null);
    localStoreData("vehiculesPro", null);
    if ((typeof result.vehicules != 'undefined') && (result.vehicules !== null) && (result.vehicules != "")) {
      myDebug("  on a des vehicules ...");

      result.vehicules.forEach((v, key) => {
        let storageKey = "vehiculesPro"
        if (v.is_perso)
          storageKey = "vehicules";

        addVehicule(storageKey, v.name, v.energy, v.power, v.type, v.kmbefore, v.number, v.uuid);
        // myDebug("  valeur non nulle on stocke le vehicule : " + JSON.stringify(vehiculeNew));
      });
      // localStoreData(key, result.vehicules[key]);
      ret = true;
    }

    //Les données des itinéraires les plus fréquents
    myDebug("  mostFrequentRoutes:: on cherche si on a des itinéraires ...");
    myDebug(result.mostFrequentRoutes);
    //On vide la liste : si on change de compte sur le smartphone il ne faut pas garder les "autres véhicules"
    localStoreData("mostFrequentRoutes", null);
    if ((typeof result.mostFrequentRoutes != 'undefined') && (result.mostFrequentRoutes !== null) && (result.mostFrequentRoutes != "")) {
      myDebug("  mostFrequentRoutes:: on a des itinéraires ...");

      result.mostFrequentRoutes.forEach((v, key) => {
        let storageKey = "mostFrequentRoutes";
        if (v.distance > 0 && v.distance != '') {
          addFrequentRoute(storageKey, v.depart, v.arrivee, v.distance, v.label, v.etapes);
        }
      });
      ret = true;
    }

    //Les tags
    myDebug("  tags:: on cherche si on a des étiquettes ...");
    myDebug(result.tags);
    //On vide la liste : si on change de compte sur le smartphone il ne faut pas garder les "autres tags"
    localStoreData("tags", null);
    if ((typeof result.tags != 'undefined') && (result.tags !== null) && (result.tags != "")) {
      myDebug("  Tags:: on a des étiquettes ..." + typeof (result.tags));
      result.tags.forEach((v, key) => {
        let storageKey = "tags";
        addTag(storageKey, v.code, v.label);
      });
      ret = true;
    }

    //Le message de mise à jour ...
    if (compareVersions(localGetData("official_app_version"), globalAppVersion) > 0) {
      myDebug("index.html : " + localGetData("official_app_message"));
      ons.notification.alert(localGetData("official_app_message"), { title: "Nouvelle version" });
    }

    //Le message custom
    if ((typeof result.customMessage != 'undefined') && (result.customMessage !== null) && (result.customMessage != "")) {
      myDebug("  on a peut-être du customMessage ...");
      // eviter d'afficher le message à chaque lancement ... avoir un numéro ou un truc pour dire "j'ai déjà lu merci"
      if (result.customMessage.original != "") {
        ons.notification.alert(result.customMessage.original, { title: "Information" });
      }
      ret = true;
    }

    //La boite a propos
    if ((typeof result.customAbout != 'undefined') && (result.customAbout !== null) && (result.customAbout != "")) {
      myDebug("  on a peut-être du customAbout ...");
      localStoreData("customAbout", result.customAbout.original);
      ret = true;
    }


    //La feuille de style
    if ((typeof result.customCSS != 'undefined') && (result.customCSS !== null) && (result.customCSS != "")) {
      myDebug("  on a peut-être du css ...");
      saveCSSFile(result.customCSS.original);
      ret = true;
    }

    //Le logo
    if ((typeof result.customLogo != 'undefined') && (result.customLogo !== null) && (result.customLogo != "")) {
      myDebug("  on a peut-être du css ...");

      let blob = base64toBlob(result.customLogo.original, "image/png");
      saveLogoFile(blob, "png");
      // saveLogoFile(result.customLogo.original, "txt");
      ret = true;
    }

    //Et on garde le compte pour la prochaine fois si case cochee
    let remember = localGetData("rememberMe");
    myDebug("localGetData rememberMe : " + remember);
    if (remember == 1) {
      let lemail = localGetData("email");
      let api_token = localGetData("api_token");
      let api_server = localGetData("api_server");
      addAccount("accounts", lemail, api_token, api_server);
    }

    myDebug("=== updateLocalUserFromServer appel de callback " + callback + " pour " + ret);
    callback(ret);
  }

}

//Factorisation de code: on envoie le profil utilisateur sur le serveur
function updateServerUserFromLocal() {
  var data = {};

  for (let key of globalSrvKeys) {
    let value = localGetData(key);
    if (value !== null) {
      data[key] = value;
    }
  }

  // myDebug("updateServerUserFromLocal");
  $.ajaxSetup({
    timeout: 10000,
    headers: {
      'X-CSRF-TOKEN': localGetData("api_token")
    }
  });

  if (ajaxUpdateServer)
    ajaxUpdateServer.abort();

  var ajaxUpdateServer = $.ajax({
    type: "PUT",
    timeout: 5000,
    url: localGetData("api_server") + "/api/user",
    data,
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
    },
    xhrFields: {
      withCredentials: true
    },
    success: function (result) {
      globalNetworkResponse = Date.now();
      // myDebug('updateServerUserFromLocal : succès ...' + JSON.stringify(result));
      if (result != undefined) {
        hideWait();
        updateLocalUserFromServer(result, updateLocalUserFromServerSuccessMessage);
        // myDebug('    updateServerUserFromLocal : sauvegarde serveur ok');
      }
      else {
        hideWait();
        ons.notification.alert('Erreur de communication avec le serveur, re essayez dans quelques instants ...', { title: "Erreur" });
      }

      if (localGetData("api_server") == null) {
        // myDebug('updateServerUserFromLocal : erreur 1 ...');
        hideWait();
        $("#erreurLogin").show();
        // setTimeout(function(){  $("#erreurLogin").hide(); }, 5000);
      }
    },
    error: function (result) {
      // myDebug('updateServerUserFromLocal : erreur 2 ...');
      hideWait();
      ons.notification.alert('Erreur de communication avec le serveur, re essayez dans quelques instants ...', { title: "Erreur" });
    }
  });
}


//Supprime le compte utilisateur "id"
function rmAccount(storageKey, id) {
  // myDebug("rmaccount :" + id);
  let accounts = JSON.parse(localGetData(storageKey));
  // myDebug("rmaccount liste avant" + accounts);
  accounts.splice(id, 1);
  if (accounts.length > 0)
    localStoreData(storageKey, JSON.stringify(accounts));
  else
    localStoreData(storageKey, null);
  // myDebug("rmaccount liste apres" + accounts);
}

//Passe à null la clé d'api de l'utilisateur
function nullKeyAccount(storageKey, email) {
  return new Promise((resolve, reject) => {
    let update = 0;
    //On cherche une place de libre (max 4 on a dit)
    let accounts = JSON.parse(localGetData(storageKey));
    if (accounts !== null) {
      for (let f = 0; f < accounts.length; f++) {
        if (accounts[f]['email'] == email) {
          accounts[f]['apikey'] = null;
        }
      }
    }
    resolve(localStoreData(storageKey, JSON.stringify(accounts)));
  });
}

//Ajoute un compte utilisateur
function addAccount(storageKey, email, apikey, server) {
  myDebug("addAccount : " + email);
  return new Promise((resolve, reject) => {
    let update = 0;
    //On cherche une place de libre (max 4 on a dit)
    let a = localGetData(storageKey);
    let accounts = [];
    if (a !== null && a !== "") {
      accounts = JSON.parse(a);
    }

    //evite les doublons, on update la clé
    for (let f = 0; f < accounts.length; f++) {
      if (accounts[f]['email'] == email) {
        accounts[f]['apikey'] = apikey;
        accounts[f]['server'] = server;
        update = 1;
        myDebug("addAccount : update");
      }
    }
    if (update == 0) {
      let accountNew = { "email": email, "apikey": apikey, "server": server };
      myDebug("addAccount : insert : " + accountNew);
      accounts.push(accountNew);
    }
    resolve(localStoreData(storageKey, JSON.stringify(accounts)));
  });
}
