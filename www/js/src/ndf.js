/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

// Wait for Cordova to connect with the device
//
document.addEventListener("deviceready", NDFonDeviceReady, false);

// Cordova is ready to be used!
//
function NDFonDeviceReady() {
  myDebug("NDFonDeviceReady de ndf.js");
  if (navigator.camera !== undefined) {
    globalMyCamera = navigator.camera;
    globalPictureSource = navigator.camera.PictureSourceType;
    globalDestinationType = navigator.camera.DestinationType;
    globalMyCamera.cleanup();
  }
  else {
    myDebug("Erreur: camera introuvable !");
  }
  myDebug("NDFonDeviceReady de ndf.js : camera ready");
}

// Called when a photo is successfully retrieved as base64
//
function onPhotoDataSuccess(imageData) {
  myDebug("onPhotoDataSuccess");
  // Uncomment to view the base64 encoded image data
  // myDebug(imageData);
  $('#displayDocument').empty();
  $('#displayDocument').append('<img id="smallImage" style="height: 200px; max-width: 100%; width:100%; width: 100vw; background-repeat:no-repeat; background-size:cover; display:block; background-position:left top; background-image:url(' + imageData + ');" onclick="capturePhotoEdit();" />');

  // let smallImage = document.getElementById('smallImage');
  // smallImage.style.display = 'block';

  // Show the captured photo
  // The inline CSS rules are used to resize the image
  // $("#smallImage").attr("src", imageData);

  // le "flag" pour dire au formulaire qu'on a bien pris la photo
  $('#photoFacturette').val(2);
}

// Called when a photo is successfully retrieved as file
function onPhotoURISuccess(imageURI) {
  var imageCDV;
  // Uncomment to view the image file URI
  myDebug("onPhotoURISuccess :" + imageURI);

  //En mode dev sur l'émulateur le crop ne marche pas alors on fake
  // if(globalDevMode) {
  //   globalPictureFileName = imageURI;

  //   // error callback
  //   let smallImage = document.getElementById('smallImage');
  //   // Unhide image elements
  //   smallImage.style.display = 'block';
  //   // Show the captured photo
  //   // The inline CSS rules are used to resize the image
  //   smallImage.src = imageURI;

  //   // le "flag" pour dire au formulaire qu'on a bien pris la photo
  //   $('#photoFacturette').val(2);
  //   return;
  // }

  //Deux pistes, peut-être que le plugin crop veut un cdv:// ou content://
  //mais finalement non il fallait juste virer le file:// au debut
  // resolveLocalFileSystemURL(imageURI, function (entry) {
  //   var nativePath = entry.toURL();
  //   myDebug('Native URI: ' + nativePath);
  // });

  // resolveLocalFileSystemURL(imageURI, function (entry) {
  //   myDebug('cdvfile URI: ' + entry.toInternalURL());
  //   imageCDV = entry.toInternalURL();
  // });

  if (imageURI.startsWith("file://")) {
    imageCDV = imageURI.substr(7);
  }
  else {
    imageCDV = imageURI;
  }

  // CODE DIFFÉRENT IOS/ANDROID
  // ======================================= iOS
  if (device !== undefined) {
    if (device.platform == "iOS") {
      plugins.crop(function success(data) {
          dataCDV = window.WkWebView.convertFilePath(data);
          //On stocke le nom du fichier complet dans la variable globale pour passer a resume et sync
          globalPictureFileName = data;
          myDebug('iOS retour du crop : ' + data + " -> convert to CDV : " + dataCDV);

          // Show the captured photo
          // The inline CSS rules are used to resize the image
          $('#displayDocument').empty();
          $('#displayDocument').append('<img id="smallImage" style="height: 200px; max-width: 100%; width:100%; width: 100vw; background-repeat:no-repeat; background-size:cover; display:block; background-position:left top; background-image:url(' + dataCDV + ');" onclick="capturePhotoEdit();" />');

          // le "flag" pour dire au formulaire qu'on a bien pris la photo
          $('#photoFacturette').val(2);
        },
        function fail() { 
        
        },
        imageURI, { quality: 80, keepAspectRatio: 0, targetWidth: -1, targetHeight: -1 }
      );
    }
    // ======================================= Android
    else {

      //nov 2023 switch to https://github.com/lincolnminto/cordova-plugin-ratio-crop.git

      myDebug('android avant appel du crop');
      plugins.crop(
        function success(data) {
          // success callback
          // saved cropted image path
          //On stocke le nom du fichier complet dans la variable globale pour passer a resume et sync
          // ons.notification.alert("Photo du justificatif " + data, { title: "Attention" });

          globalPictureFileName = data.replace(/\?.*/, '');
          myDebug('Android retour du crop : fichier croppe=' + globalPictureFileName + " fichier non croppé=" + imageURI);

          //On sauvegarde un thumbnail qu'on transformera en base64 pour l'afficher à l'utilisateur
          fileThumbnail = "";
          if (imageURI.startsWith("file://")) {
            fileThumbnail = window.cordova.file.externalDataDirectory + "/" + basename(data);
          }
          else {
            fileThumbnail = "file://" + window.cordova.file.externalDataDirectory + "/" + basename(data);
          }

          //options pour le thumbnail
          var options = {
            maxPixelSize: 200,
            targetPath: fileThumbnail
          };

          myDebug('Appel de thumbnails avec : source=' + globalPictureFileName + " et target=" + fileThumbnail);
          Thumbnails.thumbnail(globalPictureFileName, options, function success(path) {
            myDebug('thumbnails generated at path=' + path);
            getFileContentAsBase64(path, onPhotoDataSuccess);
          }, function fail(error) {
            myDebug('thumbnails ERROR ' + error);
            console.error(error);
          });

          // le "flag" pour dire au formulaire qu'on a bien pris la photo
          $('#photoFacturette').val(2);
          // ==================== end version historique avec nom de fichier
        },
        function fail(err) {
          myDebug("Crop Error: " + err);
        },
        imageURI, { quality: 80, keepAspectRatio: 0, targetWidth: -1, targetHeight: -1 });

      myDebug('android apres appel du crop');
    }
  }
  else {
    myDebug("device is undef !");
  }
}

// A button will call this function
//
function capturePhoto() {
  if (device !== undefined) {
    if (device.platform == "iOS") {
      capturePhotoIOS();
    }
    else {
      capturePhotoAndroid();
    }
  }
  else {
    myDebug("device is undef !");
  }

}

function capturePhotoAndroid() {
  // Take picture using device camera and retrieve image as
  // base64-encoded string if DATA_URL
  // file name if FILE_URI
  globalMyCamera.getPicture(onPhotoURISuccess, onFailCapturePhoto, {
    quality: 80, destinationType: globalDestinationType.FILE_URI, correctOrientation: true, encodingType: Camera.EncodingType.JPEG, mediaType: Camera.MediaType.PICTURE
  });
}

function capturePhotoIOS() {
  // Take picture using device camera and retrieve image as
  // base64-encoded string if DATA_URL
  // file name if FILE_URI
  globalMyCamera.getPicture(onPhotoURISuccess, onFailCapturePhoto, {
    quality: 80, destinationType: globalDestinationType.FILE_URI, correctOrientation: true, encodingType: Camera.EncodingType.JPEG, mediaType: Camera.MediaType.PICTURE
  });
}

// A button will call this function
//
function capturePhotoEdit() {
  // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
  //globalMyCamera.getPicture(onPhotoURISuccess, onFailCapturePhoto, { quality: 80, allowEdit: false, destinationType: globalDestinationType.FILE_URI, correctOrientation: true, encodingType: Camera.EncodingType.JPEG });
  if (device !== undefined) {
    if (device.platform == "iOS") {
      capturePhotoIOS();
    }
    else {
      capturePhotoAndroid();
    }
  }
  else {
    myDebug("device is undef !");
  }

}

// A button will call this function
//
function getPhoto(source) {
  // Retrieve image file location from specified source
  globalMyCamera.getPicture(onPhotoURISuccess, onFailCapturePhoto, { quality: 80, destinationType: globalDestinationType.FILE_URI, sourceType: source });
}

function importLocalFileFromPhone() {
  globalLocalFileContent = null;
  myDebug('import PDF / File from local storage ...');
  (async () => {
    const file = await chooser.getFile('image/jpg,image/jpeg,application/pdf');
    myDebug("file name = " + file.name);
    myDebug("file URI = " + file.uri);
    myDebug("file dataURI = " + file.dataURI);
    $('#pdfFileName').val(file.name);
    let lcase = file.name.toLowerCase();
    globalPictureFileName = file.name;
    globalLocalFileContent = file.data;

    //tentative de transformation du nom du fichier
    // if (lcase.startsWith('content:')) {
    //   myDebug("try to resolve uri");
    //   
    //   window.resolveLocalFileSystemURL(file.uri, fileEntry => {
    //     fileEntry.file(file => {
    //       const { name, size } = file;
    //       globalPictureFileName = name;
    //       myDebug(`The name of this file is ${name} and its size is ${size}`);
    //     });
    //   }, error => myDebug(error));
    // }

    if (lcase.endsWith("pdf")) {
      //gestion d'un fichier pdf
      $('#displayDocument').empty();
      $('#displayDocument').append('<h1>Document PDF : </h1><p>' + file.name + '</p>');
      $('#photoFacturette').val('pdf');
    } else {
      onPhotoURISuccess(file.uri);
    }
  })();
}

// Called if something bad happens.
//
function onFailCapturePhoto(message) {
  //alert('Failed because: ' + message);
  ons.notification.alert("Erreur 12 : " + message, { title: "Attention" }).then(function () {
    //gotoPage("menu.html", true);
  });
}

function errorHandlerFileResumeable(title, message) {
  myDebug('errorHandlerFileResumeable : ' + title + ' -- message follow:');
  myDebug(message);
  myDebug('errorHandlerFileResumeable end message');
  ons.notification.alert("Erreur 18 : " + title, { title: "Attention" }).then(function () {
    gotoPage("menu.html", true);
  });
}


/* *************************************
* Changement du nom du fichier OK
*************************************** */
function onSuccessRenameFile(entry) {
  myDebug("Changement FAIT SANS ERREUR pour le fichier ref globalLastInsertId = " + globalLastInsertId + " : " + entry.fullPath);

  localUpdateLDFfileName(globalLastInsertId, entry.fullPath);
  // if (globalDB !== null) {
  //   globalDB.executeSql('UPDATE ldfsToSync set localFileName = ? WHERE id = ?;', [entry.fullPath, globalLastInsertId],
  //     function (results) {
  //       myDebug('  ldfsToSync SQL UPDATE OK for fileRename ' + entry.fullPath);
  //     },
  //     function (error) {
  //       myDebug('  ldfsToSync SQL UPDATE ERROR for: ' + entry.fullPath + " message: " + error.message);
  //     });
  // }
}

/* *************************************
* Gestion de l'erreur de changement du nom du fichier
*************************************** */
function onErrorRenameFile(evt) {
  myDebug("onErrorRenameFile ERREUR de changement du nom du fichier");
  // myDebug(evt);
}

/* *************************************
* Changement du nom d'un fichier
*************************************** */
function moveFile(fileUri, newFileName) {
  myDebug('moveFile (1): ' + fileUri + " -> " + newFileName);
  window.resolveLocalFileSystemURL(
    fileUri,
    function (fileEntry) {
      newFileUri = "";
      if (window.cordova.platformId == "android") {
        newFileUri = window.cordova.file.externalDataDirectory;
        myDebug('moveFile (2): ' + newFileUri);
      }
      else {
        newFileUri = cordova.file.documentsDirectory;
        myDebug('moveFile (2): ' + newFileUri);
      }
      oldFileUri = fileUri;
      window.resolveLocalFileSystemURL(newFileUri,
        function (dirEntry) {
          // move the file to a new directory and rename it
          // myDebug('moveFile (2): ' + dirEntry + " -> " + newFileName);
          myDebug('moveFile (3): ' + JSON.stringify(dirEntry) + " -> " + newFileUri + " :: " + newFileName);
          fileEntry.moveTo(dirEntry, newFileName, onSuccessRenameFile, onErrorRenameFile);
        },
        onErrorRenameFile);
    },
    onErrorRenameFile);
}

function createBlob(data, type) {
  var r;
  try {
    r = new Blob([data], { type: type });
  }
  catch (e) {
    // TypeError old chrome and FF
    BlobBuilder = BlobBuilder || WebKitBlobBuilder || MozBlobBuilder || MSBlobBuilder;
    // consider to use crosswalk for android

    if (e.name === 'TypeError' && BlobBuilder) {
      var bb = new BlobBuilder();
      bb.append([data.buffer]);
      r = bb.getBlob(type);
    }
    else if (e.name == "InvalidStateError") {
      // InvalidStateError (tested on FF13 WinXP)
      r = new Blob([data.buffer], { type: type });
    }
    else {
      throw e;
    }
  }

  return r;
}

//interdiction de saisir des frais par anticipation dans le futur ...
function interdictionDateFutur() {
  if ($('input[name="ladate"]')) {
    // ons.getScriptPage().onInit = function() {
    let now = new Date();
    // maximum date the user can choose, in this case in the past up to now
    let maxDate = now.toISOString().substring(0, 10);
    $('input[name="ladate"]').prop('max', maxDate);
  }
  else {
    myDebug("pas de champ date sur cette page ...");
  }
}


function feedObjets(objets) {
  if ((typeof objets != 'undefined') && (objets !== null) && (objets != "")) {
    for (var i = 0; i < objets.length && i < 15; i++) {
      $('#listeObjets').append('<div class=\"expandable-content  list-item__expandable-content\"><a href="#" onclick="selectLabel(\'' + objets[i] + '\');">' + objets[i] + '</a></div>');
    }
  }
}

function selectLabel(objet) {
  if ((typeof objet != 'undefined') && (objet !== null) && (objet != "")) {
    $('#listeObjets').hide();
    $('#label').val(objet);
    $('#label').focus();
    document.querySelector('#listeObjets').hideExpansion();
    $('#listeObjets').show();
  }
}

//Permet de choisir le texte / titre de la page depuis la page elle même et non dans l'appel de gotoPage
function setPageTitle(titre) {
  myDebug("setPageTitle : " + titre);
  globalCurrentTitle = titre;
  $('#titreDoliScan').replaceWith('<div id="titreDoliScan" class="left" onclick="fn.back();"><i class="zmdi chevron-circle-left"></i> ' + titre + '</div>');
}

//sera appellee lorsque la page sera bien chargee pour completer automatiquement le formulaire
//avec les donnees a modifier
function completePageIKAfterGoto(dataIK) {
  myDebug("completePageIKAfterGoto : " + JSON.stringify(dataIK));

  document.getElementById("updateID").value = dataLDF['sid'];

  let date = new Date(decodeURIComponent(dataIK.ladate));
  $('#ladate').val(date.toDateInputValue());

  //Le véhicule -> voir feedvehicule


  //Le label
  $('#label').val(decodeURIComponent(dataIK.label));

  //Départ
  $('#depart').val(decodeURIComponent(dataIK.depart));

  //Decoupage du texte "Etapes : arcachon (33) -> Bordeaux (33) -> Agen (47) -> Arcachon (33)"
  invites = decodeURIComponent(dataIK.invites);
  myDebug("Tentative d'analyse de invites=" + invites);
  if (invites !== null && invites !== undefined && invites.toLowerCase().indexOf(':') > 0) {
    pattern = /.*:\s*([\w\s]*\(\d{0,2}\)*)/g;
    res = invites.split(pattern);
    nbEtape = 1;
    for (let i = 0; i < res.length; i++) {
      let ville = res[i].trim();
      if (ville != "") {
        myDebug("  Ajout d'une etape=" + ville);
        ikAjoutEtape('leformIK');
        //la fonction ikAjoutEtape incremente nbEtape ...
        $('#etape' + (nbEtape - 1)).val(ville);
      }
    }
  }
  //Et la dernière étape : l'arrivée
  ikAjoutEtape('leformIK');
  $('#etape' + (nbEtape - 1)).val(decodeURIComponent(dataIK.arrivee));

  dataIK = "";
  callAllStep();
}

function aSyncSearchVille() {
  return new Promise((resolve, reject) => {
    //here our function should be implemented 
    setTimeout(() => {
      // console.log("Hello from inside the aSyncSearchVille function");
      searchVille('leformIK', 'depart', 'departSlug', 'departUID');
      resolve();
    }, 500);
  });
}

function aSyncDistanceAutomatiqueEtape(leform, etape, etapeNb) {
  return new Promise((resolve, reject) => {
    //here our function should be implemented 
    setTimeout(() => {
      // console.log("Hello from inside the aSyncDistanceAutomatiqueEtape function");
      if (globalMyNavigator.topPage.id != "ONSChoixListe") {
        distanceAutomatiqueEtape(leform, etape, etapeNb);
      }
      resolve();
    }, 500);
  });
}

async function callAllStep() {
  myDebug("callAllStep 1");
  await ons.notification.confirm(
    {
      message: "Veuillez patienter le temps que le système recalcule votre itinéraire. Un message d'information sera affiché lorsque ce calcul sera terminé.",
      title: "Import et calcul",
      buttonLabels: ['OK'],
    }
  );
  myDebug("callAllStep 2");
  await aSyncSearchVille();
  for (let i = 1; i < nbEtape; i++) {
    //Si jamais une résolution ne marche pas et propose des choix il ne faut surtout pas continuer la boucle sinon page blanche et bug global
    myDebug("Caller aSyncDistanceAutomatiqueEtape #" + i);
    if (globalMyNavigator.topPage.id != "ONSChoixListe") {
      myDebug("Caller lancé !!!");
      await aSyncDistanceAutomatiqueEtape('leformIK', 'etape' + i, i);
    }
    else {
      myDebug("Caller disabled on est sur ONSChoixListe !!!");
    }
  }
  // console.log("After waiting");
  ons.notification.alert("Votre itinéraire a été importé.", { title: "Terminé !" });
}


//sera appellee lorsque la page sera bien chargee pour completer automatiquement le formulaire
//avec les donnees a modifier
function completePageAfterGoto(dataLDF) {
  myDebug("completePageAfterGoto ... : " + JSON.stringify(dataLDF));

  document.getElementById("updateID").value = dataLDF['sid'];
  $('#photoFacturette').val("UPDATE");

  let fieldsToValidate = ['nom', 'arrivee', 'depart', 'distance', 'label', 'ladate', 'ttc', 'typeFrais', 'moyenPaiement', 'photoFacturette'];
  for (let i = 0; i < fieldsToValidate.length; i++) {
    let field = fieldsToValidate[i];
    //cas particuliers
    switch (field) {
      case 'photoFacturette':
        // myDebug("On affiche l'image");
        $("#smallImage").attr("src", dataLDF[field]);
        break;
      case 'moyenPaiement':
        //cas particulier car il faut attendre que feedPaiement soit chargé ...
        //-> c'est la qu'on fera un unset de dataLDF
        break;
      default:
        if (document.getElementById(field)) {
          // myDebug("On essaye de completer la valeur de " + field + " par ce qu'on a du serveur " + dataLDF[field]);
          // if (field == "ladate") {
          //   console.log(dataLDF[field]);
          // }
          document.getElementById(field).value = dataLDF[field];
        }
    }
  }

  //Pour la TVA c'est un peu plus compliqué depuis qu'on a des taux variables
  //le serveur peut envoyer des données du genre     "tvaTx1": 10, "tvaTx2": 20, "tvaTx3": null, "tvaTx4": null, "tvaVal1": 26.12, "tvaVal2": 13.06, "tvaVal3": null, "tvaVal4": null,
  //il faut donc aller chercher les bonnes données
  // 'tvaVal1', 'tvaVal2', 'tvaVal3', 'tvaVal4'
  for (let i = 1; i < 5; i++) {
    let fieldNameTx = 'tvaTx' + i;
    let fieldNameVal = 'tvaVal' + i;
    // fix #20
    for (let j = 1; j < 5; j++) {
      //Si on est sur le même taux "local" et "serveur"
      if (localGetData("compta_tva_tx" + i) == dataLDF["tvaTx" + j]) {
        if (document.getElementById(fieldNameVal)) {
          myDebug("On essaye de completer la TVA de " + fieldNameVal + " locale " + $("#" + fieldNameTx).val() + " par ce qu'on a du serveur " + dataLDF[fieldNameTx] + " val = " + dataLDF[fieldNameVal]);
          document.getElementById(fieldNameVal).value = dataLDF['tvaVal' + j];
        }
      }
    }
  }
}

ons.ready(function () {
  myDebug("ONS Ready, ready in ndf.js");

  $('#label').keyup(function () {
    $('#listeObjets').show();
  });

  // -----------------------------
  // gestion du splitter ...
  let onsRightMenu = document.getElementById('rightmenu');
  if (onsRightMenu != undefined) {
    onsRightMenu.load("parts/splitter.html");
    onsRightMenu.close();
  }

  window.fn = {};

  // =================== migration vers le navigator
  //Ne doit être utilisé qu'une seule fois dans index.html
  //tous les autres appels devraient être gotoPage
  window.fn.load = function (page, data) {
    var content = document.getElementById('globalMyNavigator');
    var onsRightMenu = document.getElementById('rightmenu');
    content.pushPage(page, data)
      .then(onsRightMenu.close.bind(onsRightMenu));
  };

  window.fn.pop = function () {
    var content = document.getElementById('globalMyNavigator');
    content.popPage();
  };

  window.fn.open = function () {
    myDebug("fn.open");

    let onsRightMenu = document.getElementById('rightmenu');
    if (onsRightMenu != undefined)
      onsRightMenu.open();
    else
      myDebug("fn.open : UNDEFINED");
  };

  /*
  //Pseudo gestion du "back" a voir si on pourra le binder sur le vrai bouton back de l'os
  window.fn.back = function () {
    listeContent = JSON.parse(localGetData("history"));
    // myDebug('history lors du clic sur le bouton prev (fn.back): ' + listeContent);
    if (Array.isArray(listeContent) && (listeContent.length > 1)) {
      var currentPage = listeContent.pop();
      var prevPage = listeContent.pop();
 
      let onsContent = document.getElementById('maincontent');
 
      if (prevPage == "") {
        if (globalCurrentTitle == "DoliSCAN") {
          // myDebug("appel askQuitOrMenu 1 : " + globalCurrentTitle);
          askQuitOrGoto("menu.html");
        }
        else {
          cleanHistorique();
          globalCurrentTitle = "DoliSCAN";
          onsContent.load("menu.html");
          // $('#container').load("menu.html");
        }
      }
      else {
        listeContent.push(prevPage);
        localStoreData("history", JSON.stringify(listeContent));
        // $('#container').load(prevPage);
        onsContent.load(prevPage);
        globalCurrentPage = prevPage;
        return;
      }
    }
    else {
      //Si on est pas sur le menu ... on s'y place
      if (globalCurrentTitle != "DoliSCAN") {
        localStoreData("history", JSON.stringify(""));
        globalCurrentTitle = "DoliSCAN";
        globalCurrentPage = "menu.html";
        onsContent.load("menu.html");
        // $('#container').load("menu.html");
      }
      else {
        // et on ne propose de quitter que si on est sur le menu
        // myDebug("appel askQuitOrMenu 2");
        askQuitOrGoto("menu.html");
      }
    }
    return;
  };
  */
});


//Tout le code pour les pages
document.addEventListener('init', function (event) {
  var objets = "";
  let lapage = event.target.id; //;event.target.pushedOptions.page;

  myDebug("addEventListener : " + lapage);

  //Dans le cas où on est sur l'index / accueil / lancement initial  on ne fait rien
  if (lapage == "loadingPage" || lapage == "ONSSplitter") {
    return;
  }

  if (globalCustomCSS == true) {
    loadCustomLogo();
    loadCustomCSS();
  }
  translateUI();

  interdictionDateFutur();

  if (lapage == 'ONSApropos') {
    // setPageTitle("À propos");
    feedDoliscanInfos();
    feedAproposCustom();
    //apres des feed il faut refaire une traduction
    return;
  }

  //Quand le splitter est chargé, on pousse les infos "techniques"
  // if (lapage == '#ONSSplitter.html') {
  //   return;
  // }

  //Si on reviens sur cette page et qu'on a un ou plusieurs vehicules de deja configurés
  //On passe à la page suivante
  if (lapage == 'ONSBienvenue') {
    testvVP = localGetData("vehicules");
    if (testvVP !== null) {
      gotoPage('bienvenue-step2.html', false, false);
    }
    // setPageTitle("Bienvenue !");
    return;
  }
  if (lapage == 'ONSBienvenue2') {
    // setPageTitle("Démarrage");
    return;
  }
  //Si on reviens sur cette page et qu'on a un ou plusieurs vehicules de deja configurés
  //On passe à la page suivante
  if (lapage == 'ONSBienvenue3') {
    testVP = localGetData("vehiculesPro");
    if (testVP !== null) {
      gotoPage('ONSBienvenue4', false);
    }
    // setPageTitle("Démarrage");
    return;
  }
  if (lapage == 'ONSBienvenue4') {
    // setPageTitle("Démarrage");
    return;
  }
  if (lapage == 'ONSBienvenue5') {
    // setPageTitle("Démarrage");
    return;
  }
  if (lapage == 'ONSBienvenue6') {
    // setPageTitle("Démarrage");
    //Si on  a un vehicule perso on pourrait importer la base de donnee des principales villes ...
    //Pour l'instant non car on n'en fait rien
    testvVP = localGetData("vehicules");
    if (testvVP !== null) {
      importListeVilles();
    }
    return;
  }

  // ----------- config ajout de vehicule
  if (lapage == 'ONSConfigAddVehicule') {

    $('#leformAddVehicule').validate({
      rules: {
        onsubmit: false,
        success: "valid",
        nom: {
          required: true,
          minlength: 2
        },
        kmbefore: {
          required: false
        },
      },
      messages: {
        nom: "Veuillez saisir cette information.",
      },
      invalidHandler: function (event, validator) {
        // 'this' refers to the form
        // myDebug("addVehicule error 2");
        var errors = validator.numberOfInvalids();
        // alert(errors);
      },
      submitHandler: function (form) {
      },
    });

    // setPageTitle("Véhicule");
    return;
  }

  // ----------- config ajout de vehicule pro
  if (lapage == 'ONSConfigAddVehiculePro') {
    $('#leformAddVehiculePro').validate({
      rules: {
        onsubmit: false,
        success: "valid",
        nom: {
          required: true,
          minlength: 2
        },
        kmbefore: {
          required: false
        },
      },
      messages: {
        nom: "Veuillez saisir cette information.",
      },
      invalidHandler: function (event, validator) {
        // 'this' refers to the form
        // myDebug("addVehicule error 2");
        var errors = validator.numberOfInvalids();
        // alert(errors);
      },
      submitHandler: function (form) {
      },
    });
    // setPageTitle("Véhicule Pro.");
    return;
  }

  // ----------- config modification de vehicule
  if (lapage == 'ONSConfigEdVehicule') {

    $('#leformEdVehicule').validate({
      rules: {
        onsubmit: false,
        success: "valid",
        nom: {
          required: true,
          minlength: 2
        },
        kmbefore: {
          required: false
        },
      },
      messages: {
        nom: "Veuillez saisir cette information.",
      },
      invalidHandler: function (event, validator) {
        // 'this' refers to the form
        // myDebug("addVehicule error 2");
        var errors = validator.numberOfInvalids();
        // alert(errors);
      },
      submitHandler: function (form) {
      },
    });

    // setPageTitle("Véhicule");
    return;
  }

  // ------------- export compta
  if (lapage == 'ONSExportCompta') {

    //A l'ouverture de la page on complète le formulaire avec les valeurs de cet utilisateur
    for (let key of configKeys) {
      // myDebug("On cherche " + key);
      if (localGetData(key) !== null) {
        if ($('#' + key)[0].type == "text" || $('#' + key)[0].type == "number" || $('#' + key)[0].type == "email") {
          // myDebug('affectation de valeur ' + localGetData(key) + ' a ' + key);
          $('#' + key).val(localGetData(key));
        }
        else {
          $('#' + key).append(localGetData(key));
        }
      }
    }
    // setPageTitle("Export");

    //Et on dit ce qui est conforme
    $('#leformcompta').validate({
      onsubmit: false,
      success: "valid",
      debug: false,
      rules: {
        compta_email: {
          required: false,
          minlength: 3
        },
        compta_ik: {
          required: true,
          minlength: 3
        },
        compta_peage: {
          required: true,
          minlength: 3
        },
        compta_hotel: {
          required: true,
          minlength: 3
        },
        compta_train: {
          required: true,
          minlength: 3
        },
        compta_carburant: {
          required: true,
          minlength: 3
        },
        compta_taxi: {
          required: true,
          minlength: 3
        },
        compta_restauration: {
          required: true,
          minlength: 3
        },
        compta_divers: {
          required: true,
          minlength: 3
        },
        compta_compteperso: {
          required: true,
          minlength: 3
        },
        compta_tvadeductible: {
          required: true,
          minlength: 3
        }
      },

      submitHandler: function (form) {
        // myDebug("exportCompta début de submitHandler");
      },

      invalidHandler: function (event, validator) {
        // myDebug("exportCompta error 2" + JSON.stringify(error) + " sur " + JSON.stringify(element));
      },

      showErrors: function (errorMap, errorList) {
        // myDebug('exportCompta error, nb erreurs ' + this.numberOfInvalids() + " : " + JSON.stringify(errorList));
      },
    });
    return;
  }

  // ------------- configuration
  if (lapage == 'ONSConfig') {

    var pullHook = document.getElementById('pull-hook');

    pullHook.addEventListener('changestate', function (event) {
      var message = '';

      switch (event.state) {
        case 'initial':
          message = 'Pull to refresh';
          break;
        case 'preaction':
          feedVehiculesConfig("#feedVehiculesConfig", "vehicules");
          feedVehiculesConfig("#feedVehiculesProConfig", "vehiculesPro");
          //apres des feed il faut refaire une traduction
          message = 'Release';
          break;
        case 'action':
          message = 'Loading...';
          break;
      }

      pullHook.innerHTML = message;
    });

    pullHook.onAction = function (done) {
      setTimeout(done, 1000);
    };

    feedVehiculesConfig("#feedVehiculesConfig", "vehicules");
    feedVehiculesConfig("#feedVehiculesProConfig", "vehiculesPro");
    //apres des feed il faut refaire une traduction
    // translateUI();

    // setPageTitle("Configuration");
    return;
  }

  // ------------- details NDF
  if (lapage == 'ONSDetails') {
    // setPageTitle("Details Note");
    // $('#contenuNDFS').empty();
    showWait();

    if (ajaxDetailsNDF)
      ajaxDetailsNDF.abort();

    var ajaxDetailsNDF = $.ajax({
      url: localGetData("api_server") + "/api/NdeFraisDetails/" + localGetData("ndfId"),
      timeout: 5000,
      type: "GET",
      dataType: "json",
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
      },
      xhrFields: {
        withCredentials: true
      },
      success: function (result) {
        globalNetworkResponse = Date.now();
        // myDebug(result);

        let titre = "";
        if (localGetData("ndfStatus") == 0) {
          titre = "Historique de la note de frais #";
        }
        else {
          titre = "Note de frais provisoire #";
        }
        $('#ndfLabel').text(titre + localGetData("ndfLabel"));

        for (var i in result) {
          data = result[i];
          var ladate = dateFormatDDMMYYYY(data.ladate);
          var icone = faIcone(data.label, data.type_frais_id);
          var texte = ladate + " (" + data.sid + ") " + data.label;
          if (data.ttc > 0) {
            texte += " (" + nbFR(data.ttc) + ")";
          }
          var loadthis = "<ons-list-item modifier=\"longdivider\" tappable onclick=\"localStoreData('ldfId','" + data.sid + "');gotoPage('detailsLdf.html');\"><div class=\"left\">" + icone + "</div><div class=\"center\">" + texte + "</div></ons-list-item>";
          $(loadthis).hide().appendTo($('#contenuNDFS')).fadeIn(1000);
        }
        hideWait();
      },
      error: function (result) {
        hideWait();
        //TODO message a faire
        // window.location.replace('index.html');
        closePage();
      }
    });
    return;
  }

  // ------------- details LDF
  if (lapage == 'ONSDetailsLDF') {
    // setPageTitle("Details Frais");

    let el = document.querySelector('#displayDocument');
    // let pz = new PinchZoom(el, {});

    $('#contenuLDF').empty();
    showWait();
    if (ajaxUpdateServerLDF)
      ajaxUpdateServerLDF.abort();

    var ajaxUpdateServerLDF = $.ajax({
      url: localGetData("api_server") + "/api/LdeFrais/" + localGetData("ldfId"),
      timeout: 5000,
      type: "GET",
      dataType: "json",
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
      },
      xhrFields: {
        withCredentials: true
      },
      success: function (result) {
        dataLDF = result;
        //Pour forcer la persistance des données ... il faut modifier l'objet...
        dataLDF['moyenPaiement'] = result.paiement_slug;
        localStoreData('moyenPaiement', result.paiement_slug);
        // myDebug(dataLDF);
        globalNetworkResponse = Date.now();

        var ladate = dateFormatDDMMYYYY(dataLDF.ladate);
        var loadthis = "";
        // if (dataLDF.isUpdatable) {
        //   loadthis = "<input type=\"button\" onclick=\"gotoPage('frais-" + dataLDF.frais_slug + ".html',false);\" value=\"Modifier cette fiche\" /><br />";
        // }
        // else {
        //   loadthis = "<i>Document archivé, non modifiable</i>";
        // }
        loadthis += "<ul><li><b>Date :</b> " + ladate + "</li><li><b>Objet :</b> " + dataLDF.texte + "</li><li><b>Montant :</b> " + dataLDF.montant + "</li></ul>";
        if (dataLDF.isUpdatable) {
          loadthis += "<input type=\"button\" onclick=\"askDoubleCheckForDelete();\" value=\"Supprimer cette fiche\" /><br />";
        }
        $(loadthis).hide().appendTo($('#contenuLDF')).fadeIn(1000);

        $('#ldfId').text(localGetData("ldfId"));

        // ons.notification.alert(dataLDF.fileName, { title: trans("warning") });
        if (dataLDF.fileName == null || dataLDF.frais_slug == "ik") {
          $('#displayDocument').empty();
          hideWait();
        }
        else {
          //image du genre /ldfImages/demo@cap-rel.fr/20191009-restauration-louis-2K4hoz5pdy.jpeg
          imgURI = localGetData("api_server") + "/api/ldfImages/" + localGetData("email") + "/" + dataLDF.fileName;
          if (ajaxUpdateServerLDFBis)
            ajaxUpdateServerLDFBis.abort();

          var ajaxUpdateServerLDFBis = $.ajax({
            url: imgURI,
            timeout: 5000,
            type: "GET",
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
            },
            xhrFields: {
              withCredentials: true
            },
            success: function (dataImg) {
              globalNetworkResponse = Date.now();
              hideWait();
              //var image = "<img id=\"imageZoomable\" src=\"data:image;base64," + dataImg + "\" style=\"min-height: 200px; max-width: 200px;\"/>";
              //$(image).hide().appendTo($('#contenuLDFImage')).fadeIn(1000);
              let imageSrc = "data:image;base64," + dataImg;
              $("#imageZoomable").attr("src", imageSrc);
              dataLDF.photoFacturette = imageSrc;
            },
            error: function (result) {
              hideWait();
              //pas trop grave pour l'image :)
              //alert('error');
            }
          });
        }


      },
      error: function (result) {
        hideWait();
        //TODO message a faire
        //alert('Erreur de transfert réseau ...');
        // window.location.replace('index.html');
        closePage();
      }
    });
    return;
  }

  // ------------- historique
  if (lapage == 'ONSHistorique') {

    // setPageTitle("Historique");
    $('#contenuNDF').empty();

    if (ajaxHistory)
      ajaxHistory.abort();

    var ajaxHistory = $.ajax({
      url: localGetData("api_server") + "/api/NdeFrais",
      timeout: 5000,
      type: "GET",
      dataType: "json",
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
      },
      xhrFields: {
        withCredentials: true
      },
      success: function (result) {
        globalNetworkResponse = Date.now();

        hideWait();
        for (var i = 0; i < result.length; i++) {
          //result.length
          var data = result[i];
          if (typeof data == 'object' && data !== null) {
            // var dataimg = "";
            let montantTxt = "";
            let styleTxt = "";
            if (i == 0) {
              montantTxt = " ( en cours )";
            }
            else {
              styleTxt = "style=\"background: #efefef;\"";
              if (data.montant > 0) {
                montantTxt = " (" + data.montant + ")";
              }
            }
            var loadthis = "<ons-button modifier=\"material large\" onclick=\"localStoreData('ndfId','" + data.sid + "'); localStoreData('ndfStatus','" + data.status + "');localStoreData('ndfLabel','" + data.label + "');gotoPage('details.html');\" class=\"menubutton\"" + styleTxt + "><i class=\"fas fa-angle-right\"></i> " + data.label + montantTxt + "</ons-button>";

            //var loadthis = "<p>" + data.label + " (" + data.montant + ") <a href=\"#\" onclick=\">[détails]</a></p>";
            $(loadthis).hide().appendTo($('#contenuNDF')).fadeIn(1000);
          }
          else {
            //TODO message a faire
            // window.location.replace('index.html');
            closePage();
          }
        }
      },
      error: function (result) {
        hideWait();
        // myDebug('historique.html erreur de transfert avec le serveur ... retour a l index');
        //Probable fin de session ...
        // window.location.replace('index.html');
        closePage();
      }
    });
    return;
  }

  // ------------- login
  if (lapage == 'ONSlogin') {
    myDebug("ons show login.html ...");

    // myDebug("SELECT * FROM config ERROR");
    if (localGetData("api_server") !== null) {
      //Au lancement on regarde si on a pas déjà ces infos de la dernière fois ...
      // switchServer('#server', localGetData("name_server"), localGetData("api_server"));
      //Et on essaye de reprendre la session ! sauf si on a plusieurs comptes sur l'appli
      //en ce cas on affiche toujours le user switcher au lancement !
      myDebug("Login with API token...");
      let a = localGetData("accounts");
      if (a !== null && a !== "") {
        accounts = JSON.parse(a);
        if (accounts.length == 1) {
          loginWithAPIToken();
        }
      }
    }

    $(".toggle-password").click(function () {
      myDebug("Clic sur l'affichage du mot de passe ...");

      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });

    // myDebug("carouselLogin call 1");
    // carouselLogin();
    return;
  }

  // ------------- menu
  if (lapage == 'ONSMenu') {
    // $('#titreDoliScan').replaceWith('<div id="titreDoliScan" class="left" onclick="gotoPage(\'menu.html\', false);">Doliscan</div>');
    // $('#menuHautDroite').replaceWith('<div id="menuHautDroite" class="right"><ons-toolbar-button onclick="fn.open()"><ons-icon icon="md-menu"></ons-icon></ons-toolbar-button></div>');
    // globalCurrentTitle = 'DoliSCAN';

    //Un petit reset des données de mise à jour
    dataLDF = "";
    feedDoliscanAccount();
  }

  // ------------- upload ok
  if (lapage == 'ONSMessageDone') {
    // $('#titreDoliScan').replaceWith('<div id="titreDoliScan" class="left" onclick="gotoPage(\'menu.html\', false);">Doliscan</div>');
    // $('#menuHautDroite').replaceWith('<div id="menuHautDroite" class="right"><ons-toolbar-button onclick="fn.open()"><ons-icon icon="md-menu"></ons-icon></ons-toolbar-button></div>');
    // globalCurrentTitle = 'UploadOK';
    return;
  }

  // ------------- upload err
  if (lapage == 'ONSMessageError') {
    // $('#titreDoliScan').replaceWith('<div id="titreDoliScan" class="left" onclick="gotoPage(\'menu.html\', false);">Doliscan</div>');
    // $('#menuHautDroite').replaceWith('<div id="menuHautDroite" class="right"><ons-toolbar-button onclick="fn.open()"><ons-icon icon="md-menu"></ons-icon></ons-toolbar-button></div>');
    // globalCurrentTitle = 'UploadErr';
    return;
  }

  // ------------- upload in progress
  if (lapage == 'ONSMessageUploadProgress') {
    $('#titreDoliScan').replaceWith('<div id="titreDoliScan" class="left" onclick="gotoPage(\'menu.html\', false);">Téléversement...</div>');
    // $('#menuHautDroite').replaceWith('<div id="menuHautDroite" class="right"><ons-toolbar-button onclick="fn.open()"><ons-icon icon="md-menu"></ons-icon></ons-toolbar-button></div>');
    // globalCurrentTitle = 'TransfertInProgress';
    return;
  }

  // ------------- mon compte
  //A l'ouverture de la page on complète le formulaire avec les valeurs de cet utilisateur
  if (lapage == 'ONSMonCompte') {
    myDebug("SQL api_token check " + localGetData("api_token"));
    for (let key of configKeys) {
      // myDebug("On cherche " + key);
      // myDebug("on cherche " + key);
      if (localGetData(key) !== null) {
        if ($('#' + key)[0].type == "text" || $('#' + key)[0].type == "number" || $('#' + key)[0].type == "email") {
          $('#' + key).val(localGetData(key));
        }
        else {
          $('#' + key).append(localGetData(key));
        }
      }
    }
    return;
  }

  // configutation tva
  if (lapage == 'ONSVATConfig') {
    myDebug("SQL api_token check " + localGetData("api_token"));
    for (let key of configKeys) {
      // myDebug("On cherche " + key);
      // myDebug("on cherche " + key);
      if (localGetData(key) !== null) {
        if ($('#' + key)[0].type == "text" || $('#' + key)[0].type == "number" || $('#' + key)[0].type == "email") {
          $('#' + key).val(localGetData(key));
        }
        else {
          $('#' + key).append(localGetData(key));
        }
      }
    }
    return;
  }

  // ------------- synchronisation
  if (lapage == 'ONSSync') {
    // feedToSync("#feedToSync");
    if (window.cordova.platformId == "android") {
      listPath(window.cordova.file.externalDataDirectory);
    }
    else {
      listPath(cordova.file.documentsDirectory);
    }
    return;
  }

  // ------------- recuperation de donnees sur l'agenda
  if (lapage == 'ONSAgendaIK') {
    // feedToAgendaIK("#feedToAgendaIK");
    // if (document.getElementById('feedToAgendaIK.html') {
    //     $("#feedToAgendaIK").append(feedToAgendaIK("feedToAgendaIK"));
    // }
    // setPageTitle("Agenda -> IK");
    //pas de return on a besoin du initialiseform
    //return;
  }


  // ============================== les frais
  // ----------- carburant
  if (lapage == 'ONSFraisCarburant') {
    objets = JSON.parse(localGetData("objetsCarburant"));

    // setPageTitle("Carburant");
    // myDebug("page FraisCarburant Chargée...");
  }

  // ----------- divers
  if (lapage == 'ONSFraisDivers') {
    objets = JSON.parse(localGetData("objetsDivers"));
    // setPageTitle("Divers");
  }
  if (lapage == 'ONSFraisDiversPDF') {
    objets = JSON.parse(localGetData("objetsDivers"));
    // setPageTitle("Divers");
  }

  // ----------- hotel
  if (lapage == 'ONSFraisHotel') {
    objets = JSON.parse(localGetData("objetsHotel"));
    // setPageTitle("Hôtel");
    myDebug("page FraiHotel Chargée...");
  }

  // ----------- IK
  if (lapage == 'ONSFraisIK') {
    myDebug("addEventListener : " + lapage);

    objets = JSON.parse(localGetData("objetsIK"));

    //On evite d'ajouter à chaque fois à la liste
    if ($('.expandable-content').length == 0) {
      // setPageTitle("Indemnités Kms");
      cities = JSON.parse(localGetData("cities"));
      if (cities) {
        //On limite à 15 pour eviter de bloquer le terminal ...
        for (var i = 0; i < cities.length && i < 15; i++) {
          $('#listeDepart').append('<div class=\"expandable-content list-item__expandable-content\"><a href="#" onclick="selectDepart(\'' + cities[i] + '\');">' + cities[i] + '</a></div>');
          $('#listeArrivee').append('<div class=\"expandable-content list-item__expandable-content\"><a href="#" onclick="selectArrivee(\'' + cities[i] + '\');">' + cities[i] + '</a></div>');
        }
      }
    }
  }

  // Agenda / IK
  if (lapage == 'ONSAgendaIK') {
    vTest = localGetData("vehicules");

    if (vTest == null) {
      ons.notification.alert("Si vous voulez saisir des IK ajoutez un véhicule dans le menu configuration.", { title: "Attention" }).then(function () {
        gotoPage("menu.html", true);
      });
    }
    objets = JSON.parse(localGetData("objetsIK"));

    //On evite d'ajouter à chaque fois à la liste
    if ($('.expandable-content').length == 0) {
      cities = JSON.parse(localGetData("cities"));
      if (cities) {
        //On limite à 15 pour eviter de bloquer le terminal ...
        for (var i = 0; i < cities.length && i < 15; i++) {
          $('#listeDepart').append('<div class=\"expandable-content list-item__expandable-content\"><a href="#" onclick="selectDepart(\'' + cities[i] + '\');">' + cities[i] + '</a></div>');
        }
      }
    }
  }


  // ----------- Péage
  if (lapage == 'ONSFraisPeage') {
    objets = JSON.parse(localGetData("objetsPeageParking"));

    // setPageTitle("Péage ou Parking");
  }

  // ----------- Restauration
  if (lapage == 'ONSFraisRestauration') {
    objets = JSON.parse(localGetData("objetsResto"));

    // setPageTitle("Restauration");
  }

  // ----------- Taxi
  if (lapage == 'ONSFraisTaxi') {
    objets = JSON.parse(localGetData("objetsTaxi"));

    // setPageTitle("Taxi");
  }

  // ----------- Train
  if (lapage == 'ONSFraisTrain') {
    objets = JSON.parse(localGetData("objetsTrain"));

    // setPageTitle("Train");
    // myDebug("on fait un appel a  setPageTitle pour train");

  }

  // ----------- Code commun
  // nom du fichier global remis à zéro
  globalPictureFileName = "";
  myDebug("reset de globalPictureFileName ...");

  //On complète automatiquement le champ
  feedObjets(objets);

  //Et on initialise le formulaire (moyens de paiements etc.)
  initialiseForm();

  //Et si on a des données à compléter automatiquement
  if (dataLDF != "") {
    //Et si on a des données d'IK à compléter automatiquement
    if (lapage == 'ONSFraisIK') {
      completePageIKAfterGoto(dataLDF);
    }
    else {
      //Si ce n'est pas une page de frais pas la peine d'y aller
      if (lapage.startsWith('ONSFrais')) {
        completePageAfterGoto(dataLDF);
      }
    }
  }

  //Pour eviter d'avoir des "envois" de formulaire quand on appuie sur le bouton
  //suivant du keypad/numpad
  $('form input').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    myDebug("keypressed : " + keyCode);
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
  });



  //On accroche le pinchZoom sur l'objet qui contient la facturette
  // let el = document.querySelector('#contenuLDFImage');
  // if (el !== null) {
  // myDebug("Contenu LDF Image : " + el);
  // let pz = new PinchZoom(el, {});
  // }

  //apres des feed il faut refaire une traduction
  // translateUI();

}, false);
