/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

//Supprime un id en attente de synchronisation
function rSyncDelete(idToDelete) {
    if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
        localDeleteLDFtoSync(idToDelete);
        // myDebug("  ldfsToSync SQL DELETE FROM ldfsToSync WHERE ID=" + idToDelete);
        // globalDB.executeSql('DELETE FROM ldfsToSync WHERE ID=?;', [idToDelete]);
        // myDebug("  ldfsToSync SQL DELETE FROM ... : ok");
        ons.notification.alert("Document supprimé", { title: "Information" }).then(
            function () { closePage(true); }
        )
    }
    else {
        // myDebug("  ldfsToSync ERREUR SQL DELETE impossible (rsyncdelete)");
        ons.notification.alert("Erreur de suppression (SQL)", { title: "Information" }).then(
            function () { closePage(true); }
        )
    }
}

//Synchronise un élément vers le serveur
function rSyncOne(idToSync) {
    showWait();
    //on utilise globalLastInsertId pour que l'upload supprime l'id si ça s'est bien passé :)
    globalLastInsertId = idToSync;
    //et le flag suivant pour eviter de passer en boucle infinie "sync/sync/sync"
    globalSyncInProgress = true;
    myDebug("start rsyncOne ... pour globalLastInsertId = " + globalLastInsertId);
    if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
        myDebug("  ldfsToSync SQL : select * from ldfsToSync WHERE id=" + idToSync);

        let objectStore = globalIndexedDB.transaction("ldfsToSync").objectStore("ldfsToSync", "readwrite");
        objectStore.get(idToSync).onsuccess = function (event) {
            myDebug("  get idToSync ok " + JSON.stringify(event.target.result));

            let v = event.target.result;
            //Mise a jour uniquement si non vide
            if (v !== null && v != "") {
                let url = v.url;
                let jsonData = v.jsonData;
                let localFileName = v.localFileName;
                let data = JSON.parse(jsonData);
                myDebug('  feedToSync ' + idToSync + ' -> ' + v);

                var formdata = new FormData();
                //On complete le formdata a partir de ce qui était stocké en base
                for (var key in data) {
                    if (key == "afile") {
                    } else {
                        formdata.append(key, data[key]);
                    }
                }

                let pathToFile = cordova.file.documentsDirectory;
                if (window.cordova.platformId == "android") {
                    pathToFile = window.cordova.file.externalDataDirectory; //.replace("file://","");
                }
                myDebug("  on recherche " + pathToFile + " dans " + localFileName + "...");
                //parfois on concatène un coup de trop ...
                if (localFileName.startsWith(pathToFile) > 0) {
                    myDebug("  nettoyage de pathToFile... ");
                    pathToFile = "";
                }
                if (localFileName.startsWith("file://") > 0) {
                    myDebug("  nettoyage de pathToFile (file://)... ");
                    pathToFile = "";
                }
                endSendForm(formdata, pathToFile + localFileName, "sync", idToSync);
            }
        }

        objectStore.get(idToSync).onerror = function (event) {
            myDebug("  get idToSync Error" + JSON.stringify(event));
        }



        // SQLite old
        // globalDB.readTransaction(function (t) {
        //     t.executeSql('SELECT * FROM ldfsToSync WHERE id=?', [idToSync],
        //         function (tx, rs) {
        //             myDebug("  ldfsToSync -> SQL SELECT OK: res " + rs.rows.length);
        //             let nbRes = rs.rows.length;
        //             let i = 0;
        //             let id = rs.rows.item(i).sid;
        //             let url = rs.rows.item(i).url;
        //             let jsonData = rs.rows.item(i).jsonData;
        //             let localFileName = rs.rows.item(i).localFileName;
        //             let data = JSON.parse(jsonData);

        //             var formdata = new FormData();
        //             //On complete le formdata a partir de ce qui était stocké en base
        //             for (var key in data) {
        //                 if (key == "afile") {
        //                 } else {
        //                     formdata.append(key, data[key]);
        //                 }
        //             }

        //             let pathToFile = cordova.file.documentsDirectory;
        //             if (window.cordova.platformId == "android") {
        //                 pathToFile = window.cordova.file.externalDataDirectory; //.replace("file://","");
        //             }
        //             myDebug("  on recherche " + pathToFile + " dans " + localFileName + "...");
        //             //parfois on concatène un coup de trop ...
        //             if (localFileName.startsWith(pathToFile) > 0) {
        //                 myDebug("  nettoyage de pathToFile... ");
        //                 pathToFile = "";
        //             }
        //             if (localFileName.startsWith("file://") > 0) {
        //                 myDebug("  nettoyage de pathToFile (file://)... ");
        //                 pathToFile = "";
        //             }
        //             endSendForm(formdata, pathToFile + localFileName);
        //         },
        //         function () {
        //             myDebug("  ldfsToSync -> SQL Erreur de SELECT FROM ldfsToSync");
        //         });
        // });
    }
    myDebug("end rsyncOne ...");
}


//Synchronise un élément vers le serveur, nouveau modele (c'est le serveur qui indique ce qu'il manque)
function rSyncOneNew(id, localFileName, idToSync) {
    showWait();
    myDebug("start rSyncOneNew ... pour idToSync = " + idToSync);
    let data = globalSyncObject[idToSync];

    //on utilise globalLastInsertId pour que l'upload supprime l'id si ça s'est bien passé :)
    globalLastInsertId = idToSync;
    //et le flag suivant pour eviter de passer en boucle infinie "sync/sync/sync"
    globalSyncInProgress = true;
    myDebug("start rSyncOneNew ... pour globalLastInsertId = " + globalLastInsertId);
    var formdata = new FormData();
    //On complete le formdata a partir de ce qui était stocké en base
    for (var key in data) {
        formdata.append(key, data[key]);
        myDebug("  rSyncOneNew data " + key + " = " + data[key]);
    }

    myDebug("  rSyncOneNew localFileName " + localFileName);
    endSendForm(formdata, localFileName, "sync", idToSync);

}

//Tentative de recherche dans un tableau selon le masque de recherche ...
function filtreTexte(arr, requete) {
    myDebug("filtreTexte :" + requete + " dans " + JSON.stringify(arr));
    return arr.filter(function (el) {
        return el.toLowerCase().indexOf(requete.toLowerCase()) !== -1;
    })
}

/**
 * Recupere la liste des LDF à synchroniser (liste des photos manquantes)
 */
function getListToSync() {
    showWait();
    globalSyncObject = [];
    //et le flag suivant pour eviter de passer en boucle infinie "sync/sync/sync"
    globalSyncInProgress = true;
    myDebug("start getListToSync");

    $.ajaxSetup({
        timeout: 10000,
        headers: {
            'X-CSRF-TOKEN': localGetData("api_token")
        }
    });

    if (ajaxDownloadListToSync)
        ajaxDownloadListToSync.abort();

    let htmlFTS = "";
    $('#feedToSync').empty();

    var ajaxDownloadListToSync = $.ajax({
        type: "GET",
        timeout: 5000,
        dataType: "json",
        url: localGetData("api_server") + "/api/ldfImagesMissed",
        data: {
            email: localGetData("email"),
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
        },
        success: function (result) {
            globalNetworkResponse = Date.now();
            myDebug('ajaxDownloadListToSync : succès ...' + JSON.stringify(result));
            let id = 1;
            for (var k in result) {
                globalSyncObject[k] = result[k];
                let idLdf = result[k].id;
                myDebug("ajaxDownloadListToSync ref distante " + idLdf);
                //Si un fichier local correspond ...
                let leFic = filtreTexte(globalPicturesAvailable, result[k].searchFile);
                myDebug("filtreTexte retour :" + leFic);

                if (leFic != "") {
                    // myDebug('  ajout du code HTML pour ' + id);
                    htmlFTS += "<div style=\"clear:both\">Justificatif des frais du " + result[k].ladate + " (" + result[k].label + ", " + nbFR(result[k].ttc) + " ) <br /><div style=\"float:right\"><ons-button modifier=\"material\" onclick=\"rSyncOneNew(" + id + ",'" + leFic + "', " + idLdf + ");\">Synchroniser</ons-button> </div></div><hr />\n";
                    id++;
                }
                else {
                    myDebug('  fichier introuvable ' + result[k].searchFile);
                }
            }
            myDebug("apres le for");
            // htmlFTS += "<div style=\"clear:both\">" + id + ": " + data.typeFrais + " du " + data.ladate + " (" + data.label + ", " + nbFR(data.ttc) + " ) <br /><div style=\"float:right\"><ons-button modifier=\"material\" onclick=\"rSyncOne(" + id + ");\">Synchroniser</ons-button> <ons-button modifier=\"material\" onclick=\"rSyncDeleteConfirm(" + id + ");\">Supprimer</ons-button></div></div><hr />\n";
            hideWait();
            $('#feedToSync').append(htmlFTS);
        },
        error: function (result) {
            hideWait();
            myDebug('ajaxDownloadListToSync : error ' + result.error);
        }
    });

    myDebug("end getListToSync");
}
