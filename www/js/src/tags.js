/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2023 - GNU AGPLv3
*/


// ============================================================================================================= tags

//Ajoute un tag
function addTag(storageKey, code, label) {
  myDebug("  addTag:: ajout de " + code + " et " + label + "...");
  return new Promise((resolve, reject) => {
    //On cherche une place de libre (max 20 on a dit)
    let rNew = { "code": code, "label": label };
    let rs = JSON.parse(localGetData(storageKey));
    if (rs === null) {
      rs = [];
    }
    rs.push(rNew);
    resolve(localStoreData(storageKey, JSON.stringify(rs)));
    //window.location.replace('config.html');
  });
}


/**
 * Synchronise les etiquettes avec le serveur
 */

function syncTagsFromServer() {
  myDebug("syncTagsFromServer");

  if (ajaxSyncTags)
    ajaxSyncTags.abort();


  var ajaxSyncTags = $.ajax({
    url: localGetData("api_server") + "/api/Tag",
    timeout: 5000,
    type: "GET",
    dataType: "json",
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
    },
    xhrFields: {
      withCredentials: true
    },
    success: function (result) {
      globalNetworkResponse = Date.now();

      for (var i in result) {
        data = result[i];
        t.push({ id: data.id, slug: data.code, label: data.label });
        myDebug(data);
      }

      // //Liste triée en amont pour se simplifier la vie
      // // t.sort(function (a, b) {
      // //     return a.label.localeCompare(b.label);
      // // });

      localStoreData("tags", JSON.stringify(t));
      hideWait();
    },
    error: function (result) {
      hideWait();
      //TODO message a faire
      // window.location.replace('index.html');
      closePage();
    }
  });
}


function showTagsListDialog() {
  globalMyNavigator.pushPage("tags.html", { data: { showApplyButton: true } });
}

/**
 * Ajoute une entrée à la liste
 **/
function makeOneTagListEntry(id, l_title, l_subtitle, l_value) {
  let h = "";
  var valA = { "id": id, "slug": l_value, "label": l_title };
  let val = btoa(JSON.stringify(valA));
  //tentative avec checkbox
  h += "<ons-list-item tappable>\n";
  h += "      <label class=\"left\"><ons-icon icon=\"md-close\" onclick=\"removeOneTag('" + id + "');\"></ons-icon></label>\n";
  h += "      <label for=\"" + l_value + "\" class=\"center\">" + l_title + "</label>\n";
  h += "</ons-list-item>\n";
  return h;
}

//Actualisation des tags, retour de la page de choix des tags
function removeOneTag(id) {
  myDebug("Il faut supprimer " + id);
  let t = $('#tags').val();
  myDebug("t =  " + t);
  let liste = JSON.parse(t);
  myDebug("Liste: " + liste);
  liste.splice(id, 1);
  refreshTags(liste);
}

//Actualisation des tags, retour de la page de choix des tags
function refreshTags(liste) {
  let formField = "#listeTags";
  let html = "";
  let arrayTags = [];
  $(formField).empty();

  myDebug("Appel de refreshTags");
  if (liste.length > 0) {
    let f = 0;
    liste.forEach(e => {
      let val = JSON.parse(atob(e));
      html += makeOneTagListEntry(f, val.label, val.label, val.slug);
      arrayTags[f] = e;
      f += 1;
    });
  }
  $('#tags').val(JSON.stringify(arrayTags));
  $(formField).append(html);
}


//Actualisation des tags, retour de la page de choix des tags
function addTagsList(liste) {
  let formField = "#listeTags";
  let html = "";
  let arrayTags = [];

  myDebug("Appel de addTagsList");
  if (liste.length > 0) {
    let f = 0;
    liste.forEach(e => {
      let val = JSON.parse(atob(e.value));
      myDebug(e.value + " :: " + val.slug + " :: " + val.label);
      html += makeOneTagListEntry(f, val.label, val.label, val.slug);
      arrayTags[f] = e.value;
      f += 1;
    });
  }
  $('#tags').val(JSON.stringify(arrayTags));
  $(formField).append(html);
}