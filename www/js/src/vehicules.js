/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

//Supprime le véhicule "id"
function rmVehicule(storageKey, id) {
    // myDebug("rmVehicule :" + id);
    let vehicules = JSON.parse(localGetData(storageKey));
    // myDebug("rmVehicule liste avant" + vehicules);
    vehicules.splice(id, 1);
    if (vehicules.length > 0)
        localStoreData(storageKey, JSON.stringify(vehicules));
    else
        localStoreData(storageKey, null);
    // myDebug("rmVehicule liste apres" + vehicules);
}

//Ajoute un véhicule perso -> maximum 3 véhicules dans cette v1 !-)
function addVehicule(storageKey, nom, energie, puissance, type, kmbefore, immat, uuid) {
    return new Promise((resolve, reject) => {
        //On cherche une place de libre (max 4 on a dit)
        let vehiculeNew = { "nom": nom, "energie": energie, "puissance": puissance, "type": type, "kmbefore": kmbefore, "immat": immat, "uuid": uuid };
        let vehicules = JSON.parse(localGetData(storageKey));
        if (vehicules === null) {
            vehicules = [];
        }
        vehicules.push(vehiculeNew);
        resolve(localStoreData(storageKey, JSON.stringify(vehicules)));
        //window.location.replace('config.html');
    });
}

/**
 * Creation de l'appel vers la page de modification du véhicule
 * @param {*} storageKey
 * @param {*} id
 */
function edVehicule(storageKey, id) {
    myDebug("edVehicule avec");
    myDebug(storageKey);
    myDebug("et id = " + id);
    myDebug("edVehicule end");

    globalVehiculeEd = { storageKey: storageKey, id: id };

    gotoPage('config-edVehicule.html', false);
    //Q: Possible de modifier le contenu de la page ?
}


//Ajoute un véhicule perso -> maximum 3 véhicules dans cette v1 !-)
function updateVehicule(storageKey, id, nom, energie, puissance, type, kmbefore, immat, uuid) {
    myDebug("updateVehicule avec uuid = " + uuid);
    return new Promise((resolve, reject) => {
        //On cherche une place de libre (max 4 on a dit)
        let vehiculeNew = { "nom": nom, "energie": energie, "puissance": puissance, "type": type, "kmbefore": kmbefore, "immat": immat, "uuid": uuid };
        let vehicules = JSON.parse(localGetData(storageKey));

        //On ecrase avec les nouvelles données
        vehicules[id] = vehiculeNew;
        resolve(localStoreData(storageKey, JSON.stringify(vehicules)));
        //window.location.replace('config.html');
    });
}

function switchEnergie(id, nom) {
    $('#energie').empty();
    $('#energievalue').val(id);
    $('#energie').append('Energie: <b>' + nom + "</b>");
    document.querySelector('#energie-list').hideExpansion();
}
function switchPuissance(id, nom) {
    $('#puissance').empty();
    $('#puissancevalue').val(id);
    $('#puissance').append('Puissance: <b>' + nom + "</b>");
    document.querySelector('#puissance-list').hideExpansion();
}
function switchType(id, nom) {
    $('#type').empty();
    $('#typevalue').val(id);
    $('#type').append('Type: <b>' + nom + "</b>");
    document.querySelector('#type-list').hideExpansion();
}


function syncVehiculesWithServer() {
    myDebug('syncVehiculesWithServer : start');
    $.ajaxSetup({
        timeout: 10000,
        headers: {
            'X-CSRF-TOKEN': localGetData("api_token")
        }
    });

    if (ajaxSyncVehicules)
        ajaxSyncVehicules.abort();

    var ajaxSyncVehicules = $.ajax({
        type: "POST",
        timeout: 5000,
        url: localGetData("api_server") + "/api/Vehicule",
        data: {
            email: localGetData("email"),
            vehicules: localGetData("vehicules"),
            vehiculesPro: localGetData("vehiculesPro"),
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localGetData("api_token"));
        },
        xhrFields: {
            withCredentials: true
        },
        success: function (result) {
            globalNetworkResponse = Date.now();
            hideWait();
            myDebug('  syncVehiculesWithServer : done');
        },
        error: function (result) {
            hideWait();
            myDebug('  syncVehiculesWithServer : error');
        }
    });
}
